
# This folder

- This folder contains info about 256 images due to be published
  as a 'benchmark dataset'. 

- See [benchmark-dataset.pdf](./benchmark-dataset.pdf)

- Paper title: "Curated benchmark dataset for ultrasound based
  breast lesion analysis"
    - Anna Pawłowska1 et al.

![benchmark_data](./image.jpg)

# Abstract

- ... we present the BrEaST dataset, the new detailed dataset of
  breast ultrasound scans. Each scan was manually annotated and
  labeled by a radiologist experienced in breast ultrasound
  examination. In particular, each tumor was identified in the
  image using a freehand annotation and labeled according to
  BIRADS features and lexicon.

- The BrEaST dataset is the first breast ultrasound dataset
  containing patient-level labels, image-level annotations, and
  tumor-level labels with all cases confirmed by follow-up care
  or core needle biopsy result. The tumor histopathological
  classification is also stated for patients who underwent
  a biopsy. 

- The dataset consists of 256 breast scans collected from 256
  patients. To enable research into breast disease detection,
  tumor segmentation and classification, the BrEaST dataset is
  made publicly available with the CC-BY 4.0 license.
