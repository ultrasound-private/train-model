-- In[]

-- ImageID
-- CaseID
-- Image_filename
-- Mask_tumor
-- Mask_other
-- Pixel_size
-- Age
-- Tissue_composition
-- Signs
-- Symptoms
-- Shape
-- Margin
-- Echogenicity
-- Posterior_features
-- Halo
-- Calcifications
-- Skin_thickening
-- Interpretation
-- BIRADS
-- Verification
-- Diagnosis
-- Classification


-- In[]

PRAGMA table_info(published_data);

-- .desc published_data

-- In[]

select ImageID, CaseID, Pixel_size, Age, Tissue_composition, Signs, Symptoms, Shape, Margin, Echogenicity, Posterior_features, Halo, Calcifications, Skin_thickening, Interpretation, BIRADS, Verification, Diagnosis, Classification from published_data limit 1;

-- ImageID:              7422                                             
-- CaseID:               1                                                
-- Pixel_size:           0.0078125                                        
-- Age:                  57                                               
-- Tissue_composition:   heterogeneous: predominantly fat                 
-- Signs:                breast scar                                      
-- Symptoms:             family history of breast/ovarian cancer          
-- Shape:                irregular                                        
-- Margin:               not circumscribed - indistinct                   
-- Echogenicity:         heterogeneous                                    
-- Posterior_features:   shadowing                                        
-- Halo:                 no                                               
-- Calcifications:       no                                               
-- Skin_thickening:      yes                                              
-- Interpretation:       Breast scar (surgery)&Breast scar (radiotherapy) 
-- BIRADS:               2                                                
-- Verification:         confirmed by follow-up care                      
-- Diagnosis:            not applicable                                   
-- Classification:       benign                                           


-- In[]




















