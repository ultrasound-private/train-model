import os
import shutil

# List of IDs to search for
ids = [
        7422, 8060, 7372, 9061, 5804, 5127, 5133, 4820, 6705, 7000, 6511,
        8308, 7733, 8351, 8864, 8905, 5494, 5192, 8020, 4950, 7379, 7387,
        8630, 7134, 6639, 7885, 8184, 7365, 6425, 5215, 7674, 5831, 4640,
        8234, 5824, 6904, 8007, 6482, 8103, 6646, 7964, 8043, 6883, 4607,
        6375, 6755, 5833, 5536, 8463, 6776, 8736, 6606, 8344, 6653, 8435,
        8239, 6588, 5771, 8423, 8902, 6332, 7882, 6792, 8493, 8337, 5349,
        8690, 9032, 7363, 8730, 7678, 5229, 5568, 6712, 8215, 8478, 8409,
        6276, 7391, 4941, 5136, 5190, 7922, 7254, 5489, 7251, 5213, 4717,
        8517, 7440, 8849, 8029, 7570, 8440, 8724, 8991, 2518, 8705, 7696,
        9036, 7383, 7099, 8430, 5812, 7304, 8806, 7539, 8513, 8118, 7575,
        5827, 7875, 4877, 7710, 7835, 8093, 8754, 8910, 8340, 7563, 8839,
        7560, 4578, 5154, 8694, 7128, 8507, 5703, 7428, 7739, 4621, 7702,
        6500, 4650, 7136, 5503, 5163, 5650, 5208, 5478, 4631, 7754, 5842,
        7402, 8012, 7352, 7397, 6947, 5165, 7180, 8549, 8709, 5839, 5662,
        7525, 7804, 6749, 5654, 8889, 4858, 5100, 5203, 5275, 7706, 7529,
        4664, 8855, 8082, 8115, 4812, 8605, 8877, 8931, 8579, 8392, 7360,
        5418, 6687, 5169, 4612, 8885, 6039, 5855, 6408, 7061, 7828, 4611,
        8503, 7431, 8568, 6436, 6970, 4701, 5392, 7862, 6338, 8799, 5144,
        8960, 7418, 8053, 9082, 5845, 6738, 7038, 8316, 8444, 4962, 5425,
        6492, 4768, 7367, 6415, 5383, 5663, 8819, 7913, 7085, 8833, 5054,
        8209, 8101, 7449, 5470, 7686, 6033, 5182, 5738, 8761, 4822, 5821,
        4692, 6847, 2569, 8794, 5514, 4945, 6046, 7007, 4987, 9070, 6396,
        5983, 4790, 5992, 4643, 7358, 8280, 7749, 4886, 4845, 8358, 8816,
        7390, 5129, 8484
        ]


# REcreate folder (remove existing)
def recreate_dir(folder_path):
    if os.path.exists(folder_path):
        shutil.rmtree(folder_path)
        print(f"\nOK: Deleted existing Folder: '{folder_path}'")
    os.makedirs(folder_path)
    print(f"\nOK: (re)Created Folder: '{folder_path}'")



# Source and destination directories
src_dir = '/mnt/dysk_roboczy/efarell/repos/US-Data/2023-11-12/pipeline-yellow-contour-no-pixelresize/data-30'
dst_dir = '../published-imageIDs'

# Create the destination directory if it doesn't exist
recreate_dir(dst_dir)

# Function to check if the filename contains any of the IDs
def contains_id(filename):
    return any(f'--ID_{id}--' in filename for id in ids)
    # return any(f'{id}' in filename for id in ids)

# Search and copy files
for root, dirs, files in os.walk(src_dir):
    for file in files:
        if contains_id(file):
            print('Copying file: ', file)
            src_file_path = os.path.join(root, file)
            dst_file_path = os.path.join(dst_dir, file)
            shutil.copy2(src_file_path, dst_file_path)

print("Files copied successfully.")
