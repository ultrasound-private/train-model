
import re

def extract_id(filename):
    # Use regular expression to find the pattern 'ID_' followed by numbers
    match = re.search(r'ID_(\d+)', filename)
    return match.group(1) if match else None

# Example usage
filename = "malignant_4c--ID_4768--584x467--Philips.jpg"
extracted_id = extract_id(filename)
print(extracted_id)  # Output: 4768

