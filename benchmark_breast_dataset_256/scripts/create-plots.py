import sqlite3
import matplotlib.pyplot as plt
import os
import shutil
import re
from PIL import Image
import textwrap


# REcreate folder (remove existing)
def recreate_dir(folder_path):
    if os.path.exists(folder_path):
        shutil.rmtree(folder_path)
        print(f"\nOK: Deleted existing Folder: '{folder_path}'")
    os.makedirs(folder_path)
    print(f"\nOK: (re)Created Folder: '{folder_path}'")


PLOT_DIR = './plots'

recreate_dir(PLOT_DIR)

# Connect to the SQLite database
conn = sqlite3.connect('../breast_dataset.sqlite')
cursor = conn.cursor()

# Path to the images folder
image_folder = '../published-imageIDs'

# Fetch all records from the database
cursor.execute("SELECT * FROM published_data order by CaseID")
records = cursor.fetchall()

# # Function to extract ImageID from filename
# def extract_image_id(filename):
    # start = filename.find('--ID_') + 1
    # end = filename.find('--')
    # return filename[start:end]


def extract_image_id(filename):
    # Use regular expression to find the pattern 'ID_' followed by numbers
    match = re.search(r'ID_(\d+)', filename)
    return match.group(1) if match else None



# Custom order for fields
field_order = [
                'CaseID', 
                'Classification', 
                'Age', 
                'Tissue_composition', 
                'Signs', 
                'Symptoms', 
                'Shape', 
                'Margin', 
                'Echogenicity', 
                'Posterior_features', 
                'Halo', 
                'Calcifications', 
                'Skin_thickening', 
                'Interpretation', 
                'BIRADS', 
                'Verification', 
                'Diagnosis', 
                'ImageID', 
                'Image_filename', 
                'Pixel_size'
]



def format_record_data(description, record, filename, align_pos=20, wrap_width=50):
    field_names = [d[0] for d in description]  # Extract only field names
    record_dict = dict(zip(field_names, record))
    record_dict['Image_filename'] = filename  # Add the filename to the record
    formatted_data = []
    for field in field_order:
        if field in record_dict:
            value = str(record_dict[field])
            value = value.replace('&', ' & ')
            if field == 'Pixel_size':
                value = f"{float(value):.4f}"  # Format pixel_size to 4 decimal places
            if field in ['Diagnosis']:
                value = "\n".join(textwrap.wrap(value, wrap_width))
                value = value + "\n"
            if field in ['Interpretation']:
                value = "\n".join(textwrap.wrap(value, wrap_width))
            truncated_field = field[:14]  # Truncate field name to 13 characters
            lines = value.split('\n')
            first_line = f"{truncated_field.ljust(align_pos, '.')} : {lines[0]}"
            formatted_data.append(first_line)
            for line in lines[1:]:
                formatted_data.append(" " * align_pos + "   " + line)
    return "\n".join(formatted_data)



# Process each file in the images folder
for file in os.listdir(image_folder):
    if file.endswith((".png", ".jpg", ".jpeg")):
        image_id = extract_image_id(file)
        # Find the corresponding record in the database
        record = next((r for r in records if str(r[0]) == image_id), None)
        if record:
            print('Processing: ', file)
            case_id = record[1]  # Assuming CaseID is the second field in the record

            # Create a new figure with two subplots
            fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(8.27, 11.69))  # A4 size in inches

            # Load and display the image
            img = Image.open(os.path.join(image_folder, file))
            ax1.imshow(img)
            ax1.axis('off')  # Hide axes for images

            # Prepare and display the text for the second subplot
            data_text = format_record_data(cursor.description, record, file)
            ax2.text(-0.1, 0.5, data_text, ha='left', va='center', fontsize=13, fontname='Ubuntu Mono')
            ax2.axis('off')  # Hide axes for text

            # Apply tight layout
            # plt.tight_layout()
            
            # Save the figure with the new naming convention
            plot_path = os.path.join(PLOT_DIR, f"Case_{str(case_id).zfill(3)}_{os.path.splitext(file)[0]}.png")
            plt.savefig(plot_path)
            plt.close(fig)

# Close the database connection
conn.close()
