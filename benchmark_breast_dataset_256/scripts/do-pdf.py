from PIL import Image
import os

# Folder containing your images
image_folder = './plots'
pdf_name     = '../benchmark-dataset.pdf'

# Get all image files in the folder
image_files = [os.path.join(image_folder, f) for f in os.listdir(image_folder) if f.endswith(('.png', '.jpg', '.jpeg'))]
image_files.sort()

# Open images and convert to RGB (to ensure compatibility)
images = [Image.open(f).convert('RGB') for f in image_files]

# Save to PDF
images[0].save(pdf_name, save_all=True, append_images=images[1:])

print('Created:', pdf_name)
