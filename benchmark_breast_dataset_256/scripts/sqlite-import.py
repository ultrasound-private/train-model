# In[]

import os
import sys
import chardet
import pandas as pd
from sqlalchemy import create_engine


# In[]

def create_table(filename, table, engine):

    if not os.path.isfile(filename):
        print('Not found: ', filename)
        return

    # Detect CSV encoding
    with open(filename, 'rb') as rawdata:
        charset = chardet.detect(rawdata.read(10000000))

    encoding = charset['encoding']
    # encoding = "UTF-8"
    # encoding = "ISO-8859-1"
        # Note: ISO-8859-1 (also called Latin-1) is identical to Windows-1252
        # (also called CP1252) except for the code points 128-159 (0x80-0x9F).
        # ISO-8859-1 assigns several control codes in this range. Windows-1252
        # has several characters, punctuation, arithmetic and business symbols
        # assigned to these code points.



    print('\n\n')
    print('FILE:      ' + filename)
    print('TABLE:     ' + table)
    print('ENCODING:  ' + encoding)

    # read file using detected encoding
    df = pd.read_csv(filename, encoding=encoding)
    old_cols = df.columns.to_list()

    # clean column names
    df.rename(columns=lambda x: x.replace(' ', '_'),    inplace =True)
    df.rename(columns=lambda x: x.replace('-', '_'),    inplace =True)
    df.rename(columns=lambda x: x.replace('*', 'star'), inplace =True)
    df.rename(columns=lambda x: x.replace('?', ''),     inplace =True)
    df.rename(columns=lambda x: x.replace('.', ''),     inplace =True)
    df.rename(columns=lambda x: x.replace(';', ''),     inplace =True)
    df.rename(columns=lambda x: x.replace(':', ''),     inplace =True)
    df.rename(columns=lambda x: x.replace('[', ''),     inplace =True)
    df.rename(columns=lambda x: x.replace(']', ''),     inplace =True)
    df.rename(columns=lambda x: x.replace('(', ''),     inplace =True)
    df.rename(columns=lambda x: x.replace('/', ''),     inplace =True)
    df.rename(columns=lambda x: x.replace(')', ''),     inplace =True)
    df.rename(columns=lambda x: x.replace('\r', ''),    inplace =True)
    df.rename(columns=lambda x: x.replace('\n', ''),    inplace =True)
    df.rename(columns=lambda x: x.replace('\t', ''),    inplace =True)
    df.rename(columns=lambda x: x.replace('__', '_'),   inplace =True)
    df.rename(columns=lambda x: x.replace('__', '_'),   inplace =True)

    new_cols = df.columns.to_list()

    print('OLD:')
    print(old_cols)
    print('NEW:')
    print(new_cols)

    df.to_sql(name=table, con=engine, if_exists='replace', index=False)

# In[]

if __name__ == '__main__':

    db = './breast_dataset.sqlite'

    # check folder exists
    if not os.path.isdir(os.path.dirname(db)):
        print('\nInvalid folder: ', db)
        sys.exit() 

    db_engine = create_engine('sqlite:///' + db)

    # ad-hoc files
    create_table('./breast_dataset.csv', 'published_data', db_engine)

# In[]
