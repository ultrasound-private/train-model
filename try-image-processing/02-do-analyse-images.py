import os
os.environ['QT_QPA_PLATFORM'] = 'offscreen'
import cv2
import os
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
from tqdm import tqdm


import sqlite3
import json
import numpy as np


PIPELINE = '/mnt/dysk_roboczy/efarell/repos/US-Data/2023-11-12/pipeline-003/'

base_input_dir = '/mnt/dysk_roboczy/efarell/repos/US-Data/2023-11-12/ORIG_NO_MASKS_RENAMED'
# base_input_dir  =  '/home/efarell/x'
# base_input_dir  = os.path.join(PIPELINE, 'data-30')
# base_input_dir  = os.path.join(PIPELINE, 'data-30-small-test')

base_output_dir  =  '/home/efarell/analyse-orig-images'
# base_output_dir  =  '/home/efarell/x-out'
# base_output_dir = os.path.join(PIPELINE, 'data-30-image-analysis')


def save_to_sqlite(db_path, properties, file_path):
    # Connect to the SQLite database
    conn = sqlite3.connect(db_path)
    cursor = conn.cursor()

    
    # Create a table with height and width columns
    cursor.execute('''CREATE TABLE IF NOT EXISTS image_properties (
                        file_path TEXT,
                        image_id TEXT,
                        type TEXT,
                        birads TEXT,
                        file_format TEXT,
                        device_name TEXT,
                        height INTEGER,
                        width INTEGER,
                        channels INTEGER,
                        bit_depth TEXT,
                        alpha_channel BOOLEAN,
                        contrast REAL,
                        texture REAL,
                        edge_info INTEGER,
                        sharpness REAL,
                        exif_data TEXT,
                        summary_noise REAL
                      )''')

                        # histogram BLOB,
                        # noise_level_estimate BLOB

    # Prepare complex data for storage
    # histogram_bytes = properties['Histogram'].tobytes()
    # noise_estimate_bytes = properties['Noise Level Estimate'].tobytes() if isinstance(properties['Noise Level Estimate'], np.ndarray) else None


    # Extract height and width from the dimensions
    height, width = properties['Dimensions']

     # Assuming file_path is the full path to your file
    filename = os.path.basename(file_path)  # Extracts the filename from the full path

    # Splitting the filename into its components
    parts = filename.split('--')

    # Extract 'type' and 'birads' from the first part
    type_birads_part = parts[0]
    type_part, birads_part = type_birads_part.split('_')

    # Extract device name (without file extension)
    device_with_ext = parts[-1]
    device_name = device_with_ext.split('.')[0]

    image_id = [part.replace('ID_', '') for part in parts if part.startswith('ID_')][0]


    cursor.execute('''INSERT INTO image_properties (
                        file_path, 
                        image_id,
                        type,
                        birads,
                        file_format, 
                        device_name, 
                        height, 
                        width, 
                        channels, 
                        bit_depth, 
                        alpha_channel, 
                        contrast, 
                        texture, 
                        edge_info, 
                        sharpness, 
                        exif_data, 
                        summary_noise)
                      VALUES (?, ?, ?, ?, ?, ?, ?, ?,  ?, ?, ?, ?, ?, ?, ?, ?, ?)''',
                    (file_path, 
                        image_id,
                        type_part,
                        birads_part,
                    properties['File Format'], 
                    device_name,
                    height, 
                    width, 
                    properties['Channels'], 
                    str(properties['Bit Depth']), 
                    properties['Alpha Channel'], 
                    float(properties['Contrast']),
                    properties['Texture (s.d.)'], 
                    properties['Edge Info'], 
                    properties['Sharpness'], 
                    json.dumps(properties['EXIF Data']), 
                    properties['Summary Noise']
                    # histogram_bytes, 
                    # noise_estimate_bytes
                    ))

                        # histogram, 
                        # noise_level_estimate) 


    # Commit and close the database connection
    conn.commit()
    conn.close()



def analyze_image_properties(image_path):
    # Load image using OpenCV
    image = cv2.imread(image_path)

    # Basic properties
    file_format = image_path.split('.')[-1]
    dimensions = image.shape[:2]
    channels = image.shape[2] if len(image.shape) == 3 else 1
    bit_depth = image.dtype
    alpha_channel = channels == 4

    # Advanced properties
    if channels == 3:
        gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    else:
        gray_image = image

    # Histogram
    histogram = cv2.calcHist([gray_image], [0], None, [256], [0, 256])

    # Contrast
    min_intensity, max_intensity = np.amin(gray_image), np.amax(gray_image)
    contrast = max_intensity - min_intensity

    # Texture (using standard deviation as a simple measure)
    texture = round(np.std(gray_image), 2)

    # Edge Information (using Canny edge detector)
    edges = cv2.Canny(gray_image, 100, 200)
    edge_count = np.count_nonzero(edges)


    # Gaussian Blur and Absolute Difference:

        # Purpose: This approach is typically used for visualizing noise. By
        # blurring the image (using a Gaussian filter) and then subtracting this
        # blurred version from the original, you effectively highlight the
        # high-frequency components, which include noise and edges.

        # Usage in Your Script: In the context of your script, this method is
        # used to create a visual representation of the noise (the noise_image).
        # This visual representation can then be used to calculate a 'summary
        # noise' metric, like the standard deviation.

    # High-Pass Filter:

        # Purpose: The high-pass filter (with the kernel provided) is designed
        # to emphasize the high-frequency components in the image while
        # suppressing low-frequency components (like smooth gradients or
        # constant regions). This is another way to estimate noise or edge
        # details in the image.

        # Usage in Your Script: The noise_estimate variable, obtained by
        # applying this high-pass filter, is also a form of noise
        # representation. It emphasizes areas of rapid intensity change, which
        # are often indicative of noise or edges.

    # Comparison and Context for 'Summary Noise':

        # - While both methods highlight noise, the Gaussian Blur and Absolute
        # Difference approach is more suitable for calculating a 'summary noise'
        # metric. It provides a clearer distinction between the original image's
        # content and the noise.

        # - The high-pass filter method is more abstract and typically used for
        # edge detection or feature enhancement rather than noise
        # quantification.

    # For calculating a single value that summarizes the noise level
    # (summary_noise), using the standard deviation of the noise image obtained
    # from the first method (Gaussian Blur and Absolute Difference) is more
    # appropriate. It gives a more direct and interpretable measure of the
    # noise's variability and intensity in the image.



    # Noise Level (estimate using high-pass filter)
    kernel = np.array([[1, 1, 1], [1, -8, 1], [1, 1, 1]], np.float32)
    noise_estimate = cv2.filter2D(gray_image, -1, kernel)

    # Calculate noise image
    smoothed_image = cv2.GaussianBlur(image, (5, 5), 0)
    noise_image = cv2.absdiff(image, smoothed_image)

    # Calculate the standard deviation (summary noise)
    summary_noise = np.std(noise_image)

    # Sharpness
    laplacian = cv2.Laplacian(gray_image, cv2.CV_64F)
    sharpness = round(cv2.mean(np.abs(laplacian))[0], 2)

    # Metadata (EXIF data)
    try:
        with Image.open(image_path) as img:
            exif_data = img._getexif()
    except:
        exif_data = "Not available"

    return {
        "File Format"          : file_format,
        "Dimensions"           : dimensions,
        "Channels"             : channels,
        "Bit Depth"            : bit_depth,
        "Alpha Channel"        : alpha_channel,
        "Histogram"            : histogram,
        "Contrast"             : contrast,
        "Texture (s.d.)"       : texture,
        "Edge Info"            : edge_count,
        "Noise Level Estimate" : noise_estimate,
        "Summary Noise"        : summary_noise,
        "Sharpness"            : sharpness,
        "EXIF Data"            : exif_data
    }

def plot_and_save_properties(image_path, properties, base_input_dir, base_output_dir):
    # Load image using OpenCV
    original_image = cv2.imread(image_path)
    image_gray = cv2.cvtColor(original_image, cv2.COLOR_BGR2GRAY)

    # Extracting noise
    smoothed_image = cv2.GaussianBlur(image_gray, (5, 5), 0)
    noise_image = cv2.absdiff(image_gray, smoothed_image)

    # Create a 2x2 grid of subplots
    fig, axs = plt.subplots(2, 2, figsize=(12, 10))

    # Display original image
    axs[0, 0].imshow(cv2.cvtColor(original_image, cv2.COLOR_BGR2RGB))  # Convert color from BGR to RGB for display
    axs[0, 0].set_title('Original Image')
    axs[0, 0].axis('off')

    # Plot histogram
    # axs[0, 1].hist(image_gray.ravel(), bins=256, range=[0, 256], density=True)
    axs[0, 1].hist(image_gray.ravel(), bins=256, range=[0, 256])
    axs[0, 1].set_xlim([0, 255])
    axs[0, 1].set_ylim([0.0, 6000]) 
    # axs[0, 1].set_ylim([0.0, 0.2]) 
    axs[0, 1].set_title('Histogram')

    # Display noise image
    axs[1, 0].imshow(noise_image, cmap='gray')
    axs[1, 0].set_title('Noise Image')
    axs[1, 0].axis('off')

    # Text Subplot for properties
    max_key_length = 20
    properties_text = '\n'.join([f'{key.ljust(max_key_length)}: {value}' for key, value in properties.items() if key != 'Histogram' and key != 'Noise Level Estimate'])
    axs[1, 1].text(0.1, 0.5, properties_text, ha='left', va='center', fontsize=13, wrap=True, fontname='Ubuntu Mono')
    axs[1, 1].set_title('Image Properties')
    axs[1, 1].axis('off')


    # Construct the output directory path
    output_dir = os.path.join(base_output_dir, os.path.relpath(os.path.dirname(image_path), base_input_dir))

    # Ensure the output directory exists
    os.makedirs(output_dir, exist_ok=True)

    # Save plot in the output directory
    plot_filename = f"plot-{os.path.basename(image_path).split('.')[0]}.png"

    # Set plot title as the image path
    plt.title(f"Image: {plot_filename}")

    plt.savefig(os.path.join(output_dir, plot_filename))
    plt.close()






for root, dirs, files in os.walk(base_input_dir):
    # Filter and sort files to process
    image_files = sorted([f for f in files if f.lower().endswith(('.png', '.jpg', '.jpeg'))])

    # test = image_files[0:100]
    # for file in tqdm(test, desc="Processing Images"):
    for file in tqdm(image_files, desc="Processing Images"):
        if file.lower().endswith(('.png', '.jpg', '.jpeg')):
            file_path = os.path.join(root, file)
            properties = analyze_image_properties(file_path)
            # save_to_sqlite('image_summary.db', properties, file_path)
            plot_and_save_properties(file_path, properties, base_input_dir, base_output_dir)



