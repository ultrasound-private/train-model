from fastai.vision.all import *
import cv2
import numpy as np


# PIPELINE       = '/mnt/dysk_roboczy/efarell/repos/US-Data/2023-11-12/pipeline-003/'
PIPELINE       = '/mnt/dysk_roboczy/efarell/repos/US-Data/2023-11-12/pipeline-004/'
base_input_dir = os.path.join(PIPELINE, 'data-30-resized')

stats_file = os.path.join(PIPELINE, 'dataset_stats.json')

# Function to calculate mean and standard deviation
def compute_stats(base_input_dir):
    pixel_sum = np.zeros(3)  # For RGB channels
    pixel_sq_sum = np.zeros(3)
    num_pixels = 0

    for root, dirs, files in os.walk(base_input_dir):
        for file in files:
            if file.lower().endswith(('.png', '.jpg', '.jpeg')):
                file_path = os.path.join(root, file)
                image = cv2.imread(file_path)
                image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB) / 255.0  # Normalize pixel values

                pixel_sum += image.sum(axis=(0, 1))
                pixel_sq_sum += (image ** 2).sum(axis=(0, 1))
                num_pixels += image.shape[0] * image.shape[1]

    mean = pixel_sum / num_pixels
    std = np.sqrt((pixel_sq_sum / num_pixels) - (mean ** 2))

    return mean, std


# Compute stats for your dataset
my_dataset_stats = compute_stats(base_input_dir)
print('my_dataset_stats: before saving:')
print(my_dataset_stats)


# Save stats to file
mean, std = my_dataset_stats
stats_dict = {
    'mean': mean.tolist(),  # Convert numpy array to list for JSON compatibility
    'std': std.tolist()
}
stats_json = json.dumps(stats_dict, indent=4)  # `indent=4` for pretty printing

# Write to JSON file
with open(stats_file, 'w') as f:
    f.write(stats_json)


# will be saved like this
# {
    # "mean": [0.485, 0.456, 0.406],  // Example values
    # "std": [0.229, 0.224, 0.225]
# }


print(f"Dataset statistics saved to {stats_file}")


print('my_dataset_stats: AFTER saving:')
with open(stats_file, 'r') as f:
        stats_dict = json.load(f)
my_dataset_stats = (np.array(stats_dict['mean']), np.array(stats_dict['std']))
print(my_dataset_stats)


# when training network, use like this:
batch_tfms = [Normalize.from_stats(*my_dataset_stats)]
