from fastai.vision.all import *
from pathlib import Path
import os

# --------------------------------------------------------------------------------------------
# 
# Need to RESIZE these images in order to compute the IMAGE STATS 
# which get passed into fastai:
#       batch_tfms = [Normalize.from_stats(*my_dataset_stats)]
# 
# --------------------------------------------------------------------------------------------

NEW_SIZE = (256, 256)

# Define the path to your original and new image folders

# PIPELINE = '/mnt/dysk_roboczy/efarell/repos/US-Data/2023-11-12/pipeline-003/'
PIPELINE = '/mnt/dysk_roboczy/efarell/repos/US-Data/2023-11-12/pipeline-004/'

original_folder = os.path.join(PIPELINE, 'data-50')
new_folder      = Path(os.path.join(PIPELINE, 'data-50-resized'))

new_folder.mkdir(parents=True, exist_ok=True)  # Create the new folder


# Define the resize transform
resize_pad = Resize(NEW_SIZE, method=ResizeMethod.Pad, pad_mode='zeros')

# Function to apply transformation and save image
def resize_and_save_image(file_path, dest_folder, resize_transform):
    # Open the image using PIL and convert to greyscale
    img = PILImage.create(file_path).convert('L')
    # img = PILImage.create(file_path)
    # print(img.shape)
    
    # Apply the resize transformation
    resized_img = resize_transform(img)
    print('resized shape ', resized_img.shape)
    print(type(resized_img))

    # Save the transformed image to the new folder
    resized_img.save(dest_folder/file_path.name)

# Loop through each image file in the original folder and apply the transformation
for img_file in get_image_files(original_folder):
    print('processing: ', img_file)
    resize_and_save_image(img_file, new_folder, resize_pad)

print("Resizing and saving completed.")

