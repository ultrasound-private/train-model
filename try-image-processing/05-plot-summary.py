import sqlite3
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

# Path to your SQLite database
db_path = './image-properties.sqlite'

# Connect to the SQLite database
conn = sqlite3.connect(db_path)

# Query to select relevant columns
query = """
SELECT device_name, contrast, texture, edge_info, sharpness, summary_noise
FROM image_properties
ORDER BY device_name
"""

# Executing the query and reading the data into a DataFrame
data = pd.read_sql_query(query, conn)

# Closing the database connection
conn.close()

# Plotting settings
sns.set(style="whitegrid")

# Creating a list of image quality aspects
qualities = ['contrast', 'texture', 'edge_info', 'sharpness', 'summary_noise']

# box plots
# Creating and saving plots for each quality aspect
for quality in qualities:
    print('processing:', quality)
    plt.figure(figsize=(10, 6))
    sns.boxplot(x='device_name', y=quality, data=data)
    plt.title(quality, fontsize=16, fontweight='bold') 
    plt.ylabel(f'{quality.capitalize()}')
    plt.xlabel('Device Name')
    plt.xticks(rotation=45)
    plt.savefig(f'box_plot_{quality}.png', bbox_inches='tight')
    plt.close()


# # facet grids 
# for quality in qualities:
    # print('processing:', quality)
    # g = sns.FacetGrid(data, col="device_name", col_wrap=4, sharex=True, sharey=True)
    # g.map(plt.hist, quality, bins=20, color='skyblue', edgecolor='black')
    # for ax, title in zip(g.axes.flat, g.col_names):
        # ax.set_title(title)
    # plt.subplots_adjust(top=0.9)
    # g.fig.suptitle(f'{quality.capitalize()}', fontsize=16, fontweight='bold')
    # g.savefig(f'facet_grid_{quality}.png')
    # plt.close(g.fig)
