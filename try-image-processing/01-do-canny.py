
import os
from skimage import io, color, feature
import matplotlib.pyplot as plt
import numpy as np

def process_image(image_path, output_path):
    img = io.imread(image_path)
    if len(img.shape) > 2:  # Check if image is not grayscale
        img = color.rgb2gray(img)  # Convert to grayscale

    # print('median:', np.median(img))

    # Adjust sigma and thresholds

    sigma          = 3
    median         = np.median(img)
    # low_threshold  = 0.10 * median
    # high_threshold = 0.20 * median
    low_threshold  = 0.04 * median
    high_threshold = 0.10 * median
    # low_threshold  = 0.9 
    # high_threshold = 1.3

    edge_canny = feature.canny(img, 
                                sigma=sigma, 
                                low_threshold=low_threshold, 
                                high_threshold=high_threshold)

    # # Debugging: Display the image
    # plt.imshow(edge_canny, cmap='gray')
    # plt.show()

    # Create subdirectory if it doesn't exist
    os.makedirs(os.path.dirname(output_path), exist_ok=True)

    plt.imsave(output_path, edge_canny, cmap='gray')

def process_folder(input_folder, output_folder):
    for root, dirs, files in os.walk(input_folder):
        for file in files:
            print('processing:', file)
            if file.lower().endswith(('.png', '.jpg', '.jpeg')):
                input_path = os.path.join(root, file)
                relative_path = os.path.relpath(input_path, input_folder)
                output_path = os.path.join(output_folder, relative_path)
                print(f'Processing: {file}')
                process_image(input_path, output_path)

input_folder = './data-30'
output_folder = './processed_data-30'
process_folder(input_folder, output_folder)

