from fastai.vision.all import *
import cv2
import os
import numpy as np


# PIPELINE       = '/mnt/dysk_roboczy/efarell/repos/US-Data/2023-11-12/pipeline-003/'
PIPELINE       = '/mnt/dysk_roboczy/efarell/repos/US-Data/2023-11-12/pipeline-004/'

# INPUT_DIR = os.path.join(PIPELINE, 'data-30-resized')
# INPUT_DIR = os.path.join(PIPELINE, 'data-50-resized')
# OUTPUT_STATS_FILE = os.path.join(PIPELINE, 'dataset_stats.json')

INPUT_DIR         = '/home/efarell/Extracted-IDs/'
OUTPUT_STATS_FILE = '/home/efarell/Extracted-IDs/small-stats.json'



# Function to calculate mean and standard deviation for greyscale images
def compute_stats(INPUT_DIR):
    pixel_sum = 0  # Single value for greyscale
    pixel_sq_sum = 0
    num_pixels = 0

    for root, dirs, files in os.walk(INPUT_DIR):
        for file in files:
            if file.lower().endswith(('.png', '.jpg', '.jpeg')):
                file_path = os.path.join(root, file)
                image = cv2.imread(file_path, cv2.IMREAD_GRAYSCALE) / 255.0  # Read as greyscale and normalize

                pixel_sum += image.sum()
                pixel_sq_sum += (image ** 2).sum()
                num_pixels += image.size  # Number of pixels in the image

    mean = pixel_sum / num_pixels
    std = np.sqrt((pixel_sq_sum / num_pixels) - (mean ** 2))

    return mean, std

# Compute stats for your dataset
my_dataset_stats = compute_stats(INPUT_DIR)
print('my_dataset_stats: before saving:')
print(my_dataset_stats)


# Save stats to file
mean, std = my_dataset_stats
stats_dict = {
    'mean': mean.tolist(),  # Convert numpy array to list for JSON compatibility
    'std': std.tolist()
}
stats_json = json.dumps(stats_dict, indent=4)  # `indent=4` for pretty printing

# Write to JSON file
with open(OUTPUT_STATS_FILE, 'w') as f:
    f.write(stats_json)


# will be saved like this
# {
    # "mean": [0.485, 0.456, 0.406],  // Example values
    # "std": [0.229, 0.224, 0.225]
# }


print(f"Dataset statistics saved to {OUTPUT_STATS_FILE}")


print('my_dataset_stats: AFTER saving:')
with open(OUTPUT_STATS_FILE, 'r') as f:
        stats_dict = json.load(f)
my_dataset_stats = (np.array(stats_dict['mean']), np.array(stats_dict['std']))
print(my_dataset_stats)


# when training network, use like this:
batch_tfms = [Normalize.from_stats(*my_dataset_stats)]
