
from fastai.vision.all import *
from pathlib import Path
import os

NEW_SIZE = (256, 256)
PIPELINE = '/mnt/dysk_roboczy/efarell/repos/US-Data/2023-11-12/pipeline-004/'

original_folder = os.path.join(PIPELINE, 'data-50')
new_folder = Path(os.path.join(PIPELINE, 'data-50-resized-subfolders'))

resize_pad = Resize(NEW_SIZE, method=ResizeMethod.Pad, pad_mode='zeros')

def replicate_subfolder_structure(original_folder, new_folder, file_path):
    # Create subfolder structure in new_folder based on original_folder
    subfolder_path = new_folder / file_path.relative_to(original_folder).parent
    subfolder_path.mkdir(parents=True, exist_ok=True)
    return subfolder_path

def resize_and_save_image(file_path, resize_transform):
    img = PILImage.create(file_path)
    print(img.shape)
    resized_img = resize_transform(img)
    dest_folder = replicate_subfolder_structure(Path(original_folder), new_folder, file_path)
    resized_img.save(dest_folder / file_path.name)

for img_file in get_image_files(original_folder):
    print('processing: ', img_file)
    resize_and_save_image(img_file, resize_pad)

print("Resizing and saving completed.")
