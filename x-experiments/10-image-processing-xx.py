from PIL import Image as PIL_Image
from tqdm import tqdm
import argparse
import configparser
import csv
import cv2
import numpy as np
import os
import os, sys
import shutil
import utils


AIM = 'Crop images: Dataset: /mnt/pierwsi/Dane/x/'

# Set up argument parser
parser = argparse.ArgumentParser(description=AIM)
parser.add_argument('--config',  required=True,  help='Which config to use')
parser.add_argument('--in_dir',  required=True, help='Name of input folder')
parser.add_argument('--out_dir', required=True, help='Name of output folder')
args = parser.parse_args()

# read ini
config = configparser.ConfigParser(inline_comment_prefixes=";")
config.read(args.config)


# Should we execute this script??
DO_IMAGE_PROCESSING = config.getboolean('PIPELINE', 'DO_IMAGE_PROCESSING')

if not DO_IMAGE_PROCESSING:
    print('\nSkipping Image processing!')
    exit()


# image transformations
CROP_IMAGES          = config.getboolean('PIPELINE', 'CROP_IMAGES')
CONVERT_TO_GREYSCALE = config.getboolean('PIPELINE', 'CONVERT_TO_GREYSCALE')
DRAW_COUNTOURS       = config.getboolean('PIPELINE', 'DRAW_COUNTOURS')
CONTOUR_WIDTH        = config.getint('PIPELINE', 'CONTOUR_WIDTH')
CONTOUR_COLOR        = config.get('PIPELINE', 'CONTOUR_COLOR')
CONTOUR_COLOR        = tuple(map(int, CONTOUR_COLOR.strip('()').split(',')))

# Paths
EXPORT_DATA    = config['PIPELINE']['EXPORT_DATA']
BASE_DIR       = config[EXPORT_DATA]['BASE_DIR']
PIPELINE_DIR   = os.path.join(BASE_DIR, config['PIPELINE']['NAME'])
META_DIR       = config[EXPORT_DATA]['META_DIR']
CSV_METADATA   = os.path.join(META_DIR, config[EXPORT_DATA]['CSV'])
CSV_PREPROCESS = os.path.join(META_DIR, config[EXPORT_DATA]['CSV_PREPROCESS'])

INPUT_DIR    = os.path.join(BASE_DIR,     args.in_dir)
OUTPUT_DIR   = os.path.join(PIPELINE_DIR, args.out_dir)

# Which "type" of folder is INPUT_DIR??
#       type "1": folder .../1_ok_csv      : Image good quality
#       type "2": folder .../2_not_ok_csv  : Image poor quality (scan)
#       type "3": folder .../3_fake_csv    : Image very poor quality
TARGET_FOLDER_TYPE = os.path.basename(INPUT_DIR).split('_')[0]


# show info
utils.print_banner(AIM, os.path.basename(__file__))
print('CSV_METADATA         ', CSV_METADATA)
print('CSV_PREPROCESS       ', CSV_PREPROCESS)
print('')
print('Transformations:')
print('CROP_IMAGES          ', CROP_IMAGES)
print('CONVERT_TO_GREYSCALE ', CONVERT_TO_GREYSCALE)
print('')
print('DRAW_COUNTOURS       ', DRAW_COUNTOURS)
print('CONTOUR_COLOR        ', CONTOUR_COLOR)
print('CONTOUR_WIDTH        ', CONTOUR_WIDTH)
print('')
print('INPUT_DIR            ', INPUT_DIR)
print('OUTPUT_DIR           ', OUTPUT_DIR)
print('TARGET_FOLDER_TYPE   ', TARGET_FOLDER_TYPE)


# Will we go ahead?
response = input("Continue? (y/n): ").lower()
if response == 'n':
    exit()

# Verify input/output folders
utils.assert_dir_exists(INPUT_DIR)
utils.assert_dir_not_empty(INPUT_DIR)
utils.folder_summary(INPUT_DIR)
utils.recreate_dir(OUTPUT_DIR)



# For an ultrasound image, add green 'border' around any tumours in image
# (use the mask 'contours' to draw 'border')
def draw_boundary(img, Tumor_mask_field):
    img_arr = np.array(img)

    # Could be multiple mask files, separated by ";"
    mask_filenames = Tumor_mask_field.split(';')

    # Read each mask file
    # DEBUG: should only add 1 mask here??? not multiple??
    for mask_filename in mask_filenames:
        # Check if filename blank
        if not mask_filename.strip():
            # print(f"Info: No Masks for image {image_name}. Skipping.")
            continue

        mask_path = os.path.join(INPUT_DIR, mask_filename)
        try:
            mask = PIL_Image.open(mask_path)
        except IOError:
            print(f"Warning: Could not open mask {mask_path}. Skipping.")
            continue

        mask_arr = np.array(mask)

        # Convert mask into binary
        # (or else 'cv2.findContours' will fail)
        mask_gray      = cv2.cvtColor(mask_arr, cv2.COLOR_BGR2GRAY)
        _, mask_binary = cv2.threshold(mask_gray, 1, 255, cv2.THRESH_BINARY)

        # Find contours
        contours, _ = cv2.findContours(mask_binary,
                                       cv2.RETR_EXTERNAL,
                                       cv2.CHAIN_APPROX_SIMPLE)

        # Draw contours
        cv2.drawContours(image=img_arr,
                         contours=contours,
                         contourIdx=-1,
                         color=CONTOUR_COLOR,
                         thickness=CONTOUR_WIDTH)

    # Convert to Image to adjust alpha channel
    overlay_img = PIL_Image.fromarray(img_arr)

    # Remove alpha channel...
    # Convert RGBA to RGB
    final_img = overlay_img.convert('RGB')

    # # don't convert? temp debug
    # final_img = overlay_img

    # Convert PIL Image back to NumPy
    final_arr = np.array(final_img)

    return final_arr



# create handy variables
updated_rows = []
total_rows = utils.count_lines(CSV_METADATA) - 1 # subtract header


# Process CSV
with open(CSV_METADATA, 'r', newline='') as csv_file:
    print("\nProcessing...")
    csv_reader = csv.DictReader(csv_file, delimiter=';')

    for row in tqdm(csv_reader, total=total_rows):
        # There's lots of rows in the CSV. 
        # Only process the rows in the folder we're interested in.

        if row['Folder'] != TARGET_FOLDER_TYPE:
            # No processing was done on this file. Skip it
            continue

        image_file = row['ID'] + '.png'
        image_path = os.path.join(INPUT_DIR, image_file)


        if not os.path.isfile(image_path):
            print('File not found: ', image_path)
            continue

        try:
            img = np.array(PIL_Image.open(image_path))
        except IOError:
            print(f"Error: Cannot open: {image_file}.")
            continue

        # Store image size
        row['orig_height']      = img.shape[0]
        row['orig_width']       = img.shape[1]

        # by DEFAULT, assume we won't cut/crop/resize the image
        row['processed_height'] = img.shape[0]
        row['processed_width']  = img.shape[1]


        if DRAW_COUNTOURS:
            # Load image and add boundary countours around ROIs (using masks)
            # img = draw_boundary(INPUT_DIR, row['Image_filename'], row['Tumor_mask_filename'])
            img = draw_boundary(img, row['Tumor_mask_filename'])

        if CROP_IMAGES:
            # store Bounding box
            row_start = int(row['Y1'])
            row_end   = int(row['Y2'])
            col_start = int(row['X1'])
            col_end   = int(row['X2'])

            # crop image
            img = img[row_start:row_end, col_start:col_end]

            # save new coords
            row['processed_height'] = img.shape[0]
            row['processed_width']  = img.shape[1]


        crop_path = os.path.join(OUTPUT_DIR, image_file)

        # Save using cv2
        # --------------

        if CONVERT_TO_GREYSCALE:
            if len(img.shape) == 3 and img.shape[2] == 3:
                # Save image as greyscale
                img_gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
                # Write grayscale image to disk
            else:
                # it's already single channel grayscale
                img_gray = img
            cv2.imwrite(crop_path, img_gray)
        else:
            # Save image in color (BGR)
            # Convert from RGB (used by PIL) to BGR (used by OpenCV)
            img_bgr = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
            cv2.imwrite(crop_path, img_bgr)

        # update METADATA csv
        updated_rows.append(row)



print("Done.")
utils.folder_summary(OUTPUT_DIR)


# Write updated rows to a new CSV file
new_fieldnames = csv_reader.fieldnames + ['orig_width', 'orig_height', 'processed_width', 'processed_height']

with open(CSV_PREPROCESS, 'w', newline='') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames=new_fieldnames, delimiter=';')
    writer.writeheader()
    for row in updated_rows:
        writer.writerow(row)

print(f"New CSV with preprocessing info: {CSV_PREPROCESS}")
