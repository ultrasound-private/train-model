

-- In[]

.schema images

-- "ID"          TEXT,
-- "Case"        INTEGER,
-- "Histology"   TEXT,
-- "Pathology"   INTEGER,
-- "BIRADS"      INTEGER,
-- "Device"      TEXT,
-- "Width"       TEXT,
-- "Height"      INTEGER,
-- "Side"        INTEGER,
-- "BBOX"        TEXT,
-- "X1"          INTEGER,
-- "Y1"          INTEGER,
-- "X2"          INTEGER,
-- "Y2"          INTEGER,
-- "xxx"         INTEGER,
-- "Folder"      INTEGER,
-- "cut_up"      INTEGER


-- In[]

select Folder, count(*) from images group by Folder;

-- Folder  count(*)
-- ------  --------
-- 0       215      ?  <--- ah these are the ones where xxx = 0
-- 1       876      ok_csv
-- 2       703      not_ok_csv
-- 3       81       fake_csv


-- In[]

select xxx, count(*) from images group by xxx;

-- xxx  count(*)
-- ---  --------
-- 0    215
-- 1    1660


-- In[]

-- Same 215 records
select * from images where xxx = 0;
select * from images where Folder = 0;


-- In[]

select Device, count(*) from images group by Device order by count(*) DESC;

-- Device                        count(*)
-- ----------------------------  --------
-- GE Logiq 7 @10-14MHz          902
-- GE Logiq 5 @10-12MHz          809
-- Toshiba Aplio 300 @12-14 MHz  139
-- U-Systems @10-14MHz           25


-- In[]

select Device, Width, count(*) from images 
group by Device, Width 
order by Device, Width;

-- Device                        WIDTH  count(*)
-- ----------------------------  -----  --------
-- GE Logiq 7 @10-14MHz          240    1
-- GE Logiq 7 @10-14MHz          245    1
-- GE Logiq 7 @10-14MHz          248    1
-- GE Logiq 7 @10-14MHz          249    1
-- GE Logiq 7 @10-14MHz          261    2
-- GE Logiq 7 @10-14MHz          262    2
-- GE Logiq 7 @10-14MHz          264    2
-- GE Logiq 7 @10-14MHz          265    2
-- GE Logiq 7 @10-14MHz          266    2
-- GE Logiq 7 @10-14MHz          268    2
-- GE Logiq 7 @10-14MHz          269    4
-- GE Logiq 7 @10-14MHz          270    1
-- GE Logiq 7 @10-14MHz          271    3
-- GE Logiq 7 @10-14MHz          272    8
-- GE Logiq 7 @10-14MHz          273    32
-- GE Logiq 7 @10-14MHz          274    93
-- GE Logiq 7 @10-14MHz          275    168
-- GE Logiq 7 @10-14MHz          276    196
-- GE Logiq 7 @10-14MHz          277    98
-- GE Logiq 7 @10-14MHz          278    34
-- GE Logiq 7 @10-14MHz          279    22
-- GE Logiq 7 @10-14MHz          280    20
-- GE Logiq 7 @10-14MHz          281    20
-- GE Logiq 7 @10-14MHz          282    7
-- GE Logiq 7 @10-14MHz          283    4
-- GE Logiq 7 @10-14MHz          284    16
-- GE Logiq 7 @10-14MHz          285    21
-- GE Logiq 7 @10-14MHz          286    28
-- GE Logiq 7 @10-14MHz          287    26
-- GE Logiq 7 @10-14MHz          288    8
-- GE Logiq 7 @10-14MHz          289    4
-- GE Logiq 7 @10-14MHz          290    2
-- GE Logiq 7 @10-14MHz          293    3
-- GE Logiq 7 @10-14MHz          314    1
-- GE Logiq 7 @10-14MHz          357    1
-- GE Logiq 7 @10-14MHz          358    2
-- GE Logiq 7 @10-14MHz          359    2
-- GE Logiq 7 @10-14MHz          360    8
-- GE Logiq 7 @10-14MHz          361    2
-- GE Logiq 7 @10-14MHz          362    2
-- GE Logiq 7 @10-14MHz          367    1
-- GE Logiq 7 @10-14MHz          375    1
-- GE Logiq 7 @10-14MHz          402    4
-- GE Logiq 7 @10-14MHz          403    7
-- GE Logiq 7 @10-14MHz          404    8
-- GE Logiq 7 @10-14MHz          405    9
-- GE Logiq 7 @10-14MHz          406    4
-- GE Logiq 7 @10-14MHz          407    1
-- GE Logiq 7 @10-14MHz          408    1
-- GE Logiq 7 @10-14MHz          413    1
-- GE Logiq 7 @10-14MHz          415    1
-- GE Logiq 7 @10-14MHz          416    1
-- GE Logiq 7 @10-14MHz          417    1
-- GE Logiq 7 @10-14MHz          439    2
-- GE Logiq 7 @10-14MHz          467    1
-- GE Logiq 7 @10-14MHz          471    2
-- GE Logiq 7 @10-14MHz          485    1
-- GE Logiq 7 @10-14MHz          534    1
-- GE Logiq 7 @10-14MHz          536    1
-- GE Logiq 7 @10-14MHz          540    1
-- GE Logiq 7 @10-14MHz          566    1


-- GE Logiq 5 @10-12MHz          267    1
-- GE Logiq 5 @10-12MHz          268    2
-- GE Logiq 5 @10-12MHz          269    3
-- GE Logiq 5 @10-12MHz          270    12
-- GE Logiq 5 @10-12MHz          271    6
-- GE Logiq 5 @10-12MHz          272    2
-- GE Logiq 5 @10-12MHz          316    2
-- GE Logiq 5 @10-12MHz          317    3
-- GE Logiq 5 @10-12MHz          318    14
-- GE Logiq 5 @10-12MHz          319    39
-- GE Logiq 5 @10-12MHz          320    74
-- GE Logiq 5 @10-12MHz          321    102
-- GE Logiq 5 @10-12MHz          322    133
-- GE Logiq 5 @10-12MHz          323    135
-- GE Logiq 5 @10-12MHz          324    69
-- GE Logiq 5 @10-12MHz          325    25
-- GE Logiq 5 @10-12MHz          326    6
-- GE Logiq 5 @10-12MHz          328    1
-- GE Logiq 5 @10-12MHz          338    2
-- GE Logiq 5 @10-12MHz          343    2
-- GE Logiq 5 @10-12MHz          345    1
-- GE Logiq 5 @10-12MHz          346    1
-- GE Logiq 5 @10-12MHz          361    1
-- GE Logiq 5 @10-12MHz          362    2
-- GE Logiq 5 @10-12MHz          363    1
-- GE Logiq 5 @10-12MHz          379    1
-- GE Logiq 5 @10-12MHz          380    1
-- GE Logiq 5 @10-12MHz          381    1
-- GE Logiq 5 @10-12MHz          382    2
-- GE Logiq 5 @10-12MHz          428    1
-- GE Logiq 5 @10-12MHz          429    4
-- GE Logiq 5 @10-12MHz          430    4
-- GE Logiq 5 @10-12MHz          431    5
-- GE Logiq 5 @10-12MHz          432    6
-- GE Logiq 5 @10-12MHz          433    6
-- GE Logiq 5 @10-12MHz          434    4
-- GE Logiq 5 @10-12MHz          435    3
-- GE Logiq 5 @10-12MHz          457    1
-- GE Logiq 5 @10-12MHz          459    1
-- GE Logiq 5 @10-12MHz          460    2
-- GE Logiq 5 @10-12MHz          461    2
-- GE Logiq 5 @10-12MHz          462    1
-- GE Logiq 5 @10-12MHz          464    1
-- GE Logiq 5 @10-12MHz          563    2
-- GE Logiq 5 @10-12MHz          564    3
-- GE Logiq 5 @10-12MHz          565    8
-- GE Logiq 5 @10-12MHz          566    18
-- GE Logiq 5 @10-12MHz          567    16
-- GE Logiq 5 @10-12MHz          568    27
-- GE Logiq 5 @10-12MHz          569    30
-- GE Logiq 5 @10-12MHz          570    14
-- GE Logiq 5 @10-12MHz          571    5
-- GE Logiq 5 @10-12MHz          572    1


-- Toshiba Aplio 300 @12-14 MHz  275    10
-- Toshiba Aplio 300 @12-14 MHz  276    107
-- Toshiba Aplio 300 @12-14 MHz  277    11
-- Toshiba Aplio 300 @12-14 MHz  411    2
-- Toshiba Aplio 300 @12-14 MHz  413    1
-- Toshiba Aplio 300 @12-14 MHz  462    3
-- Toshiba Aplio 300 @12-14 MHz  463    2
-- Toshiba Aplio 300 @12-14 MHz  528    1
-- Toshiba Aplio 300 @12-14 MHz  529    1
-- Toshiba Aplio 300 @12-14 MHz  550    1

-- U-Systems @10-14MHz           316    6
-- U-Systems @10-14MHz           317    7
-- U-Systems @10-14MHz           318    2
-- U-Systems @10-14MHz           319    2
-- U-Systems @10-14MHz           320    1
-- U-Systems @10-14MHz           383    1
-- U-Systems @10-14MHz           384    1
-- U-Systems @10-14MHz           385    1
-- U-Systems @10-14MHz           386    1
-- U-Systems @10-14MHz           387    2
-- U-Systems @10-14MHz           579    1


-- select Device, Width, Height, count(*) from images 
-- group by Device, Width, Height 
-- order by Device;


-- In[]

select Device, Height, count(*) from images 
group by Device, Height  
order by Device, Height;

-- Device                        HEIGHT  count(*)
-- ----------------------------  ------  --------
-- GE Logiq 5 @10-12MHz          395     2
-- GE Logiq 5 @10-12MHz          396     4
-- GE Logiq 5 @10-12MHz          397     6
-- GE Logiq 5 @10-12MHz          398     8
-- GE Logiq 5 @10-12MHz          399     8
-- GE Logiq 5 @10-12MHz          411     2
-- GE Logiq 5 @10-12MHz          416     2
-- GE Logiq 5 @10-12MHz          419     2
-- GE Logiq 5 @10-12MHz          426     4
-- GE Logiq 5 @10-12MHz          427     2
-- GE Logiq 5 @10-12MHz          428     2
-- GE Logiq 5 @10-12MHz          429     8
-- GE Logiq 5 @10-12MHz          430     22
-- GE Logiq 5 @10-12MHz          431     40
-- GE Logiq 5 @10-12MHz          432     46
-- GE Logiq 5 @10-12MHz          433     60
-- GE Logiq 5 @10-12MHz          434     75
-- GE Logiq 5 @10-12MHz          435     56
-- GE Logiq 5 @10-12MHz          436     20
-- GE Logiq 5 @10-12MHz          437     14
-- GE Logiq 5 @10-12MHz          438     6
-- GE Logiq 5 @10-12MHz          439     4
-- GE Logiq 5 @10-12MHz          440     2
-- GE Logiq 5 @10-12MHz          442     2
-- GE Logiq 5 @10-12MHz          445     2
-- GE Logiq 5 @10-12MHz          447     4
-- GE Logiq 5 @10-12MHz          448     14
-- GE Logiq 5 @10-12MHz          449     4
-- GE Logiq 5 @10-12MHz          450     18
-- GE Logiq 5 @10-12MHz          451     22
-- GE Logiq 5 @10-12MHz          452     30
-- GE Logiq 5 @10-12MHz          453     44
-- GE Logiq 5 @10-12MHz          454     24
-- GE Logiq 5 @10-12MHz          455     36
-- GE Logiq 5 @10-12MHz          456     16
-- GE Logiq 5 @10-12MHz          457     8
-- GE Logiq 5 @10-12MHz          458     2
-- GE Logiq 5 @10-12MHz          459     10
-- GE Logiq 5 @10-12MHz          460     3
-- GE Logiq 5 @10-12MHz          461     6
-- GE Logiq 5 @10-12MHz          462     2
-- GE Logiq 5 @10-12MHz          463     4
-- GE Logiq 5 @10-12MHz          464     8
-- GE Logiq 5 @10-12MHz          465     11
-- GE Logiq 5 @10-12MHz          466     14
-- GE Logiq 5 @10-12MHz          467     13
-- GE Logiq 5 @10-12MHz          468     20
-- GE Logiq 5 @10-12MHz          469     24
-- GE Logiq 5 @10-12MHz          470     32
-- GE Logiq 5 @10-12MHz          471     19
-- GE Logiq 5 @10-12MHz          472     12
-- GE Logiq 5 @10-12MHz          473     5
-- GE Logiq 5 @10-12MHz          474     2
-- GE Logiq 5 @10-12MHz          475     2
-- GE Logiq 5 @10-12MHz          478     1

-- GE Logiq 7 @10-14MHz          268     1
-- GE Logiq 7 @10-14MHz          295     8
-- GE Logiq 7 @10-14MHz          296     18
-- GE Logiq 7 @10-14MHz          297     8
-- GE Logiq 7 @10-14MHz          298     2
-- GE Logiq 7 @10-14MHz          299     2
-- GE Logiq 7 @10-14MHz          308     3
-- GE Logiq 7 @10-14MHz          313     4
-- GE Logiq 7 @10-14MHz          314     10
-- GE Logiq 7 @10-14MHz          315     6
-- GE Logiq 7 @10-14MHz          316     16
-- GE Logiq 7 @10-14MHz          317     2
-- GE Logiq 7 @10-14MHz          318     2
-- GE Logiq 7 @10-14MHz          327     2
-- GE Logiq 7 @10-14MHz          329     2
-- GE Logiq 7 @10-14MHz          330     2
-- GE Logiq 7 @10-14MHz          331     4
-- GE Logiq 7 @10-14MHz          332     18
-- GE Logiq 7 @10-14MHz          333     12
-- GE Logiq 7 @10-14MHz          334     6
-- GE Logiq 7 @10-14MHz          335     4
-- GE Logiq 7 @10-14MHz          336     2
-- GE Logiq 7 @10-14MHz          339     1
-- GE Logiq 7 @10-14MHz          342     2
-- GE Logiq 7 @10-14MHz          343     2
-- GE Logiq 7 @10-14MHz          344     20
-- GE Logiq 7 @10-14MHz          345     9
-- GE Logiq 7 @10-14MHz          346     22
-- GE Logiq 7 @10-14MHz          347     27
-- GE Logiq 7 @10-14MHz          348     23
-- GE Logiq 7 @10-14MHz          349     45
-- GE Logiq 7 @10-14MHz          350     57
-- GE Logiq 7 @10-14MHz          351     57
-- GE Logiq 7 @10-14MHz          352     64
-- GE Logiq 7 @10-14MHz          353     42
-- GE Logiq 7 @10-14MHz          354     19
-- GE Logiq 7 @10-14MHz          355     13
-- GE Logiq 7 @10-14MHz          357     8
-- GE Logiq 7 @10-14MHz          358     2
-- GE Logiq 7 @10-14MHz          359     4
-- GE Logiq 7 @10-14MHz          362     2
-- GE Logiq 7 @10-14MHz          363     2
-- GE Logiq 7 @10-14MHz          364     4
-- GE Logiq 7 @10-14MHz          365     1
-- GE Logiq 7 @10-14MHz          367     6
-- GE Logiq 7 @10-14MHz          368     21
-- GE Logiq 7 @10-14MHz          369     56
-- GE Logiq 7 @10-14MHz          370     43
-- GE Logiq 7 @10-14MHz          371     30
-- GE Logiq 7 @10-14MHz          372     10
-- GE Logiq 7 @10-14MHz          373     6
-- GE Logiq 7 @10-14MHz          381     8
-- GE Logiq 7 @10-14MHz          382     24
-- GE Logiq 7 @10-14MHz          383     39
-- GE Logiq 7 @10-14MHz          384     32
-- GE Logiq 7 @10-14MHz          385     28
-- GE Logiq 7 @10-14MHz          386     16
-- GE Logiq 7 @10-14MHz          387     5
-- GE Logiq 7 @10-14MHz          388     1
-- GE Logiq 7 @10-14MHz          389     7
-- GE Logiq 7 @10-14MHz          390     2
-- GE Logiq 7 @10-14MHz          406     2
-- GE Logiq 7 @10-14MHz          407     2
-- GE Logiq 7 @10-14MHz          408     2
-- GE Logiq 7 @10-14MHz          409     2

-- Toshiba Aplio 300 @12-14 MHz  301     2
-- Toshiba Aplio 300 @12-14 MHz  302     6
-- Toshiba Aplio 300 @12-14 MHz  303     23
-- Toshiba Aplio 300 @12-14 MHz  304     34
-- Toshiba Aplio 300 @12-14 MHz  305     35
-- Toshiba Aplio 300 @12-14 MHz  306     15
-- Toshiba Aplio 300 @12-14 MHz  307     2
-- Toshiba Aplio 300 @12-14 MHz  308     2
-- Toshiba Aplio 300 @12-14 MHz  309     8
-- Toshiba Aplio 300 @12-14 MHz  311     4
-- Toshiba Aplio 300 @12-14 MHz  313     2
-- Toshiba Aplio 300 @12-14 MHz  314     2
-- Toshiba Aplio 300 @12-14 MHz  315     2
-- Toshiba Aplio 300 @12-14 MHz  317     2

-- U-Systems @10-14MHz           353     1
-- U-Systems @10-14MHz           393     2
-- U-Systems @10-14MHz           403     2
-- U-Systems @10-14MHz           404     3
-- U-Systems @10-14MHz           405     3
-- U-Systems @10-14MHz           407     4
-- U-Systems @10-14MHz           408     2
-- U-Systems @10-14MHz           409     1
-- U-Systems @10-14MHz           410     2
-- U-Systems @10-14MHz           411     2
-- U-Systems @10-14MHz           441     2
-- U-Systems @10-14MHz           473     1


-- In[]

