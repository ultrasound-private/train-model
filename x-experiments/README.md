# Task Board:

- iWork Task: https://best.ippt.pan.pl/iwork/?controller=TaskViewController&action=show&task_id=956

# File locations

- See the output from the experimental model: [OUTPUT images](./OUTPUT)

    - INPUT data
        - /mnt/dysk_roboczy/efarell/repos/US-Data/x

    - OUTPUT data
        - {OUTPUT_ROOT} = /mnt/dysk_roboczy/efarell/repos/output_xx

# TRAINED on data with markings; 

- TESTED on data with markings
    - {OUTPUT_ROOT}/convnext_tiny.fb_in22k--pipeline-006-WITH-xx/plot-test-old/

- TESTED on data __without__ markings
    - {OUTPUT_ROOT}/convnext_tiny.fb_in22k--pipeline-006-WITH-xx/plot-test-criss-cross

# TRAINED on data without markings

- TESTED on data without markings
    - {OUTPUT_ROOT}/convnext_tiny.fb_in22k--pipeline-007-no-xx/plot-test-old/

- TESTED on data __with__ markings
    - {OUTPUT_ROOT}/convnext_tiny.fb_in22k--pipeline-007-no-xx/plot-test-criss-cross

# Training script

- Execute this to train the model:
    - [./do_training.sh](./do_training.sh)

# Aim:
    - We have removed markings from a dataset which contains
      a lot of image annotation.

    - It'd be interesting to test how these markings
      INFLUENCE classification

    - ie: What network sees when:
        - trained on data WITHOUT markings ; tested on data without markings
        - trained on data WITHOUT markings ; tested on data with markings
        - trained on data WITH markings    ; tested on data with markings
        - trained on data WITH markings    ; tested on data without markings


# 'FOLDER' field:
    - Signifies quality
        - `1`:  folder `1_ok_csv`      Image is good quality
        - `2`:  folder `2_not_ok_csv`  Image is poor quality (scan)
        - `3`:  folder `3_fake_csv`    Image very poor quality, contains superfluous objects

# Dataset Locations
    - Original Dataset in:
        - `/mnt/pierwsi/Dane/x/`

    - MY COPY:
        - /mnt/dysk_roboczy/efarell/repos/US-Data/x/1_ok_csv_withxx/
        - /mnt/dysk_roboczy/efarell/repos/US-Data/x/1_ok_csv_withoutxx/
        - /mnt/dysk_roboczy/efarell/repos/US-Data/x/1_ok_csv_masks/

        - /mnt/dysk_roboczy/efarell/repos/US-Data/x/2_not_ok_csv_withxx/
        - /mnt/dysk_roboczy/efarell/repos/US-Data/x/2_not_ok_csv_withoutxx/
        - /mnt/dysk_roboczy/efarell/repos/US-Data/x/2_not_ok_csv_masks/

        - /mnt/dysk_roboczy/efarell/repos/US-Data/x/3_fake_csv_withxx/
        - /mnt/dysk_roboczy/efarell/repos/US-Data/x/3_fake_csv_withoutxx/
        - /mnt/dysk_roboczy/efarell/repos/US-Data/x/3_fake_csv_masks/


# CSV file description:

# 'xxx' column,
    - value `1`:
        - the image contained crosses

    - value `0`:
        - Had no crosses and we don't do further calculations.
          (it does not have a bounding box determined)
        - These cases will also have 'Folder' = 0.

# columns X1, Y1, X2, Y2
    - bounding box
    - `X1`, `Y1` (top left corner)
    - `X2`, `Y2` (bottom right corner)
    - (note : coordinates counted from 1 and not from 0)

# 'cut_up' column
    - `1`: image cut from the top by the bounding box

# Pipelines
    - `pipeline-006-WITH-xx`  original images with markings
    - `pipeline-007-no-xx`    markings removed


# CSV Fields
- CSV file: [./metadata/save_lv8.csv](./metadata/save_lv8.csv)
- (csv delimter = ';')

    - "ID"          TEXT
    - "Case"        INTEGER
    - "Histology"   TEXT
    - "Pathology"   INTEGER
    - "BIRADS"      INTEGER
    - "Device"      TEXT
    - "Width"       TEXT
    - "Height"      INTEGER
    - "Side"        INTEGER
    - "BBOX"        TEXT
    - "X1"          INTEGER
    - "Y1"          INTEGER
    - "X2"          INTEGER
    - "Y2"          INTEGER
    - "xxx"         INTEGER
    - "Folder"      INTEGER
    - "cut_up"      INTEGER
