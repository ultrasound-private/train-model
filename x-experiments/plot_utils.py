import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
import matplotlib.gridspec as gridspec
import numpy as np
import os
from fastai.vision.all import Path

import configparser

import cv2
from pytorch_grad_cam                     import (GradCAM, LayerCAM, GradCAMElementWise)
from pytorch_grad_cam                     import GuidedBackpropReLUModel
from pytorch_grad_cam.utils.image         import (show_cam_on_image, deprocess_image, preprocess_image)
from pytorch_grad_cam.utils.model_targets import ClassifierOutputTarget

import seaborn as sns
from sklearn.metrics import confusion_matrix, precision_recall_curve, auc, roc_curve

from sklearn.preprocessing import label_binarize
from matplotlib.font_manager import FontProperties


def plot_auc(probs, targets, class_labels, plot_title, model_export_name, plot_base_dir):
    """
    Generate ROC AUC plot for both binary and multi-class classification.

    Args:
    - probs (array)           : Predicted probabilities for each class.
    - targets (array)         : True target labels.
    - class_labels (list)     : List of class labels.
    - plot_title (str)        : Title for the plot.
    - model_export_name (str) : Model name for saving the plot.
    """

    # Read ini
    config = configparser.ConfigParser(inline_comment_prefixes=";")
    config.read(CONFIG)

    # Settings
    EXPORT_DATA           = config['MODEL']['EXPORT_DATA'] # Nov, June etc
    BASE_DIR              = config[EXPORT_DATA]['BASE_DIR']
    RUN_COMMENT           = config.get('MODEL','COMMENT')
    MODEL_EXPORT_NAME     = config.get('MODEL','MODEL_EXPORT_NAME').replace('.', '_')
    TRAIN_NEW_MODEL       = config.getboolean('MODEL', 'TRAIN_NEW_MODEL')
    BATCH_SIZE            = int(config['MODEL']['BATCH_SIZE'])
    TRAINING_EPOCHS       = int(config['MODEL']['TRAINING_EPOCHS'])
    item_tfms_IMAGE_SIZE  = config['MODEL']['item_tfms_IMAGE_SIZE']
    batch_tfms_IMAGE_SIZE = config['MODEL']['batch_tfms_IMAGE_SIZE']
    item_tfms_IMAGE_SIZE  = tuple(map(int, item_tfms_IMAGE_SIZE.strip('()').split(',')))
    batch_tfms_IMAGE_SIZE = tuple(map(int, batch_tfms_IMAGE_SIZE.strip('()').split(',')))
    TRAIN_DATA            = os.path.join(BASE_DIR, config['MODEL']['TRAIN_DATA'])
    TEST_DATA             = os.path.join(BASE_DIR, config['MODEL']['TEST_DATA'])


    str_train_dir = '/'.join(Path(TRAIN_DATA).parts[-3:])
    str_test_dir  = '/'.join(Path(TEST_DATA).parts[-3:])

    title_extra  = '\n'
    title_extra += 'Model:    ' + MODEL_EXPORT_NAME + '\n'
    title_extra += 'Comment:  ' + RUN_COMMENT       + '\n'
    title_extra += 'Train:    ' + str_train_dir     + '\n'
    title_extra += 'Test:     ' + str_test_dir


    # Dynamically determine number of classes
    unique_classes = np.unique(targets)
    n_classes      = len(unique_classes)

    # Dynamically binarize the output labels for multi-class
    true_labels_bin = label_binarize(targets, classes=unique_classes)

    # Check if it's a binary classification
    is_binary_classification = n_classes == 2

    # Compute ROC curve and ROC area for each class
    fpr, tpr, roc_auc = dict(), dict(), dict()
    if is_binary_classification:
        # assume 'malignant' is in index 1 in 'probs' array
        positive_class = 1
        fpr[0], tpr[0], _ = roc_curve(true_labels_bin, probs[:, positive_class])
        roc_auc[0] = auc(fpr[0], tpr[0])
    else:
        for i in range(n_classes):
            fpr[i], tpr[i], _ = roc_curve(true_labels_bin[:, i], probs[:, i])
            roc_auc[i] = auc(fpr[i], tpr[i])

    # Plot settings
    colors = ['Green', 'Orange', 'Red', 'Blue', 'Purple', 'Cyan'][:n_classes]
    font_prop = FontProperties(family='DejaVu Sans Mono', size=14)

    plt.figure(figsize=(12, 8))

    if is_binary_classification:
        plt.plot(fpr[0], tpr[0], color='blue', lw=3, label=f'ROC: {class_labels[1]:<15} (area: {roc_auc[0]:.3f})')
    else:
        for i, color in zip(range(n_classes), colors):
            plt.plot(fpr[i], tpr[i], color=color, lw=3, label=f'ROC: {class_labels[i]:<15} (area: {roc_auc[i]:.3f})')



    plt.plot([0, 1], [0, 1], 'k--', lw=2)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title(plot_title + title_extra, loc='left', fontname="DejaVu Sans Mono")
    plt.legend(loc="lower right", prop=font_prop)

    # Save plot to disk
    plot_path = os.path.join(plot_base_dir, f'ROC-{model_export_name}.png')
    print(f'Saving ROC plot: {plot_path}')
    plt.savefig(plot_path)
    plt.close()


def create_cam (rgb_img, learner, target_idx_lesion, target_idx_birads):
    # LayerCAM: Exploring Hierarchical Class Activation Maps for Localization
    # https://ieeexplore.ieee.org/document/9462463
    cam_algorithm = LayerCAM
    # cam_algorithm = GradCAM
    # cam_algorithm = GradCAMElementWise


    # Convnext models
    target_layer_id = "0.model.stages.3.blocks.2"
    # target_layer_id = "0.model.stages.3.blocks.2.conv_dw"

    # Resnet models
     # target_layer_id = "0.7.2"
     # target_layer_id = "0.7.2.conv2"


    # Get target layer and convert to list
    target_layers = [learner.get_submodule(target_layer_id)]

    # Specify which target to generate the CAM for.
    # If targets is "None", the highest scoring category (for every member in the batch) will be used.
    # targets = None
    lesion_targets = [ClassifierOutputTarget(target_idx_lesion)]
    birads_targets = [ClassifierOutputTarget(target_idx_birads)]


    input_tensor = preprocess_image(rgb_img,
                                    mean=[0.485, 0.456, 0.406],
                                    std=[0.229, 0.224, 0.225])


    with cam_algorithm(model         = learner.model,
                        target_layers = target_layers,
                        use_cuda      = True) as cam:

        # Two versions of CAM plot:
        #   'cam_image_lesion': CAM for 'benign' or 'malignant'
        #   'cam_image_birads': CAM for birads '4a', '4b', '5' etc

        grayscale_cam_lesion = cam(input_tensor = input_tensor,
                                   targets      = lesion_targets,
                                   aug_smooth   = False,
                                   eigen_smooth = False)


        grayscale_cam_birads = cam(input_tensor = input_tensor,
                                   targets      = birads_targets,
                                   aug_smooth   = False,
                                   eigen_smooth = False)

        grayscale_cam_lesion = grayscale_cam_lesion[0, :]
        grayscale_cam_birads = grayscale_cam_birads[0, :]

        cam_image_lesion = show_cam_on_image(rgb_img, grayscale_cam_lesion, use_rgb=True)
        cam_image_birads = show_cam_on_image(rgb_img, grayscale_cam_birads, use_rgb=True)


        # cam_image = cv2.cvtColor(cam_image, cv2.COLOR_RGB2BGR)

        return (cam_image_lesion, cam_image_birads)



def plot_prediction_multi(main_title, image_path, learner, lesion_outcome, birads_outcome, pred_class_lesion, pred_class_birads, true_class_lesion, true_class_birads, probabilities, plot_base_dir):
    # Assuming probabilities is a list of two lists, one for each group of classes
    prob_lesion, prob_birads = probabilities

    # Extract classes for each group
    lesion_classes = list(learner.dls.vocab[0])
    birads_classes = list(learner.dls.vocab[1])


    # Determine the index of the true classes in their respective groups
    true_idx_lesion = lesion_classes.index(true_class_lesion) if true_class_lesion in lesion_classes else -1
    true_idx_birads = birads_classes.index(true_class_birads) if true_class_birads in birads_classes else -1
    pred_idx_lesion = lesion_classes.index(pred_class_lesion) if pred_class_lesion in lesion_classes else -1
    pred_idx_birads = birads_classes.index(pred_class_birads) if pred_class_birads in birads_classes else -1

    # print('predicted:', pred_class_lesion, pred_idx_lesion)
    # print('true:     ', true_class_lesion, true_idx_lesion)

    # Save plot: Class Activation Map
    if lesion_outcome == 'Bad' or birads_outcome == 'Bad':
        plot_dir = os.path.join(plot_base_dir, 'bad')
    else:
        plot_dir = os.path.join(plot_base_dir, 'good')


    # Read the image using cv2.imread and convert it to RGB format
    rgb_img = cv2.imread(str(image_path), cv2.IMREAD_COLOR)[:, :, ::-1]

    # Create grayscale copy too
    image_gray = cv2.cvtColor(rgb_img, cv2.COLOR_RGB2GRAY)

    # Now normalise rgb_img
    rgb_img = np.float32(rgb_img) / 255


    # create class activation map of the PREDICTIONS (i.e. the highest prob)
    cam_image_lesion, cam_image_birads = create_cam(rgb_img, learner, pred_idx_lesion, pred_idx_birads)

    # Create fig
    fig = plt.figure(figsize=(12, 10))
    # GridSpec: 3 rows, 2 columns
    gs = gridspec.GridSpec(3, 2, height_ratios=[0.7, 1, 3])
    lesion_colors = ['darkseagreen', 'rosybrown']

    main_title += '\n' + os.path.basename(image_path)
    fig.suptitle(main_title, fontsize=18, y=1.05)

    # lesion type: Create bar chart  ('benign' etc)
    ax0 = plt.subplot(gs[0, 0])
    y_pos_lesion = range(len(lesion_classes))
    ax0.barh(y_pos_lesion, prob_lesion, color=lesion_colors)
    ax0.set_yticks(y_pos_lesion)
    ax0.set_yticklabels(lesion_classes, fontweight='bold', fontsize='large')
    ax0.set_xlim(0, 1)
    ax0.set_xticks([0, 0.25, 0.5, 0.75])  # Set x-ticks
    ax0.grid(axis='x', color='lightgrey', linestyle='-', linewidth=0.5)  # Add vertical gridlines
    ax0.tick_params(axis='both', which='both', length=0)
    ax0.set_title('Lesion Probability', fontsize=14)


    # BIRADS rating: Create 2nd bar chart (share x-axis with 1st bar chart)
    ax1 = plt.subplot(gs[1, 0], sharex=ax0)
    # Define custom order for the y-ticks and labels
    custom_order = ['LOW', 'MODERATE', 'HIGH']
    custom_y_pos = range(len(custom_order))
    # Map probabilities to the custom order
    # Assumes: prob_birads is ordered as in birads_classes
    # Adjust if 'prob_birads' is not in same order as 'birads_classes'
    mapped_probabilities = [prob_birads[birads_classes.index(rating)] for rating in custom_order]
    ax1.barh(custom_y_pos, mapped_probabilities, color='lightsteelblue')
    ax1.set_yticks(custom_y_pos)
    ax1.set_yticklabels(custom_order, fontweight='bold', fontsize='large')
    ax1.set_xlim(0, 1)
    ax1.grid(axis='x', color='lightgrey', linestyle='-', linewidth=0.5)  # Add vertical gridlines
    ax1.tick_params(axis='both', which='both', length=0)
    ax1.set_title('BIRADS Probability', fontsize=14)


    # lesion type: Mark ground truth
    for idx, value in enumerate(prob_lesion):
        # Position the text at the end of each bar
        ax0.text(value, idx, ' True' if idx == true_idx_lesion else '',
                va='center', color='gray', fontweight='bold')

    # birads: mark ground truth
    true_class_position_birads = custom_order.index(birads_classes[true_idx_birads]) if true_idx_birads != -1 else -1
    for idx, value in enumerate(mapped_probabilities):
        # Position the text at the end of each bar
        ax1.text(value, idx, ' True' if idx == true_class_position_birads else '',
                va='center', color='gray', fontweight='bold')


    # Plot pixel histogram
    ax_pixel = plt.subplot(gs[0:2, 1])
    # ax_pixel.hist(image_gray.ravel(), bins=256, range=[0, 256], density=True)
    # ax_pixel.set_ylim([0.0, 0.2])
    ax_pixel.hist(image_gray.ravel(), bins=256, range=[0, 256])
    ax_pixel.set_ylim([0.0, 6000])
    ax_pixel.set_xlim([0, 255])
    ax_pixel.set_title('Pixel Intensity')


    # First image (Original)
    ax_img = plt.subplot(gs[2, 1])
    ax_img.imshow(rgb_img)
    ax_img.set_title('Original')
    ax_img.axis('off')


    # CAM overlay
    ax_cam = plt.subplot(gs[2, 0])
    # ax_cam.imshow(cam_image_birads)
    ax_cam.imshow(cam_image_lesion)
    ax_cam.set_title('Class Activation Map (lesion)')
    ax_cam.axis('off')



    # Save to file
    plt.subplots_adjust(top=0.85)
    plt.tight_layout()
    cam_image_path = os.path.join(plot_dir, os.path.basename(image_path))
    plt.savefig(cam_image_path, bbox_inches='tight')
    plt.close()  # free memory
    print(f'Saved: {cam_image_path}')


def plot_confusion_matrix(true_labels, predicted_labels, classes, title, ax=None):
    cm = confusion_matrix(true_labels, predicted_labels, labels=classes)
    sns.heatmap(cm, annot=True, fmt='d', cmap='Blues', xticklabels=classes, yticklabels=classes, ax=ax, annot_kws={"size": 18}, cbar=False)
    
    # Set the labels and titles with increased font size
    ax.set_xlabel('Predicted', fontsize=18, fontweight='bold')
    ax.set_ylabel('True', fontsize=18, fontweight='bold')
    ax.set_title(title, fontsize=16) 

    # Increase the font size of the tick labels
    ax.set_xticklabels(ax.get_xticklabels(), fontsize=12)
    ax.set_yticklabels(ax.get_yticklabels(), fontsize=12)


def plot_probability_distribution(data, prob_columns, title, ax=None, colors=None):
    if colors is None:
        colors = ['blue', 'orange', 'green', 'red', 'purple', 'brown', 'pink', 'gray', 'olive', 'cyan']  # Default colors
    for col, color in zip(prob_columns, colors):
        sns.kdeplot(data[col], ax=ax, fill=True, common_norm=False, alpha=.5, linewidth=1.5, color=color, label=col)
    ax.set_title(title, fontsize=16)
    ax.set_xlabel('Probability', fontsize=16)
    ax.set_ylabel(None)
    ax.legend()


# Function to plot Precision-Recall Curve
def plot_precision_recall_curve(true_labels, probabilities, title, ax=None):
    precision, recall, thresholds = precision_recall_curve(true_labels, probabilities)
    ax.plot(recall, precision, marker='.')
    ax.set_xlabel('Recall', fontsize=16)
    ax.set_ylabel('Precision', fontsize=16)
    ax.set_title(title, fontsize=16)
    auc_score = auc(recall, precision)
    ax.legend([f'AUC={auc_score:.2f}'], fontsize=18)


def plot_lesion_probs_with_errors(data, title, ax=None):
    # Plot the KDE for the benign predictions
    # sns.kdeplot(data['lesion_prob_benign'], ax=ax, fill=True, color='green', label='Benign', alpha=.5)

    # Plot the KDE for the malignant predictions
    sns.kdeplot(data['lesion_prob_malignant'], ax=ax, fill=True, color='red', label='Malignant', alpha=.5)

    # Identify the false negatives
    false_negatives = data[(data['lesion_prob_malignant'] < 0.5) & (data['lesion_true'] == 'malignant')]

    # Overlay false negatives as larger, labeled points on the plot
    for i, point in false_negatives.iterrows():
        # ax.scatter(point['lesion_prob_malignant'], 0, color='blue', s=30, zorder=5)  # s controls the size of the marker
        ax.scatter(point['lesion_prob_malignant'], point['lesion_prob_malignant'], color='blue', s=30, zorder=5)  # s controls the size of the marker

    ax.set_title(title, fontsize=16) 
    ax.set_xlabel('Probability', fontsize=16) 
    # ax.set_ylabel('Density')
    ax.set_ylabel(None)
    ax.legend()


def plot_probabilities_2d_scatter(data, ax=None):
    # Points where the prediction was correct
    correct_predictions = data[data['lesion_outcome'] == 'Good']
    # Points where the prediction was Wrong
    wrong_predictions = data[data['lesion_outcome'] == 'Bad']

    # Plot correct predictions in green
    ax.scatter(correct_predictions['lesion_prob_benign'], correct_predictions['lesion_prob_malignant'], color='green', label='Correct', alpha=0.5)

    # Plot Wrong predictions in red
    ax.scatter(wrong_predictions['lesion_prob_benign'], wrong_predictions['lesion_prob_malignant'], color='red', label='Wrong', alpha=0.8)

    ax.set_xlabel('Benign Probability')
    ax.set_ylabel('Malignant Probability')
    ax.set_title('2D Probability Space of Lesion Predictions')
    ax.legend()


def plot_probability_difference_vs_max(data, ax=None):
    # Calculate the difference and max probability
    data['prob_diff'] = data['lesion_prob_benign'] - data['lesion_prob_malignant']
    data['prob_max'] = data[['lesion_prob_benign', 'lesion_prob_malignant']].max(axis=1)

    # Points where the prediction was correct
    correct_predictions = data[data['lesion_outcome'] == 'Good']
    # Points where the prediction was Wrong
    wrong_predictions = data[data['lesion_outcome'] == 'Bad']

    # Plot correct predictions in green
    ax.scatter(correct_predictions['prob_diff'], correct_predictions['prob_max'], color='green', label='Correct', alpha=0.5)

    # Plot Wrong predictions in red
    ax.scatter(wrong_predictions['prob_diff'], wrong_predictions['prob_max'], color='red', label='Wrong', alpha=0.5)

    ax.set_xlabel('Probability Difference (Benign - Malignant)')
    ax.set_ylabel('Max Probability')
    ax.set_title('Probability Difference vs Max Probability')
    ax.legend()

def plots_probs_v_cases(data, ax):
    # Separate correct and Wrong predictions
    correct_predictions = data[data['lesion_outcome'] == 'Good']
    wrong_predictions   = data[data['lesion_outcome'] == 'Bad']

    # # Plot benign probabilities
    # ax1.scatter(correct_predictions.index, correct_predictions['lesion_prob_benign'], color='green', label='Correct', alpha=0.5)
    # ax1.scatter(wrong_predictions.index, wrong_predictions['lesion_prob_benign'], color='red', label='Wrong', alpha=0.5)
    # ax1.set_title('Benign Probability vs. Test Cases')
    # ax1.set_xlabel('Index')
    # ax1.set_ylabel('Benign Probability')

    # Plot malignant probabilities
    ax.scatter(correct_predictions.index, correct_predictions['lesion_prob_malignant'], color='green', label='Correct', alpha=0.5)
    ax.scatter(wrong_predictions.index, wrong_predictions['lesion_prob_malignant'],     color='red',   label='Wrong',   alpha=0.8)
    ax.set_title('Malignant Probability vs. Cases', fontsize=16)
    ax.set_xlabel('Test Case', fontsize=14)
    ax.set_ylabel('Malignant Probability', fontsize=16)


    # Set legends
    # ax1.legend()
    ax.legend()
