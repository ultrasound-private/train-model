#   Aim:
#   RENAME image files
#
#       Use: FILENAME_FORMAT = "{type}_{birads}--ID_{image_id}--{width}x{height}--{device}{ext}"
#


import csv
import os, sys
import shutil
from PIL import Image
import argparse
import configparser
import utils

# Get handy CONSTANTS
from CONSTANTS import  BENCHMARK_IMAGES
from CONSTANTS import  FILENAME_FORMAT
from CONSTANTS import  DEVICE_MAP
from CONSTANTS import  BAD_IMAGE_IDS


AIM = 'Rename images, remove BAD images, organise folders (BENIGN, MALIGNANT)'

# Set up argument parser
parser = argparse.ArgumentParser(description=AIM)
parser.add_argument('--config',  required=True,  help='Which config to use')
parser.add_argument('--in_dir',  required=True, help='Name of input folder')
parser.add_argument('--out_dir', required=True, help='Name of output folder')
args = parser.parse_args()

# Subfolders to organise / catagorise all images
BAD_DATA_FOLDER  = "BAD-DATA"
VALID_FOLDER     = "VALID-FOR-TRAINING"
NEW_SUBFOLDERS   = [VALID_FOLDER, BAD_DATA_FOLDER]


config = configparser.ConfigParser(inline_comment_prefixes=";")
config.read(args.config)

# Define paths/files
EXPORT_DATA  = config['PIPELINE']['EXPORT_DATA']
BASE_DIR     = config[EXPORT_DATA]['BASE_DIR']
META_DIR     = config[EXPORT_DATA]['META_DIR']
PIPELINE_DIR = os.path.join(BASE_DIR, config['PIPELINE']['NAME'])
INPUT_DIR    = os.path.join(PIPELINE_DIR, args.in_dir)
OUTPUT_DIR   = os.path.join(PIPELINE_DIR, args.out_dir)
CSV_META     = os.path.join(META_DIR, config[EXPORT_DATA]['CSV_PREPROCESS'])

utils.print_banner(AIM, os.path.basename(__file__))
print('')
print('EXPORT_DATA ', EXPORT_DATA)
print('INPUT_DIR   ', INPUT_DIR)
print('OUTPUT_DIR  ', OUTPUT_DIR)
print('CSV_META  ', CSV_META)
print('')


# Setup input/output folders
utils.assert_dir_exists(INPUT_DIR)
utils.assert_dir_not_empty(INPUT_DIR)
utils.folder_summary(INPUT_DIR)
utils.recreate_dir(OUTPUT_DIR)


# Create subfolders
for o in NEW_SUBFOLDERS:
    os.makedirs(os.path.join(OUTPUT_DIR, o), exist_ok=True)


# Process CSV
with open(CSV_META, 'r', newline='') as csv_file:
    csv_reader = csv.DictReader(csv_file, delimiter=';')
    print("\nProcessing...")

    for row in csv_reader:
        # More readable device name
        row['Device_name_short'] = DEVICE_MAP.get(row['Device'], row['Device'])

        # Get image extension (.png, .jpg etc)
        file_ext  = '.png'
        image_file = row['ID'] + file_ext

        # Create new filename
        new_name = FILENAME_FORMAT.format(
                type     = row['Pathology'],
                birads   = row['BIRADS'],
                image_id = row['ID'].replace("_", "").replace("-", ""),
                width    = row['processed_width'],
                height   = row['processed_height'],
                device   = row['Device_name_short'],
                ext      = file_ext
                )

        # move/rename file
        src  = os.path.join(INPUT_DIR, image_file)

        if row['ID'] in BAD_IMAGE_IDS:
            # move bad data to separate folder
            # (don't use for training)
            # print(f"Bad image: {row['ImageID']:<12} {new_name}")
            dest = os.path.join(OUTPUT_DIR, BAD_DATA_FOLDER, new_name)

        else:
            dest = os.path.join(OUTPUT_DIR, VALID_FOLDER, new_name)

        # execute move
        shutil.copy(src, dest)


print("Re-org done!")
utils.folder_summary(OUTPUT_DIR)
