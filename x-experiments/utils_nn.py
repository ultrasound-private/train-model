import torch.nn.functional as F

from fastai.vision.all import first
from fastai.vision.all import PILImage
from fastai.vision.all import Hook
from fastai.vision.all import Path
from fastai.vision.all import partial
from fastai.vision.all import error_rate


import os
from PIL import Image
import re

import matplotlib
from matplotlib.font_manager import FontProperties
matplotlib.use('Agg') # non-interactive backend
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

from skimage.transform import resize
from datetime import datetime
import utils

import configparser
from CONSTANTS import FILENAME_FORMAT


import numpy as np
from itertools import cycle
from sklearn.preprocessing import label_binarize
from sklearn.metrics import roc_curve, auc
from pathlib import Path
import pandas as pd


# Location of CONFIG file
# Will be set in one of the 'main' calling scripts
CONFIG = None


def generate_regex_pattern(filename_format, keyword):
    # Adjust for the file extension
    filename_format = filename_format.replace("{ext}", ".{ext}")
    pattern = filename_format
    placeholders = ['type', 'birads', 'image_id', 'width', 'height', 'device', 'ext']
    for placeholder in placeholders:
        if placeholder == keyword:
            pattern = pattern.replace(f'{{{placeholder}}}', f'(?P<{placeholder}>[^--]*)')
        else:
            pattern = pattern.replace(f'{{{placeholder}}}', '[^--]*')
    pattern = pattern.replace('.', '\.')  # Escape the period for the file extension
    return pattern


def generate_regex_pattern_xx_OLD(filename_format, keyword):
    # Escape special characters and adjust for the file extension
    filename_format = filename_format.replace("{ext}", "\.(?P<ext>[^.]*)")
    placeholders = ['type', 'birads', 'image_id', 'width', 'height', 'device']

    for placeholder in placeholders:
        if placeholder == keyword:
            pattern = filename_format.replace(f'{{{placeholder}}}', f'(?P<{placeholder}>[^_]+)')
        else:
            # Allow for additional characters like '-' in 'image_id'
            pattern = filename_format.replace(f'{{{placeholder}}}', '[^--]+')

    return f'^{pattern}$'  # Ensure the entire filename matches the pattern


def generate_regex_pattern_xx(filename_format, keyword):
    placeholders = {
        'type': '(?P<type>[^_]+)',
        'birads': '(?P<birads>[^_]+)',
        'image_id': '(?P<image_id>[^-]+)',
        'width': '(?P<width>[^x]+)',
        'height': '(?P<height>[^-]+)',
        'device': '(?P<device>[^.]+)',
        'ext': '\.(?P<ext>[^.]*)'
    }

    # Replace each placeholder in the format string with its regex pattern
    pattern = filename_format
    for key, value in placeholders.items():
        pattern = pattern.replace('{' + key + '}', value if key == keyword else '[^_]+')

    return f'^{pattern}$'




def get_images(path, device_keyword=None):
    matching_images = []

    if device_keyword == 'ALL':
        # take everything
        for img_path in Path(path).rglob('*'):
            matching_images.append(img_path)
    else:
        # return a list of image files based on DEVICE type
        regex = re.compile(generate_regex_pattern(FILENAME_FORMAT, 'device'))
        for img_path in Path(path).rglob('*'):
            match = regex.match(img_path.name)
            if match:
                # If device_keyword is None or empty, add **ALL** devices
                if device_keyword in [None, '']:
                    matching_images.append(img_path)
                # Otherwise, only add images matching 'device' keyword
                elif match.group('device') == device_keyword:
                    matching_images.append(img_path)

    return matching_images


def get_labels(file_path, label_keyword):
    # return a list of LABELS based on the image filename
    pattern = generate_regex_pattern_xx(FILENAME_FORMAT, label_keyword)
    # pattern = generate_regex_pattern(FILENAME_FORMAT, label_keyword)

    # debug
    # print('FILENAME_FORMAT:')
    # print(FILENAME_FORMAT)
    # print('pattern:')
    # print(pattern)
    # print('--------')

    regex = re.compile(pattern)
    match = regex.match(file_path.name)

    # debug
    # print('match:')
    # print(match)
    # print('--------')

    return match.group(label_keyword) if match else None



# For multi-target models, specialised metrics are needed
def lesion_loss(inp, lesion, birads): return F.cross_entropy(inp[:, :2], lesion)
def birads_loss(inp, lesion, birads): return F.cross_entropy(inp[:, 2:], birads)

def lesion_err(inp, lesion, birads): return error_rate(inp[:, :2], lesion)
def birads_err(inp, lesion, birads): return error_rate(inp[:, 2:], birads)

get_lesion_type = partial(get_labels, label_keyword='type')



def get_birads_category(path):
    # For use with Ania's export data
    birads = get_labels(file_path=path, label_keyword='birads')
    if birads in ['0', '1', '2', '3', '4']:
        return 'LOW'
    elif birads in ['4a', '4b']:
        return 'MODERATE'
    elif birads in ['4c', '5']:
        return 'HIGH'
    else:
        return 'Error, unexpected birads value: ' + birads  

def get_birads_category_xx(path):
    # For use with Beata's data
    birads = get_labels(file_path=path, label_keyword='birads')
    if birads in ['0', '1', '2', '3']:
        return 'LOW'
    elif birads in ['4']:
        return 'MODERATE'
    elif birads in ['5']:
        return 'HIGH'
    else:
        return 'Error, unexpected birads value: ' + birads   



def jupyter_running():
    try:
        from IPython import get_ipython
        # If 'get_ipython' does not return None, we are in a Jupyter environment
        if 'ipykernel' in str(get_ipython()):
            return True
    except ImportError:
        # 'IPython' not available, not running in Jupyter
        return False

    return False



# display file names and labels for a given DataLoader (multi-target model)
def show_vocab_labels(dl):
    print("\nSample Validation Labels:")
    one_batch = next(iter(dl))
    # for i, batch in enumerate(dl):

    # Extract images and labels; labels are a tuple of 2 elements
    images, prognosis_labels, birads_labels = one_batch

    for j in range(len(images)):
        # Get index of item in entire dataset (train or valid)
        # Since it's the first batch, index is just j
        index = j  

        # # Get index of item in the entire dataset (train or valid)
        # index = i * dl.bs + j

        # Ensure index is within the length of the items list
        if index < len(dl.items):
            file_name = dl.items[index].name
            prognosis = dl.vocab[0][prognosis_labels[j]]
            birads    = dl.vocab[1][birads_labels[j]]
            print(f"{prognosis:<10}, {birads:<10}     {file_name}")







def display_tensor(img, ax=None, figsize=(3,3), title=None, ctx=None, **kwargs):
    if ax is None: _, ax = plt.subplots(figsize=figsize)
    # Denormalize
    img = dls.train.decode((img,))[0][0]
    # Change channel order from CHW to HWC for visualization
    img = img.permute(1,2,0)
    ax.imshow(img)
    if title is not None: ax.set_title(title)
    ax.axis('off')
    return ax



def make_prediction(learner, test_image, show_image=False, do_cams=False):
    """Make a class prediction for a test image using a Fastai learner."""

    # Get prediction.
    # 'learner.predict' returns 3 things:
    #       'pred_class': Predicted class label
    #       'pred_idx':   Index of predicted class
    #       'outputs':  Raw output activations from model for each class
    pred_class, pred_idx, outputs = learner.predict(test_image)

    # show class probabilities
    # 'outputs' is 1D tensor with length equal to number of classes in dataset.
    # Each tensor value corresponds to activation value for a particular class.
    # i.e. 'outputs' is a set of numerical values.
    # For 4 classes (BENIGN, MALIGNANT, NORMAL, NULL), tensor contains
    # 4 values. To interpret outputs, convert raw activations to probabilities
    # using 'softmax':
    probabilities = F.softmax(outputs, dim=0)

    # parent directory of image file is true label
    true_label = os.path.basename(os.path.dirname(test_image))

    if show_image:
        # Display image
        img = Image.open(test_image)
        plt.imshow(img)
        # plt.axis('off')
        plt.show()

    # show details
    print('')
    print('Prediction:          ', pred_class)
    print('Actual:              ', true_label)
    print('Class probabilities: ', probabilities)
    print('File:                ', os.path.basename(test_image))


    # Show nice plot with prediction probabilities
    plot_pred_summary(test_image, learner, pred_class, true_label, probabilities, do_cams)


def replace_with_birads(s):
    parts = s.split('_', 1)  # Split on the first underscore only
    if len(parts) > 1:
        return parts[1].capitalize() + ' BIRADS'
    else:
        return s

def do_cams_old():
    # this old code was for manually calculating the CAM
    # I use pytorch-grad-cam now...
    if do_cams:
        x, = first(dls.test_dl([img]))
        # display_tensor(x)
        # print('\nx.shape =  ', x.shape)

        # Get class prediction and CAM
        class_id = learner.predict(img)[1].item()
        # print('class_id = ', class_id)

        # Create hook for final convolutional layer in model
        last_conv_layer = learner.model[0][-1]
        # last_conv_layer = learner.model.layers[-1].blocks[-1].norm2
        # last_conv_layer = learner.get_submodule("0.model.stages.3.blocks.2.conv_dw")

        with Hook(last_conv_layer, lambda m,i,o: o) as hook:
            # Run forward pass to compute outputs and capture features from hook
            # output = learner.model.eval()(x.unsqueeze(0))
            output = learner.model.eval()(x)
            feature_map = hook.stored[0]

            # print('\nFeature map:')
            # print('type(feature_map) = ', type(feature_map))
            # print('feature_map.shape = ', feature_map.shape)

        # Compute CAM for the specified class
        W = learner.model[1][-1].weight[class_id]
        # print('W.shape = ', W.shape)

        cam_map = (feature_map * W[:, None, None]).sum(0)
        cam_map -= cam_map.min()
        cam_map /= cam_map.max()

        # Convert 'cam_map' to np array
        cam_map_np = cam_map.cpu().detach().numpy()

        # cam_map_np is 'blocky'. Try to resize it
        cam_resized = resize(cam_map_np, (img.height, img.width),  order=1, anti_aliasing=True)



def predict_folder_single_targ(learn, get_files_func, input_folder):
    # Create new test 'dataloader' from 'dls'
    test_dl = learn.dls.test_dl(get_files_func(input_folder), with_labels=True)
    test_dl.show_batch(max_n=40)

    # # handy
    # test_dl.items
    # learn.dls.vocab

    preds, targs, preds_decoded = learn.get_preds(dl=test_dl, with_decoded=True)
    # preds, targs, preds_decoded = learn.tta(dl=test_dl, with_decoded=True)

    # acc = accuracy(preds, targs)
    test_loss, test_accuracy = learn.validate(dl=test_dl)

    print('\ntest_accuracy: ' + str(np.round(test_accuracy, 3)))
    print('test_loss:     ' + str(np.round(test_loss, 3)))

    # Extract file names
    file_paths       = [str(path) for path in test_dl.items]
    results          = ["Correct" if pred == targ else "Wrong" for pred, targ in zip(preds_decoded, targs)]
    predicted_labels = list(learn.dls.vocab[preds_decoded])
    actual_labels    = list(learn.dls.vocab[targs])

    # Convert preds to a list of lists
    preds_list = preds.tolist()

    # Dynamically create probability columns based on dls.vocab
    prob_columns = {}
    for i, label in enumerate(learn.dls.vocab):
        column_name = f'prob_{label}'
        prob_columns[column_name] = [np.round(p[i], 3) for p in preds_list]

    # Create DataFrame
    df_results = pd.DataFrame({
        "result"           : results,
        "actual"           : actual_labels,
        "predicted"        : predicted_labels,
        **prob_columns,  
        "files"            : file_paths
    })

    # show 'wrong' ones at the top
    df_results_sorted = df_results.sort_values(by=['result', 'actual', 'predicted'], ascending=False)
    df_results_sorted = df_results_sorted.reset_index(drop=True)

    # Convert DataFrame to a nicely formatted string
    results_string = df_results_sorted.to_string(justify='right', index=True)


    log_header = []

    # Open a single file for writing class accuracies
    # log_filename = create_log_filename(test_dl.items[0])  # Use image path to name the file
    log_filename = utils.get_next_file_number(LOG_DIR) + '-' + MODEL_EXPORT_NAME + '.txt'
    log_path = os.path.join(LOG_DIR, log_filename)
    print('log_path is:', log_path)


    now = datetime.now()
    datetime_str = now.strftime("%d-%b-%Y %H:%M")

    # Read ini
    config = configparser.ConfigParser(inline_comment_prefixes=";")
    config.read(CONFIG)

    # Settings
    EXPORT_DATA = config['MODEL']['EXPORT_DATA'] # Nov, June etc
    BASE_DIR    = config[EXPORT_DATA]['BASE_DIR']

    RUN_COMMENT           = config.get('MODEL','COMMENT')
    MODEL_EXPORT_NAME     = config.get('MODEL','MODEL_EXPORT_NAME').replace('.', '_')
    TRAIN_NEW_MODEL       = config.getboolean('MODEL', 'TRAIN_NEW_MODEL')
    BATCH_SIZE            = int(config['MODEL']['BATCH_SIZE'])
    TRAINING_EPOCHS       = int(config['MODEL']['TRAINING_EPOCHS'])
    item_tfms_IMAGE_SIZE  = config['MODEL']['item_tfms_IMAGE_SIZE']
    batch_tfms_IMAGE_SIZE = config['MODEL']['batch_tfms_IMAGE_SIZE']
    item_tfms_IMAGE_SIZE  = tuple(map(int, item_tfms_IMAGE_SIZE.strip('()').split(',')))
    batch_tfms_IMAGE_SIZE = tuple(map(int, batch_tfms_IMAGE_SIZE.strip('()').split(',')))
    TRAIN_DATA            = os.path.join(BASE_DIR, config['MODEL']['TRAIN_DATA'])
    TEST_DATA             = os.path.join(BASE_DIR, config['MODEL']['TEST_DATA'])

    PLOT_DIR = config['MODEL']['PLOT_DIR']
    # LOG_DIR  = config['MODEL']['LOG_DIR']



    log_header.append('DATE                   ' + str (datetime_str))
    log_header.append('RUN_COMMENT            ' + str (RUN_COMMENT))
    log_header.append('MODEL_EXPORT_NAME      ' + str (MODEL_EXPORT_NAME))
    log_header.append('TRAIN_NEW_MODEL        ' + str (TRAIN_NEW_MODEL))
    log_header.append('TRAIN_DATA             ' + str (TRAIN_DATA))
    log_header.append('TEST_DATA              ' + str (TEST_DATA))
    log_header.append('BATCH_SIZE             ' + str (BATCH_SIZE))
    log_header.append('TRAINING_EPOCHS        ' + str (TRAINING_EPOCHS))
    log_header.append('item_tfms_IMAGE_SIZE   ' + str (item_tfms_IMAGE_SIZE))
    log_header.append('batch_tfms_IMAGE_SIZE  ' + str (batch_tfms_IMAGE_SIZE))
    log_header.append('ROC PLOT               ' + str (os.path.join(PLOT_DIR, 'ROC-' +MODEL_EXPORT_NAME+ '.png' )))
    log_header.append('' )
    log_header.append('Test Accuracy          ' + str(np.round(test_accuracy, 3)))
    log_header.append('Test Loss              ' + str(np.round(test_loss, 3)))
    log_header.append('' )


    # Write to log
    with open(log_path, 'w') as file:
        # for line in write_lines:
        for line in log_header:
            file.write(line + "\n")

        file.write(results_string + "\n")


    # Plot ROC AUC
    # ------------

    # Dynamically determine the number of classes
    unique_classes = np.unique(targs)
    n_classes      = len(unique_classes)
    print('n_classes: ', n_classes)

    # Dynamically binarize the output labels for multi-class
    true_labels_bin = label_binarize(targs, classes=unique_classes)
    # print('true_labels_bin: ', true_labels_bin)

    # Check if it's a binary classification
    is_binary_classification = n_classes == 2

    # Compute ROC curve and ROC area for each class
    fpr     = dict()
    tpr     = dict()
    roc_auc = dict()

    if is_binary_classification:
        print('binary classification')
        # For binary classification, use label 1 (positive class) predictions
        fpr[0], tpr[0], _ = roc_curve(true_labels_bin, preds[:, 1])
        roc_auc[0] = auc(fpr[0], tpr[0])
    else:
        # Multi-class classification as before
        for i in range(n_classes):
            fpr[i], tpr[i], _ = roc_curve(true_labels_bin[:, i], preds[:, i])
            roc_auc[i] = auc(fpr[i], tpr[i])

    # title
    str_train_dir = '/'.join(Path(TRAIN_DATA).parts[-3:])
    str_test_dir  = '/'.join(Path(TEST_DATA).parts[-3:])

    plot_title  = 'Model:    ' + MODEL_EXPORT_NAME + '\n'
    plot_title += 'Comment:  ' + RUN_COMMENT       + '\n'
    plot_title += 'Train:    ' + str_train_dir     + '\n'
    plot_title += 'Test:     ' + str_test_dir

    # Generate a list of colors based on the number of classes
    # colors = plt.cm.OrRd(np.linspace(0.5, 1, n_classes))
    colors = ['Green', 'Orange', 'Red']

    font_prop = FontProperties(family='DejaVu Sans Mono')

    plt.figure(figsize=(12, 8))

    # Adjust the label in the plot for binary classification
    if is_binary_classification:
        plt.plot(fpr[0], tpr[0], color='blue', lw=3, label=f'ROC: {dls.vocab[1]} (area: {roc_auc[0]:.2f})')
    else:
        for i, color in zip(range(n_classes), colors):
            plt.plot(fpr[i], tpr[i], color=color, lw=3, 
                    label=f'ROC: {learn.dls.vocab[i]:<15} (area: {roc_auc[i]:.3f})')

    plt.plot([0, 1], [0, 1], 'k--', lw=2)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title(plot_title, loc='left', fontname="DejaVu Sans Mono")
    plt.legend(loc="lower right", prop=font_prop)


    # Save plot to disk
    utils.assert_dir_exists(PLOT_DIR)
    plot_path = os.path.join(PLOT_DIR, 'ROC-'+MODEL_EXPORT_NAME)
    print('Saving ROC plot: ', plot_path)
    plt.savefig(plot_path)
    plt.close()  # free memory

    # Optional:
    # plt.show()

    return df_results_sorted
