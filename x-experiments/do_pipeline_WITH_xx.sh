#!/bin/bash

# Pipelines created
# -----------------
# pipline-007      1_ok_csv_withxx      WITH x marks,     Beata Annotations, cropped
# pipline-006      1_ok_csv_withoutxx   without x marks,  Beata Annotations, cropped


# More info, See: ./README.md

ACTIVE_CONFIG='config-PIPELINE-WITH-xx.ini'


# # INITIAL PROCESSING
# --------------------

# # with 'x' marks
# python 10-image-processing-xx.py --config=$ACTIVE_CONFIG  --in_dir='1_ok_csv_withxx'       --out_dir='10-Processed-Images'



# # RENAME FILES
# --------------
# python 20-rename-organise-xx.py  --config=$ACTIVE_CONFIG  --in_dir='10-Processed-Images' --out_dir='20-Renamed-Images'



# # CREATE TRAINING POOL
# ----------------------
python 30-test-train-split.py  --config=$ACTIVE_CONFIG  --filter=all     --in_dir="20-Renamed-Images/VALID-FOR-TRAINING" --out_dir="Training-Pool-"

# python 30-test-train-split.py  --config=$ACTIVE_CONFIG  --filter=Logiq    --in_dir="20-Renamed-Images/VALID-FOR-TRAINING" --out_dir="Training-Pool-"
# python 30-test-train-split.py  --config=$ACTIVE_CONFIG  --filter=Toshiba  --in_dir="20-Renamed-Images/VALID-FOR-TRAINING" --out_dir="Training-Pool-"
# python 30-test-train-split.py  --config=$ACTIVE_CONFIG  --filter=Usystems --in_dir="20-Renamed-Images/VALID-FOR-TRAINING" --out_dir="Training-Pool-"
