import os
import sys
import shutil
import subprocess
import re

def count_lines(filename):
    # Using 'wc -l' to count lines
    result = subprocess.run(['wc', '-l', filename], stdout=subprocess.PIPE, text=True)
    # The output is in the format 'number filename', so split and get the first part
    line_count = int(result.stdout.split()[0])
    return line_count 


def print_banner(msg, script):
    print('')
    print('■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■')
    print(' '*15, script)
    print(' '*15, msg)
    print('■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■')


# Does folder exist?
def assert_dir_exists(folder_path):
    if os.path.isdir(folder_path):
        print(f"\nOK: Folder exists: '{folder_path}'")
    else:
        print(f"\nERROR: Path Not found: '{folder_path}'")
        sys.exit()

def make_dir_if_none(folder_path):
    if not os.path.isdir(folder_path):
        os.makedirs(folder_path)
        print(f"\nCreated new folder: '{folder_path}'")

# ensure non-empty folder
def assert_dir_not_empty(folder_path):
    if len(os.listdir(folder_path)) == 0:
        print(f"\nPROBLEM: Folder empty: '{folder_path}'")
        sys.exit()


# REcreate folder (remove existing)
def recreate_dir(folder_path):
    if os.path.exists(folder_path):
        shutil.rmtree(folder_path)
        print(f"\nOK: Deleted existing Folder: '{folder_path}'")
    os.makedirs(folder_path)
    print(f"\nOK: (re)Created Folder: '{folder_path}'")


def count_files_in_folder(folder_path):
    """Recursively count the files in the given folder."""
    file_count = 0
    # List all entries in the folder
    entries = os.listdir(folder_path)
    for entry in entries:
        entry_path = os.path.join(folder_path, entry)
        if os.path.isfile(entry_path):
            file_count += 1
        elif os.path.isdir(entry_path):
            file_count += count_files_in_folder(entry_path)
    return file_count


# create a file name based on a path
def create_log_filename(name):
    # Format date and time for file name
    now = datetime.now()
    datetime_str = now.strftime("%Y%m%d__%H.%M.%S")
    return f"{datetime_str}__{name}.txt"



def get_next_file_number(folder):
    # Regex to extract numbers from filenames
    regex = re.compile(r"(\d+)")
    
    # List all files and extract numbers
    max_number = 0
    for file in os.listdir(folder):
        match = regex.search(file)
        if match:
            number = int(match.group(1))
            max_number = max(max_number, number)

    # Increment the highest number and format new filename
    next_number = str(max_number + 1).zfill(3)  # Zero-padding to 4 digits
    
    return next_number



def folder_summary(folder_path, level=0):
    """Recursively print a summary of subfolders and their file counts."""
    try:
        # Get list of all entries in the folder
        entries = sorted(os.listdir(folder_path))

        if level==0:
            num_files = len([f for f in entries if os.path.isfile(os.path.join(folder_path, f))])
            print("")
            print("FOLDER SUMMARY")
            print(f"{num_files} {os.path.basename(folder_path)}")

        # Filter list for directories only
        subfolders = [d for d in entries if os.path.isdir(os.path.join(folder_path, d))]

        # For each subfolder, get the number of files in it
        for subfolder in subfolders:
            subfolder_path = os.path.join(folder_path, subfolder)
            num_files = count_files_in_folder(subfolder_path)
            # add 'nesting' to printout
            indent = '    ' * (level+1)
            # Show summary
            print(f"{indent}{indent}{num_files:6} {subfolder}")
            folder_summary(subfolder_path, level+1)

    except Exception as e:
        print(f"An error occurred: {e}")


