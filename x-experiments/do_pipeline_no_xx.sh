#!/bin/bash


# More info, See: ./README.md

ACTIVE_CONFIG='config-PIPELINE-no-xx.ini'


# # INITIAL PROCESSING
# --------------------


# # without 'x' marks
# python 10-image-processing-xx.py --config=$ACTIVE_CONFIG  --in_dir='1_ok_csv_withoutxx'    --out_dir='10-Processed-Images'


# # RENAME FILES
# --------------
# python 20-rename-organise-xx.py  --config=$ACTIVE_CONFIG  --in_dir='10-Processed-Images' --out_dir='20-Renamed-Images'



# # CREATE TRAINING POOL
# ----------------------
MODEL_EXISTING_TRAINING_POOL=/mnt/dysk_roboczy/efarell/repos/US-Data/x/pipeline-006-WITH-xx/Training-Pool-all/

python 30-test-train-split.py  --config=$ACTIVE_CONFIG    --filter=all     --in_dir="20-Renamed-Images/VALID-FOR-TRAINING" --out_dir="Training-Pool-"  --model_dir=$MODEL_EXISTING_TRAINING_POOL

# python 30-test-train-split.py  --config=./config-PIPELINE-xx.ini  --filter=Logiq    --in_dir="20-Renamed-Images/VALID-FOR-TRAINING" --out_dir="Training-Pool-"
# python 30-test-train-split.py  --config=./config-PIPELINE-xx.ini  --filter=Toshiba  --in_dir="20-Renamed-Images/VALID-FOR-TRAINING" --out_dir="Training-Pool-"
# python 30-test-train-split.py  --config=./config-PIPELINE-xx.ini  --filter=Usystems --in_dir="20-Renamed-Images/VALID-FOR-TRAINING" --out_dir="Training-Pool-"
