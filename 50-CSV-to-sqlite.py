# In[]

import os
import pandas as pd
import sqlite3
import configparser
import utils

# In[]

AIM = 'Create sqlite database from csv file with image info'


# get paths
INPUT_CSV   = './annotations/save_lv8.csv'
OUTPUT_DB   = './annotations/save_lv8.sqlite'

utils.print_banner(AIM, os.path.basename(__file__))  
print('INPUT_CSV    ',  INPUT_CSV)
print('OUTPUT_DB    ',  OUTPUT_DB)
print('')


# In[]

# Read csv
# df = pd.read_csv(INPUT_CSV, sep=',')
df = pd.read_csv(INPUT_CSV, sep=';')

# In[]

# Make some field names better
df.rename(columns={
    'PixelX': 'Pixel_width_X',
    'PixelY': 'Pixel_width_Y'
}, inplace=True)

# In[]

print ("Rows: ", len(df))
print (df.dtypes)

# In[]

# Create SQLite database or connect to an existing one
conn = sqlite3.connect(OUTPUT_DB)

# Convert DataFrame to SQLite table
df.to_sql('images', conn, if_exists='replace', index=False)

# Commit the changes and close the connection
conn.commit()
conn.close()

print(f'Created database: {OUTPUT_DB}')

# In[]
