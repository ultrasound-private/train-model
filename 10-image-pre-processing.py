from PIL import Image
from tqdm import tqdm
import argparse
import configparser
import csv
import cv2
import numpy as np
import os
import os, sys
import shutil
import utils


AIM = 'Draw contour around legions, and crop images'

# Set up argument parser
parser = argparse.ArgumentParser(description=AIM)
parser.add_argument('--config',  required=True,  help='Which config to use')
parser.add_argument('--in_dir',  required=True, help='Name of input folder')
parser.add_argument('--out_dir', required=True, help='Name of output folder')
args = parser.parse_args()

# read ini
config = configparser.ConfigParser(inline_comment_prefixes=";")
config.read(args.config)


# Should we execute this script??
DO_IMAGE_PROCESSING = config.getboolean('PIPELINE', 'DO_IMAGE_PROCESSING')

if not DO_IMAGE_PROCESSING:
    print('\nSkipping Image processing!')
    exit()


# image transformations
DRAW_COUNTOURS       = config.getboolean('PIPELINE', 'DRAW_COUNTOURS')
ADJUST_PIXEL_WIDTH   = config.getboolean('PIPELINE', 'ADJUST_PIXEL_WIDTH')
DRAW_CROSS           = config.getboolean('PIPELINE', 'DRAW_CROSS')
CROP_IMAGES          = config.getboolean('PIPELINE', 'CROP_IMAGES')
CONVERT_TO_GREYSCALE = config.getboolean('PIPELINE', 'CONVERT_TO_GREYSCALE')
CONTOUR_WIDTH        = config.getint('PIPELINE', 'CONTOUR_WIDTH')
CONTOUR_COLOR        = config.get('PIPELINE', 'CONTOUR_COLOR')
CONTOUR_COLOR        = tuple(map(int, CONTOUR_COLOR.strip('()').split(',')))
NEW_PIXEL_WIDTH_X    = config.getfloat('PIPELINE', 'NEW_PIXEL_WIDTH_X')
NEW_PIXEL_WIDTH_Y    = config.getfloat('PIPELINE', 'NEW_PIXEL_WIDTH_Y')

# Paths
EXPORT_DATA    = config['PIPELINE']['EXPORT_DATA']
BASE_DIR       = config[EXPORT_DATA]['BASE_DIR']
PIPELINE_DIR   = os.path.join(BASE_DIR, config['PIPELINE']['NAME'])
META_DIR       = config[EXPORT_DATA]['META_DIR']
CSV_METADATA   = os.path.join(META_DIR, config[EXPORT_DATA]['CSV'])
CSV_PREPROCESS = os.path.join(META_DIR, config[EXPORT_DATA]['CSV_PREPROCESS'])

INPUT_DIR    = os.path.join(BASE_DIR,     args.in_dir)
OUTPUT_DIR   = os.path.join(PIPELINE_DIR, args.out_dir)


# show info
utils.print_banner(AIM, os.path.basename(__file__))
print('EXPORT_DATA          ', EXPORT_DATA)
print('INPUT_DIR            ', INPUT_DIR)
print('OUTPUT_DIR           ', OUTPUT_DIR)
print('CSV_METADATA         ', CSV_METADATA)
print('CSV_PREPROCESS       ', CSV_PREPROCESS)
print('')
print('Transformations:')
print('DRAW_CROSS           ', DRAW_CROSS)
print('CROP_IMAGES          ', CROP_IMAGES)
print('CONVERT_TO_GREYSCALE ', CONVERT_TO_GREYSCALE)
print('')
print('DRAW_COUNTOURS       ', DRAW_COUNTOURS)
print('CONTOUR_COLOR        ', CONTOUR_COLOR)
print('CONTOUR_WIDTH        ', CONTOUR_WIDTH)
print('')
print('ADJUST_PIXEL_WIDTH   ', ADJUST_PIXEL_WIDTH)
print('NEW_PIXEL_WIDTH_X    ', NEW_PIXEL_WIDTH_X)
print('NEW_PIXEL_WIDTH_Y    ', NEW_PIXEL_WIDTH_Y)
print('')
print('PIPELINE_DIR        ', PIPELINE_DIR)
print('')


# Will we go ahead?
response = input("Continue? (y/n): ").lower()
if response == 'n':
    exit()

# Verify input/output folders
utils.assert_dir_exists(INPUT_DIR)
utils.assert_dir_not_empty(INPUT_DIR)
utils.folder_summary(INPUT_DIR)
utils.recreate_dir(OUTPUT_DIR)



def insert_cross(image_arr, center, size=10, color=(255, 0, 0), thickness=2):
    # Draw vertical line of the cross
    cv2.line(image_arr, (center[0], center[1] - size), (center[0], center[1] + size), color, thickness)
    # Draw horizontal line of the cross
    cv2.line(image_arr, (center[0] - size, center[1]), (center[0] + size, center[1]), color, thickness)


def draw_crosses(orig, Tumor_mask_field):
    orig_arr = np.array(orig)
    mask_filenames = Tumor_mask_field.split(';')

    for mask_filename in mask_filenames:
        if not mask_filename.strip():
            continue

        mask_path = os.path.join(INPUT_DIR, mask_filename)
        try:
            mask = Image.open(mask_path)
        except IOError:
            print(f"Warning: Could not open mask {mask_path}. Skipping.")
            continue

        mask_arr = np.array(mask)
        mask_gray = cv2.cvtColor(mask_arr, cv2.COLOR_BGR2GRAY)
        _, mask_binary = cv2.threshold(mask_gray, 1, 255, cv2.THRESH_BINARY)
        contours, _ = cv2.findContours(mask_binary, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        for contour in contours:
            M = cv2.moments(contour)
            if M["m00"] != 0:
                cX = int(M["m10"] / M["m00"])
                cY = int(M["m01"] / M["m00"])
                insert_cross(orig_arr, (cX, cY))

    overlay_img = Image.fromarray(orig_arr)

    final_img = overlay_img.convert('RGB')
    final_arr = np.array(final_img)

    return final_arr


# For an ultrasound image, add green 'border' around any tumours in image
# (use the mask 'contours' to draw 'border')
def draw_boundary(img, mask):
    img_arr  = np.array(img)
    mask_arr = np.array(mask)

    if mask_arr.size == 0:
        # no mask
        # return original array
        # print('No mask')
        return img_arr


    # Convert mask into binary
    # (or else 'cv2.findContours' will fail)
    mask_gray      = cv2.cvtColor(mask_arr, cv2.COLOR_BGR2GRAY)
    _, mask_binary = cv2.threshold(mask_gray, 1, 255, cv2.THRESH_BINARY)

    # Find contours
    contours, _ = cv2.findContours(mask_binary,
                                    cv2.RETR_EXTERNAL,
                                    cv2.CHAIN_APPROX_SIMPLE)

    # Draw contours
    cv2.drawContours(image=img_arr,
                        contours=contours,
                        contourIdx=-1,
                        color=CONTOUR_COLOR,
                        thickness=CONTOUR_WIDTH)

    # Convert to Image to adjust alpha channel
    overlay_img = Image.fromarray(img_arr)

    # Remove alpha channel...
    # Convert RGBA to RGB
    final_img = overlay_img.convert('RGB')

    # Convert PIL Image back to NumPy
    final_arr = np.array(final_img)

    return final_arr


def standardise_pixel_width(img, target_px_x, target_px_y, cur_px_x, cur_px_y):
    #
    # Resize image to desired new pixel size
    #
    #   current pixel size x:   cur_px_x
    #   current pixel size y:   cur_px_y
    #
    #   target pixel size x:  target_px_x
    #   target pixel size y:  target_px_y
    #
    if isinstance(img, np.ndarray):
        # convert to PIL image
        img = Image.fromarray(img)

    factor_x = float(cur_px_x) / target_px_x
    factor_y = float(cur_px_y) / target_px_y

    new_w    = int(img.width  * factor_x)
    new_h    = int(img.height * factor_y)

    # note: 'Lanczos' is an anti-alias algorithm
    resized_img = img.resize((new_w, new_h), Image.Resampling.LANCZOS)
    return np.array(resized_img)



# create handy variables
updated_rows = []
total_rows = utils.count_lines(CSV_METADATA) - 1 # subtract 1 for header


# Process CSV
with open(CSV_METADATA, 'r', newline='') as csv_file:
    print("\nProcessing...")
    csv_reader = csv.DictReader(csv_file)

    for row in tqdm(csv_reader, total=total_rows):
        # Could be multiple mask files, separated by ";". Take first one.
        mask_filename = row['Tumor_mask_filename'].split(';')[0]
        # print(mask_filename)

        # firstly, load orig image & mask
        # -------------------------------
        image_path = os.path.join(INPUT_DIR, row['Image_filename'])
        mask_path  = os.path.join(INPUT_DIR, mask_filename)

        try:
            img  = np.array(Image.open(image_path))

            if os.path.isfile(mask_path):
                mask = np.array(Image.open(mask_path))
            else:
                # no mask
                mask = np.array([])

        except IOError:
            print(f"Error: Cannot open: {image_path}.")
            print(f"Error: Or Cannot open: {mask_path}.")
            continue

        # Store image size
        row['orig_height']      = img.shape[0]
        row['orig_width']       = img.shape[1]

        # by DEFAULT, assume we won't cut/crop/resize the image
        row['processed_height'] = img.shape[0]
        row['processed_width']  = img.shape[1]


        if DRAW_COUNTOURS:
            # Load image and add boundary countours around ROIs (using masks)
            img = draw_boundary(img, mask)


        if DRAW_CROSS:
            # insert a cross in the centre of each legion
            img = draw_crosses(img, mask)


        if CROP_IMAGES:
            # store Bounding box
            row_start = int(row['LocY0'])
            row_end   = int(row['LocY1'])
            col_start = int(row['LocX0'])
            col_end   = int(row['LocX1'])

            # crop image and mask
            img  = img[row_start:row_end, col_start:col_end]
            if mask.size != 0:
                mask = mask[row_start:row_end, col_start:col_end]

            # save new coords
            row['processed_height'] = img.shape[0]
            row['processed_width']  = img.shape[1]




        if ADJUST_PIXEL_WIDTH:
            # add code here
            img = standardise_pixel_width(img,
                                          NEW_PIXEL_WIDTH_X,
                                          NEW_PIXEL_WIDTH_Y,
                                          row['PixelX'], row['PixelY'])

            # save new size
            row['processed_height'] = img.shape[0]
            row['processed_width']  = img.shape[1]


        crop_path      = os.path.join(OUTPUT_DIR, row['Image_filename'])
        crop_path_mask = os.path.join(OUTPUT_DIR, mask_filename)


        # Save using cv2
        # --------------

        if CONVERT_TO_GREYSCALE:
            # Save image as greyscale
            img_gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
            # Write grayscale image to disk
            cv2.imwrite(crop_path, img_gray)
        else:
            # Save image in color (BGR)
            # Convert from RGB (used by PIL) to BGR (used by OpenCV)
            img_bgr = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
            cv2.imwrite(crop_path, img_bgr)

        # Save mask too...
        # img_gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        if mask.size != 0:
            cv2.imwrite(crop_path_mask, mask)

        # or
        # cv2.imwrite(crop_path, img_bgr, [int(cv2.IMWRITE_PNG_COMPRESSION), 9])

        # update METADATA csv
        updated_rows.append(row)



print("Done.")
utils.folder_summary(OUTPUT_DIR)


# Write updated rows to a new CSV file
new_fieldnames = csv_reader.fieldnames + ['orig_width', 'orig_height', 'processed_width', 'processed_height']

with open(CSV_PREPROCESS, 'w', newline='') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames=new_fieldnames)
    writer.writeheader()
    for row in updated_rows:
        writer.writerow(row)

print(f"New CSV with preprocessing info: {CSV_PREPROCESS}")
