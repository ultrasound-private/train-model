https://forums.fast.ai/t/training-time-of-pre-sized-images-vs-resizing-on-the-fly-with-transformations/96919
img_size= 500
bs=40
splitter = RandomSplitter(0.2)

item_tfms=[Resize(size=img_size, resamples=(Image.Resampling.LANCZOS, 0))]

filters=[
    ImageFilter.EDGE_ENHANCE_MORE, 
    ImageFilter.EMBOSS,
    ImageFilter.CONTOUR,
    ImageFilter.FIND_EDGES,
]
xtra_tfms = [Dihedral()]

batch_tfms=aug_transforms(do_flip=False, max_rotate=0.0, max_zoom=1.0,
                        p_affine=0.0, p_lighting=0.0, xtra_tfms=xtra_tfms, 
                        mode='bilinear', pad_mode='reflection', align_corners=True,
                        mult=1.0, flip_vert=False, min_zoom=1.,max_lighting=0.0, 
                        max_warp=0.0, size=img_size,
                        batch=False, min_scale=1.)

db = DataBlock(blocks=(ImageBlock, CategoryBlock),
                       get_x=Pipeline([ColReader(0, pref=pref, suff=suff),
                                                 ApplyPILFilter(filters, p=0.5)]),
                       get_y=ColReader(1),
                       splitter=splitter,
                       item_tfms=item_tfms,
                       batch_tfms=batch_tfms)
dls = db.dataloaders(df, bs=bs)

learn = vision_learner(dls, densenet121, metrics=accuracy, model_dir="/kaggle/working").to_fp16()

learn.fine_tune(100, 0.002, cbs = [cb1, cb2])

