# Import necessary libraries
from fastai.vision.all import *
import numpy as np

# Dummy data creation
np.random.seed(42)  # For reproducibility
n = 100  # Number of data points
classes = ['cat', 'dog', 'bird']  # Three classes
# Create random data and labels, with 'bird' under-represented
data = np.random.rand(n, 3, 28, 28)  # Random images (28x28, 3 channels)
labels = np.random.choice(classes, n, p=[0.4, 0.4, 0.2])  # Random labels with 'bird' less frequent

# Convert data and labels into a FastAI Datasets object
dset = Datasets(list(zip(data, labels)), tfms=[ToTensor(), Categorize()])

# Create a DataBlock
dblock = DataBlock(blocks=(ImageBlock, CategoryBlock),
                   splitter=RandomSplitter(),  # Random split for train/validation
                   get_x=lambda x: x[0],  # Extract image
                   get_y=lambda x: x[1])  # Extract label

# Weighting - more weight to under-represented classes
weights = torch.tensor([1 if label == 'bird' else 0.5 for _, label in dset])

# Create weighted DataLoaders
dls = dblock.weighted_dataloaders(dset, wgts=weights, bs=16)

# Define a simple CNN model
model = cnn_learner(dls, resnet18, metrics=accuracy)

# Train the model for a few epochs
model.fit(1)

# Minimal example to show weighted training with FastAI
# Here, 'bird' class is under-represented and thus given more weight
model.show_results()

