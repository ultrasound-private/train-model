#!/bin/bash

docker run -d \
  --hostname 148.81.54.213 \
  --publish 4000:443 --publish 4500:80 --publish 2222:22 \
  --name liczyk2Z-gitlab \
  --restart always \
  --volume /mnt/dysk_roboczy/gitlab/config:/etc/gitlab \
  --volume /mnt/dysk_roboczy/gitlab/logs:/var/log/gitlab \
  --volume /mnt/dysk_roboczy/gitlab/data:/var/opt/gitlab \
  gitlab/gitlab-ce:latest



