SELECT

Examinations.Additional_information,
Examinations.Cortex_thickness,
Examinations.Depth,
Examinations.ExaminationID,
Examinations.Length,
Examinations.Node_length_long_axis,
Examinations.Node_length_short_axis,
Examinations.Notes as Examinations_Notes,
Examinations.Signs,
Examinations.Symptoms,
Examinations.Tissue_composition,
Examinations.Verification,
Examinations.Width,

HistopathologyResults.B,
HistopathologyResults.ER,
HistopathologyResults.Grade,
HistopathologyResults.HER2,
HistopathologyResults.Ki67,
HistopathologyResults.Nodes_examined,
HistopathologyResults.Nodes_metastasis,
HistopathologyResults.Notes as HistopathologyResults_Notes,
HistopathologyResults.Other,
HistopathologyResults.PgR,
HistopathologyResults.Subtype,
HistopathologyResults.Type,

Patients.Age,

Reports.Artifacts,
Reports.BIRADS,
Reports.BIRADS_comment,
Reports.BIRADS_image,
Reports.Calcifications,
Reports.Diagnosis,
Reports.Echogenicity,
Reports.Echogenicity_node,
Reports.Elasticity,
Reports.Halo,
Reports.Margin,
Reports.Microflow,
Reports.Notes as Reports_Notes,
Reports.Orientation,
Reports.Other,
Reports.Posterior_features,
Reports.Shape,
Reports.Skin_thickening,
Reports.Surroundings,
Reports.Vascular_pattern,
Reports.Vascularity_in_mass,
Reports.Vascularity_in_rim,
Reports.Vascularity_node,

Tumors.Clock,
Tumors.Cmfn,
Tumors.Side,
Tumors.Tumor_type,

TumorsReports.RadiologistID

FROM Examinations
LEFT JOIN Tumors                ON Examinations.TumorID       = Tumors.TumorID
LEFT JOIN Patients              ON Examinations.PatientID     = Patients.PatientID
LEFT JOIN HistopathologyResults ON Examinations.TumorID       = HistopathologyResults.TumorID
LEFT JOIN TumorsReports         ON Examinations.ExaminationID = TumorsReports.ExaminationID
LEFT JOIN Reports               ON TumorsReports.ReportID     = Reports.ReportID

WHERE Examinations.RadiologistID = TumorsReports.RadiologistID
AND   Reports.BIRADS_image = '%s'
AND   Examinations.ExaminationID IN
                                (SELECT ExaminationID
                                 FROM Images
                                 WHERE (Original_extension='avi' OR Original_extension='mp4' OR Original_extension='mov' OR Frames_number > 1) 
                                 AND   (Datatype='Color Doppler' OR Datatype='Power Doppler' OR Datatype='microflow'))

ORDER BY Examinations.Created", $birads)

