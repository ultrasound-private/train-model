
-- Get Image Data

SELECT 
Images.ImageID, 
Images.Path as Image_path, 
Images.Body_part, 
Images.Datatype,
GROUP_CONCAT(Masks.Path     SEPARATOR ';') as Masks_paths,
GROUP_CONCAT(Masks.ROI_type SEPARATOR ';') as ROI_types

FROM Images
LEFT JOIN Masks ON Images.ImageID = Masks.ImageID

WHERE Images.ExaminationID = %d

GROUP BY Images.ImageID


-- Get Binary data


-- Get Random Data ??

SELECT RPAD(REVERSE(ExaminationID),4,'0') as RandomID,
ROW_NUMBER() OVER(ORDER BY RandomID) as RowID,
NOW() as now_date 
FROM TumorsReports
GROUP BY ExaminationID
ORDER BY RAND()
LIMIT 1
