

-- In[]

-- Test
mysql -h localhost     -u ja -p pierwsi

-- Prod
mysql -h 148.81.54.191 -u ja -p pierwsi


-- Setting: show results in a 'pager'
pager less -n -i -S

-- Show results vertically
end line in `\G` instead of `;`

-- In[]

show tables;

-- Examinations            
-- HistopathologyResults   
-- Images                  
-- Masks                   
-- Patients                
-- Radiologists            
-- RadiologistsPrivileges  
-- Reports                 
-- Sources                 
-- Tumors                  
-- TumorsImages            
-- TumorsReports           
-- pma__bookmark           
-- pma__central_columns    
-- pma__column_info        
-- pma__designer_settings  
-- pma__export_templates   
-- pma__favorite           
-- pma__history            
-- pma__navigationhiding   
-- pma__pdf_pages          
-- pma__recent             
-- pma__relation           
-- pma__savedsearches      
-- pma__table_coords       
-- pma__table_info         
-- pma__table_uiprefs      
-- pma__tracking           
-- pma__userconfig         
-- pma__usergroups         
-- pma__users              
-- users                   


-- In[]

desc Examinations;
select count(*) from Examinations \G
select * from Examinations where PatientID = 425 \G

select * from users;

-- In[]

desc Images;

-- ImageID               | int(11)      | NO   | PRI | NULL    |
-- TumorID               | int(11)      | NO   | MUL | NULL    |
-- ExaminationID         | int(11)      | NO   | MUL | NULL    |
-- Path                  | varchar(512) | NO   |     | NULL    |
-- Export_path           | varchar(512) | YES  |     | NULL    |
-- Original_extension    | varchar(8)   | YES  |     | NULL    |
-- Body_part             | varchar(16)  | NO   |     | NULL    |
-- Datatype              | varchar(16)  | NO   |     | NULL    |
-- Dual_mode             | varchar(4)   | NO   |     | NULL    |
-- Silicone_implant      | varchar(4)   | NO   |     | NULL    |
-- Device_name           | varchar(16)  | YES  |     | NULL    |
-- Device_uname          | varchar(32)  | YES  |     | NULL    |
-- Frames_number         | int(11)      | YES  |     | NULL    |
-- Pixel_sizeX           | varchar(32)  | YES  |     | NULL    |
-- Pixel_sizeY           | varchar(32)  | YES  |     | NULL    |
-- Region_locationX0     | varchar(32)  | YES  |     | NULL    |
-- Region_locationX1     | varchar(32)  | YES  |     | NULL    |
-- Region_locationY0     | varchar(32)  | YES  |     | NULL    |
-- Region_locationY1     | varchar(32)  | YES  |     | NULL    |
-- Region_locationX0_cut | varchar(32)  | YES  |     | NULL    |
-- Region_locationX1_cut | varchar(32)  | YES  |     | NULL    |
-- Region_locationY0_cut | varchar(32)  | YES  |     | NULL    |
-- Region_locationY1_cut | varchar(32)  | YES  |     | NULL    |
-- Date                  | datetime     | YES  |     | NULL    |


select count(*) from Images;
select * from Images where ExaminationID = 438 \G

-- In[]



