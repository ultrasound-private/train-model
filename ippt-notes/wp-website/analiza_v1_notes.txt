# prime.si-analysis
    - template for unified image analysis.

    - This README taken from:
        - `/mnt/dysk_roboczy/efarell/repos/analiza/analiza_v1/README.md`

    - README also available via:
      http://best.ippt.pan.pl/benchmark/benchmark.php
        - Instructions for using the benchmark
        - Requirements for the uploaded model:
        - the model must be zipped and uploaded in 'ZIP' format
        - the model must not contain files named "main.py" and
          "analiza.py" contained in the main directory
        - the model must contain a requirements.txt file (in the
          main directory) containing the libraries necessary for
          the script to run
        - the model must be run with the function "test(image)"
        - the file containing the "test(image)" function must be
          in a script named "test.py"
        - the model must return variables in the form "return
          (decision, mask, mask center)"; decision is a string
          which can take the value "benign" or "malignant", mask
          is a binary matrix, mask center is a two-element
          integer matrix x,y
        - the model uses CUDA compilation tool 11.2, CUDA 11.4
          and CUdnn 8.1 or is compatible with them
        - only one model can be uploaded at a time!!!


- the `analyze_vanilla` class contains sample data analysis. Has
  access to a queue of data uploaded sequentially (ultimately
  a series of images),

- So the analysis may be based not on one image, but on a series.
  A queue is different from library queues in Python because it
  gives access to each element of the queue independently.

- The current sample uploads the image and the fictitious
  position of the head in space to the queue and returns a tuple
  in the result. (call python3 analyze...)

- The resulting tuple should contain the result of the analysis:
  (order of importance for linking with other parts of the
  system)

  1. coordinates of the point from the suspicious area - if the
     coordinates are empty or negative, it means that there is no
     suspicious area.
  2. mask (image with image resolution 0-1)
  3. result of the classification of the area around p.1 (may be
     a descriptive string "normal", "benign", "malignant")

- sample outline of the result: ([23,45], ... "benign")

### Rules:

- everyone makes modifications on their own branch to avoid
  conflicts
- merging individual changes to the main branch after discussing
  in the group what is worth updating


- The models to upload for the analyzer_vanilla script are below:
    - https://drive.google.com/drive/folders/1q7lDScaHZ6-DB9_aVsowo8ZHyHfv3gGP?usp=sharing

analyzer_vanilla:
1. Classification - ResNet50 trained to classify breast cancer images into benign/malignant
2. Segmentation - Plain u-net
