from PIL import Image
import numpy as np

# Load the image
image_path = '/mnt/data/malignant_5--ID_6065--436x540--ARIETTA.png'
image = Image.open(image_path)

# Convert the image to an array
image_array = np.array(image)

# Check if the image is greyscale or RGB
# If the array has 3 dimensions, it's likely RGB or RGBA; if 2, it's greyscale
is_greyscale = image_array.ndim == 2
channels = image_array.shape[-1] if not is_greyscale else 1

# Analyze basic properties of the image
# Calculate the mean and standard deviation of the pixel intensity values
mean_intensity = np.mean(image_array)
std_intensity = np.std(image_array)

# Calculate histogram of pixel intensity values
histogram, bin_edges = np.histogram(image_array, bins=256)

{
    "is_greyscale": is_greyscale,
    "channels": channels,
    "mean_intensity": mean_intensity,
    "std_intensity": std_intensity,
    "histogram": histogram.tolist(),
    "histogram_bin_edges": bin_edges.tolist()
}



