import os
import shutil

# Define folder paths

# create 'masks' test data for a model trained WITHOUT masks
source_folder    = "/mnt/dysk_roboczy/efarell/repos/US-Data/2023-11-12/pipeline-005/20-Renamed-Images/VALID-FOR-TRAINING"
folder_get_names = "/mnt/dysk_roboczy/efarell/repos/US-Data/2023-11-12/pipeline-004/Training-Pool-arietta/test"
target_folder    = "/mnt/dysk_roboczy/efarell/repos/US-Data/2023-11-12/pipeline-004/Training-Pool-arietta/test-have-masks"


# # create 'no masks' test data for a model trained WITH masks
# source_folder    = "/mnt/dysk_roboczy/efarell/repos/US-Data/2023-11-12/pipeline-004/20-Renamed-Images/VALID-FOR-TRAINING"
# folder_get_names = "/mnt/dysk_roboczy/efarell/repos/US-Data/2023-11-12/pipeline-005/Training-Pool-arietta/test"
# target_folder    = "/mnt/dysk_roboczy/efarell/repos/US-Data/2023-11-12/pipeline-005/Training-Pool-arietta/test-no-masks"

# Ensure the target folder exists
os.makedirs(target_folder, exist_ok=False)

# List all filenames in folder_get_names
filenames = os.listdir(folder_get_names)

# Copy files from source_folder to target folder if they exist in folder_get_names
for filename in filenames:
    source_file = os.path.join(source_folder, filename)
    target_file = os.path.join(target_folder, filename)
    if os.path.isfile(source_file):
        shutil.copy2(source_file, target_file)
    else:
        print(f"File not found in source_folder: {filename}")

print('Done.')
