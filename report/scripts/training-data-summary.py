
import pandas as pd
import matplotlib.pyplot as plt

# SELECT COALESCE(type, 'NULL') as type, BIRADS, COUNT(*) 
# FROM exported_images 
# GROUP BY COALESCE(type, 'NULL'), BIRADS 
# ORDER BY COALESCE(type, 'NULL'), BIRADS;


# SQL query output
data = [
    ("NULL",      "0",    2),    
    ("NULL",      "2",    122),  
    ("NULL",      "3",    118),  
    ("NULL",      "4",    2),    
    ("NULL",      "4a",   41),   
    ("NULL",      "4b",   32),   
    ("NULL",      "4c",   47),   
    ("NULL",      "5",    42),   
    ("benign",    "0",    4),    
    ("benign",    "2",    233),  
    ("benign",    "3",    241),  
    ("benign",    "4",    6),    
    ("benign",    "4a",   640),  
    ("benign",    "4b",   352),  
    ("benign",    "4c",   132),  
    ("benign",    "5",    74),   
    ("malignant", "0",    1),    
    ("malignant", "4",    4),    
    ("malignant", "4a",   14),   
    ("malignant", "4b",   171),  
    ("malignant", "4c",   351),  
    ("malignant", "5",    865),  
    ("normal",    "1",    99)

]

# Create a DataFrame from your data
df = pd.DataFrame(data, columns=["type", "BIRADS", "COUNT"])

# Pivot the DataFrame
pivot_df = df.pivot(index='BIRADS', columns='type', values='COUNT').fillna(0)
pivot_df = pivot_df.astype(int)

colors = {
    'NULL': 'darkgray',
    'normal': 'thistle',
    'benign': 'darkseagreen',
    'malignant': 'rosybrown'
}

plot_title = 'November Data Export'
# Updating the legend title
legend_title = 'Pathology'

# Re-plotting with the legend title updated and counts positioned in the middle of each bar segment
fig, ax = plt.subplots(figsize=(12, 8))
pivot_df.plot(kind='bar', stacked=True, color=[colors[type] for type in pivot_df.columns], ax=ax)

# Deactivate the top and right spines
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)

plt.title(plot_title, fontsize=28)
plt.xlabel('BIRADS', fontsize=20, labelpad=20)
plt.ylabel('Count', fontsize=18)
plt.xticks(rotation=0, fontsize=26)
plt.yticks(fontsize=24)
# plt.legend(title=legend_title)
# Moving the legend to the middle underneath the plot title
plt.legend(title=legend_title, bbox_to_anchor=(0.5, 0.98), loc='upper center', ncol=len(pivot_df.columns))
plt.tight_layout()

# Adjusting text placement to be in the middle of each bar segment
for i, birads in enumerate(pivot_df.index):
    cumulative_count = 0  # Tracks the cumulative count to position text in the middle of the segment
    for j, type in enumerate(pivot_df.columns):
        count = pivot_df.loc[birads, type]
        if count > 0:
            text_position = cumulative_count + count / 2  # Positioning the text in the middle of the segment
            if birads in ['2', '3', '4a'] and type == 'benign':
                ax.text(i, text_position, str(count), ha='center', va='center', fontsize=14, color='white')
            elif birads in ['4b', '4c', '5'] and type in ['benign', 'malignant']:
                ax.text(i, text_position, str(count), ha='center', va='center', fontsize=14, color='white')
        cumulative_count += count  # Update the cumulative count

# Show plot
# plt.show()
plot_path = './training-data-summary.png'
plt.savefig(plot_path, bbox_inches='tight')
print('Created: ', plot_path)

