import pandas as pd
import matplotlib.pyplot as plt

DEVICE_MAP = {
    'Alpinion'           : 'ALPIN',
    'Alpinion XCUBE 90'  : 'XCUBE',
    'Esaote 6150'        : 'ESAOTE',
    'Hitachi ARIETTA 70' : 'ARIETTA',
    'Philips'            : 'Philips',
    'Samsung HERA W10'   : 'SAM HERA',
    'Samsung RS85'       : 'SAM RS85',
    'Voluson E8'         : 'VOL E8',
}                               


# Provided data
data = [
    ("Alpinion", "NULL", 26), ("Alpinion", "benign", 1), ("Alpinion", "normal", 3),
    ("Alpinion XCUBE 90", "benign", 2), ("Alpinion XCUBE 90", "malignant", 4),
    ("Esaote 6150", "benign", 495), ("Esaote 6150", "malignant", 762),
    ("Hitachi ARIETTA 70", "NULL", 186), ("Hitachi ARIETTA 70", "benign", 522), ("Hitachi ARIETTA 70", "malignant", 491),
    ("Philips", "NULL", 164), ("Philips", "benign", 390), ("Philips", "malignant", 120), ("Philips", "normal", 34),
    ("Samsung HERA W10", "malignant", 3),
    ("Samsung RS85", "NULL", 27), ("Samsung RS85", "benign", 268), ("Samsung RS85", "malignant", 26), ("Samsung RS85", "normal", 62),
    ("Voluson E8", "NULL", 3), ("Voluson E8", "benign", 4)
]

# Create a DataFrame from the data
df = pd.DataFrame(data, columns=["Device", "Pathology", "count"])
df['Device'] = df['Device'].map(DEVICE_MAP)

# Pivot the DataFrame
pivot_df = df.pivot(index='Device', columns='Pathology', values='count').fillna(0)
pivot_df = pivot_df.astype(int)

# Color mapping
colors = {
    'NULL': 'darkgray',
    'normal': 'thistle',
    'benign': 'darkseagreen',
    'malignant': 'rosybrown'
}

# Plotting
plot_title = 'November Data: Devices'
legend_title = 'Pathology'

fig, ax = plt.subplots(figsize=(12, 8))
pivot_df.plot(kind='bar', stacked=True, color=[colors[pathology] for pathology in pivot_df.columns], ax=ax)

ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)

plt.title(plot_title, fontsize=28)
plt.xlabel('Device', fontsize=20)
plt.ylabel('Count', fontsize=18)
plt.xticks(rotation=-40, fontsize=18, ha='left')
plt.yticks(fontsize=24)
# plt.legend(title=legend_title, bbox_to_anchor=(0.5, 0.98), loc='upper center', ncol=len(pivot_df.columns))
plt.legend(title=legend_title, loc='upper right')
plt.tight_layout()

# Save the plot
# plt.show()

plot_path = '../images/device-summary.png'
plt.savefig(plot_path, bbox_inches='tight')
print('Created: ', plot_path)

