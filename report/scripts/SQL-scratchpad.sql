-- In[]

select count(*) from exported_images;



-- In[]

PRAGMA table_info(exported_images);
.schema exported_images

-- PatientID             INTEGER,
-- TumorID               INTEGER,
-- ExaminationID         INTEGER,
-- ImageID               TEXT,
-- Type                  TEXT,
-- BIRADS                TEXT,
-- Image_filename        TEXT,
-- Tumor_mask_filename   TEXT,
-- Other_mask_filename   TEXT,
-- Pixel_width_X         REAL,
-- Pixel_width_Y         REAL,
-- LocX0                 INTEGER,
-- LocY0                 INTEGER,
-- LocX1                 INTEGER,
-- LocY1                 INTEGER,
-- Device_name           TEXT,
-- orig_width            INTEGER,
-- orig_height           INTEGER,
-- crop_width            INTEGER,
-- crop_height           INTEGER
-- resize_width          INTEGER,
-- resize_height         INTEGER


-- In[]

select BIRADS, count(*) from exported_images 
group by BIRADS;


SELECT COALESCE(type, 'NULL') as type, BIRADS, COUNT(*) 
FROM exported_images 
GROUP BY COALESCE(type, 'NULL'), BIRADS 
ORDER BY COALESCE(type, 'NULL'), BIRADS;


SELECT type, BIRADS, COUNT(*) 
FROM exported_images 
GROUP BY type, BIRADS 
ORDER BY type, BIRADS; 

WHERE type IS NOT NULL AND type <> '' 

select count(*) from exported_images 
WHERE type IS NULL;

select Device_name as 'Device', COALESCE(type, 'NULL') as 'Pathology', count(*) as 'count' from exported_images 
group by Device_name, type 
order by Device_name, type;

-- In[]


SELECT Device_name, resize_height, resize_width,  COUNT(*) AS num_images
FROM exported_images
GROUP BY Device_name, resize_height, resize_width
ORDER BY num_images DESC limit 20;

-- Device_name         resize_height  resize_width  num_images
-- ------------------  -------------  ------------  -----------
-- Esaote 6150         515            552           641
-- Esaote 6150         600            554           510
-- Hitachi ARIETTA 70  498            450           449
-- Hitachi ARIETTA 70  438            449           178
-- Hitachi ARIETTA 70  499            771           117
-- Hitachi ARIETTA 70  379            449           103
-- Philips             467            584           81
-- Hitachi ARIETTA 70  320            450           73
-- Hitachi ARIETTA 70  558            450           58
-- Hitachi ARIETTA 70  559            814           49
-- Philips             349            557           43
-- Samsung RS85        475            586           42
-- Hitachi ARIETTA 70  677            448           39
-- Philips             439            584           39
-- Philips             468            586           36
-- Philips             289            557           34
-- Philips             533            584           32
-- Esaote 6150         601            551           28
-- Hitachi ARIETTA 70  439            732           23
-- Hitachi ARIETTA 70  678            895           23



-- In[]

SELECT Device_name, orig_height, orig_width,  COUNT(*) AS num_images
FROM exported_images
GROUP BY Device_name, orig_height, orig_width
ORDER BY num_images DESC;


-- BY DEVICE
-- Device_name           orig_height  orig_width  num_images
-- ------------------    -----------  ----------  -----------
-- Alpinion              534          383         1
-- Alpinion              534          511         4
-- Alpinion              534          613         1
-- Alpinion              534          765         10
-- Alpinion              541          789         3
-- Alpinion              541          919         1
-- Alpinion              534          1019        4
-- Alpinion              534          1025        5
-- Alpinion              541          1025        1
-- Alpinion XCUBE 90     692          1149        6
-- Esaote 6150           480          616         1257
-- Hitachi ARIETTA 70    555          985         8
-- Hitachi ARIETTA 70    576          1024        1191
-- Philips               545          432         2
-- Philips               579          438         1
-- Philips               607          438         2
-- Philips               636          438         1
-- Philips               653          438         1
-- Philips               545          445         12
-- Philips               552          445         3
-- Philips               573          445         1
-- Philips               610          445         1
-- Philips               619          445         1
-- Philips               609          485         2
-- Philips               610          485         15
-- Philips               617          485         2
-- Philips               623          485         1
-- Philips               652          485         1
-- Philips               653          485         4
-- Philips               558          773         126
-- Philips               531          778         2
-- Philips               863          871         2
-- Philips               641          930         356
-- Philips               661          930         1
-- Philips               723          930         1
-- Philips               718          996         1
-- Philips               721          997         150
-- Philips               611          1024        1
-- Philips               641          1024        1
-- Philips               645          1067        1
-- Philips               646          1069        1
-- Philips               641          1071        1
-- Philips               701          1076        1
-- Philips               641          1077        1
-- Philips               765          1078        1
-- Philips               639          1079        1
-- Philips               642          1081        1
-- Philips               766          1082        1
-- Philips               768          1082        1
-- Philips               768          1086        1
-- Philips               700          1088        1
-- Philips               766          1090        1
-- Philips               768          1090        1
-- Philips               770          1092        1
-- Philips               765          1094        1
-- Philips               768          1112        1
-- Samsung HERA W10      776          1053        1
-- Samsung HERA W10      756          1128        1
-- Samsung HERA W10      776          1180        1
-- Samsung RS85          484          556         1
-- Samsung RS85          514          556         2
-- Samsung RS85          518          556         3
-- Samsung RS85          550          556         8
-- Samsung RS85          552          556         3
-- Samsung RS85          598          556         14
-- Samsung RS85          514          557         1
-- Samsung RS85          514          572         8
-- Samsung RS85          518          572         3
-- Samsung RS85          550          572         2
-- Samsung RS85          552          572         3
-- Samsung RS85          598          572         13
-- Samsung RS85          386          638         59
-- Samsung RS85          598          1023        213
-- Samsung RS85          599          1024        8
-- Samsung RS85          727          1128        1
-- Samsung RS85          756          1128        9
-- Samsung RS85          757          1151        6
-- Samsung RS85          598          1179        11
-- Samsung RS85          753          1180        4
-- Samsung RS85          776          1180        2
-- Samsung RS85          757          1251        1
-- Samsung RS85          630          1280        8
-- Voluson E8            662          976         3
-- Voluson E8            782          1136        4

-- BY num_images
-- Device_name         orig_height  orig_width    num_images
-- ------------------  -----------  ----------    -----------
-- Esaote 6150         480          616           1257
-- Hitachi ARIETTA 70  576          1024          1191
-- Philips             641          930           356
-- Samsung RS85        598          1023          213
-- Philips             721          997           150
-- Philips             558          773           126
-- Samsung RS85        386          638           59
-- Philips             610          485           15
-- Samsung RS85        598          556           14
-- Samsung RS85        598          572           13
-- Philips             545          445           12
-- Samsung RS85        598          1179          11
-- Alpinion            534          765           10
-- Samsung RS85        756          1128          9
-- Hitachi ARIETTA 70  555          985           8
-- Samsung RS85        550          556           8
-- Samsung RS85        514          572           8
-- Samsung RS85        599          1024          8
-- Samsung RS85        630          1280          8
-- Alpinion XCUBE 90   692          1149          6
-- Samsung RS85        757          1151          6
-- Alpinion            534          1025          5
-- Alpinion            534          511           4
-- Alpinion            534          1019          4
-- Philips             653          485           4
-- Samsung RS85        753          1180          4
-- Voluson E8          782          1136          4
-- Alpinion            541          789           3
-- Philips             552          445           3
-- Samsung RS85        518          556           3
-- Samsung RS85        552          556           3
-- Samsung RS85        518          572           3
-- Samsung RS85        552          572           3
-- Voluson E8          662          976           3
-- Philips             545          432           2
-- Philips             607          438           2
-- Philips             609          485           2
-- Philips             617          485           2
-- Philips             531          778           2
-- Philips             863          871           2
-- Samsung RS85        514          556           2
-- Samsung RS85        550          572           2
-- Samsung RS85        776          1180          2
-- Alpinion            534          383           1
-- Alpinion            534          613           1
-- Alpinion            541          919           1
-- Alpinion            541          1025          1
-- Philips             579          438           1
-- Philips             636          438           1
-- Philips             653          438           1
-- Philips             573          445           1
-- Philips             610          445           1
-- Philips             619          445           1
-- Philips             623          485           1
-- Philips             652          485           1
-- Philips             661          930           1
-- Philips             723          930           1
-- Philips             718          996           1
-- Philips             611          1024          1
-- Philips             641          1024          1
-- Philips             645          1067          1
-- Philips             646          1069          1
-- Philips             641          1071          1
-- Philips             701          1076          1
-- Philips             641          1077          1
-- Philips             765          1078          1
-- Philips             639          1079          1
-- Philips             642          1081          1
-- Philips             766          1082          1
-- Philips             768          1082          1
-- Philips             768          1086          1
-- Philips             700          1088          1
-- Philips             766          1090          1
-- Philips             768          1090          1
-- Philips             770          1092          1
-- Philips             765          1094          1
-- Philips             768          1112          1
-- Samsung HERA W10    776          1053          1
-- Samsung HERA W10    756          1128          1
-- Samsung HERA W10    776          1180          1
-- Samsung RS85        484          556           1
-- Samsung RS85        514          557           1
-- Samsung RS85        727          1128          1
-- Samsung RS85        757          1251          1



-- In[]

SELECT Device_name, COUNT(*) AS num_images
FROM exported_images
GROUP BY Device_name
ORDER BY num_images DESC;


-- Esaote 6150           1,257
-- Hitachi ARIETTA 70    1,199
-- Philips                 708
-- Samsung RS85            383
-- Alpinion                 30
-- Voluson E8                7
-- Alpinion XCUBE 90         6
-- Samsung HERA W10          3


-- In[]


SELECT Device_name, orig_width, orig_height, COUNT(*) AS num_rows
FROM exported_images
GROUP BY Device_name, orig_width, orig_height
ORDER BY Device_name, orig_width, orig_height;

--                       w       h       crop_w  crop_h
--                       ---     ---     ------  ------
-- Alpinion              383     534     381     533     1
-- Alpinion              511     534     509     533     4
-- Alpinion              613     534     611     533     1
-- Alpinion              765     534     763     533     10
-- Alpinion              789     541     787     538     3
-- Alpinion              919     541     858     538     1
-- Alpinion              1019    534     881     533     4
-- Alpinion              1025    534     881     533     5
-- Alpinion              1025    541     868     538     1

--                       w       h       crop_w  crop_h
--                       ---     ---     ------  ------
-- Alpinion XCUBE 90     1149    692     1009    691     1
-- Alpinion XCUBE 90     1149    692     1092    677     5

--                       w       h       crop_w  crop_h
--                       ---     ---     ------  ------
-- Esaote 6150           616     480     300     469     2
-- Esaote 6150           616     480     334     469     14
-- Esaote 6150           616     480     376     469     20
-- Esaote 6150           616     480     378     470     4
-- Esaote 6150           616     480     430     469     28
-- Esaote 6150           616     480     432     468     518
-- Esaote 6150           616     480     502     469     645
-- Esaote 6150           616     480     504     468     1
-- Esaote 6150           616     480     599     468     21
-- Esaote 6150           616     480     605     472     4

--                       w       h       crop_w  crop_h
--                       ---     ---     ------  ------
-- Hitachi ARIETTA 70    985     555     465     515     5
-- Hitachi ARIETTA 70    985     555     599     504     2
-- Hitachi ARIETTA 70    985     555     797     516     1
-- Hitachi ARIETTA 70    1024    576     362     546     39
-- Hitachi ARIETTA 70    1024    576     436     538     1
-- Hitachi ARIETTA 70    1024    576     436     540     58
-- Hitachi ARIETTA 70    1024    576     484     536     449
-- Hitachi ARIETTA 70    1024    576     544     531     178
-- Hitachi ARIETTA 70    1024    576     622     525     103
-- Hitachi ARIETTA 70    1024    576     640     554     5
-- Hitachi ARIETTA 70    1024    576     674     550     6
-- Hitachi ARIETTA 70    1024    576     722     547     23
-- Hitachi ARIETTA 70    1024    576     726     517     73
-- Hitachi ARIETTA 70    1024    576     788     540     4
-- Hitachi ARIETTA 70    1024    576     788     541     49
-- Hitachi ARIETTA 70    1024    576     819     536     8
-- Hitachi ARIETTA 70    1024    576     828     537     2
-- Hitachi ARIETTA 70    1024    576     829     536     7
-- Hitachi ARIETTA 70    1024    576     830     505     1
-- Hitachi ARIETTA 70    1024    576     830     536     6
-- Hitachi ARIETTA 70    1024    576     830     537     117
-- Hitachi ARIETTA 70    1024    576     872     505     6
-- Hitachi ARIETTA 70    1024    576     886     531     4
-- Hitachi ARIETTA 70    1024    576     886     532     23
-- Hitachi ARIETTA 70    1024    576     954     525     3
-- Hitachi ARIETTA 70    1024    576     956     526     16
-- Hitachi ARIETTA 70    1024    576     1023    518     10

--                       w       h       crop_w  crop_h
--                       ---     ---     ------  ------
-- Philips               432     545     431     544     2
-- Philips               438     579     437     578     1
-- Philips               438     607     437     606     2
-- Philips               438     636     437     635     1
-- Philips               438     653     437     652     1
-- Philips               445     545     444     544     12
-- Philips               445     552     444     551     3
-- Philips               445     573     444     572     1
-- Philips               445     610     444     609     1
-- Philips               445     619     444     544     1
-- Philips               485     609     484     608     2
-- Philips               485     610     484     609     15
-- Philips               485     617     484     616     2
-- Philips               485     623     484     622     1
-- Philips               485     652     484     651     1
-- Philips               485     653     484     652     4
-- Philips               773     558     338     477     2
-- Philips               773     558     438     516     1
-- Philips               773     558     477     483     3
-- Philips               773     558     497     515     5
-- Philips               773     558     528     483     4
-- Philips               773     558     568     508     1
-- Philips               773     558     595     475     35
-- Philips               773     558     602     324     10
-- Philips               773     558     602     390     4
-- Philips               773     558     602     455     18
-- Philips               773     558     620     324     8
-- Philips               773     558     621     455     11
-- Philips               773     558     621     509     10
-- Philips               773     558     623     390     14
-- Philips               778     531     777     447     2
-- Philips               871     863     870     843     2
-- Philips               930     641     433     617     2
-- Philips               930     641     507     614     3
-- Philips               930     641     513     625     2
-- Philips               930     641     531     632     8
-- Philips               930     641     608     617     12
-- Philips               930     641     608     618     7
-- Philips               930     641     674     571     1
-- Philips               930     641     674     599     1
-- Philips               930     641     675     571     8
-- Philips               930     641     675     578     1
-- Philips               930     641     675     599     1
-- Philips               930     641     675     602     8
-- Philips               930     641     675     617     32
-- Philips               930     641     710     579     4
-- Philips               930     641     710     607     2
-- Philips               930     641     760     571     39
-- Philips               930     641     760     575     3
-- Philips               930     641     760     599     14
-- Philips               930     641     760     602     2
-- Philips               930     641     760     603     1
-- Philips               930     641     760     608     81
-- Philips               930     641     764     571     1
-- Philips               930     641     782     579     1
-- Philips               930     641     789     403     2
-- Philips               930     641     789     486     1
-- Philips               930     641     789     567     1
-- Philips               930     641     793     403     1
-- Philips               930     641     793     412     34
-- Philips               930     641     793     413     1
-- Philips               930     641     793     486     3
-- Philips               930     641     793     494     1
-- Philips               930     641     793     553     4
-- Philips               930     641     793     568     5
-- Philips               930     641     794     552     1
-- Philips               930     641     794     553     1
-- Philips               930     641     794     558     5
-- Philips               930     641     794     561     1
-- Philips               930     641     794     567     3
-- Philips               930     641     794     582     14
-- Philips               930     641     794     583     1
-- Philips               930     641     796     489     1
-- Philips               930     641     796     499     42
-- Philips               930     661     725     648     1
-- Philips               930     723     885     708     1
-- Philips               996     718     807     717     1
-- Philips               997     721     433     618     2
-- Philips               997     721     563     660     2
-- Philips               997     721     610     618     10
-- Philips               997     721     636     659     7
-- Philips               997     721     676     572     6
-- Philips               997     721     676     600     2
-- Philips               997     721     676     618     6
-- Philips               997     721     761     572     17
-- Philips               997     721     761     600     7
-- Philips               997     721     761     609     15
-- Philips               997     721     793     413     21
-- Philips               997     721     795     531     1
-- Philips               997     721     795     559     17
-- Philips               997     721     795     583     9
-- Philips               997     721     796     462     1
-- Philips               997     721     796     500     20
-- Philips               997     721     796     504     2
-- Philips               997     721     796     559     1
-- Philips               997     721     805     636     3
-- Philips               997     721     996     573     1
-- Philips               1024    611     756     570     1
-- Philips               1024    641     891     616     1
-- Philips               1067    645     939     480     1
-- Philips               1069    646     943     492     1
-- Philips               1071    641     939     480     1
-- Philips               1076    701     946     593     1
-- Philips               1077    641     939     480     1
-- Philips               1078    765     903     725     1
-- Philips               1079    639     939     480     1
-- Philips               1081    642     939     480     1
-- Philips               1082    766     725     736     1
-- Philips               1082    768     908     725     1
-- Philips               1086    768     902     725     1
-- Philips               1088    700     946     594     1
-- Philips               1090    766     903     725     1
-- Philips               1090    768     903     725     1
-- Philips               1092    770     725     736     1
-- Philips               1094    765     725     736     1
-- Philips               1112    768     725     735     1

--                       w       h       crop_w  crop_h
--                       ---     ---     ------  ------
-- Samsung HERA W10      1053    776     784     597     1
-- Samsung HERA W10      1128    756     1037    753     1
-- Samsung HERA W10      1180    776     1088    597     1

--                       w       h       crop_w  crop_h
--                       ---     ---     ------  ------
-- Samsung RS85          556     484     555     483     1
-- Samsung RS85          556     514     555     513     2
-- Samsung RS85          556     518     555     517     3
-- Samsung RS85          556     550     555     549     8
-- Samsung RS85          556     552     555     551     3
-- Samsung RS85          556     598     555     597     14
-- Samsung RS85          557     514     556     513     1
-- Samsung RS85          572     514     571     513     8
-- Samsung RS85          572     518     571     517     3
-- Samsung RS85          572     550     571     549     2
-- Samsung RS85          572     552     571     551     3
-- Samsung RS85          572     598     571     597     13
-- Samsung RS85          638     386     346     348     1
-- Samsung RS85          638     386     346     385     3
-- Samsung RS85          638     386     380     385     2
-- Samsung RS85          638     386     423     385     7
-- Samsung RS85          638     386     465     385     1
-- Samsung RS85          638     386     475     144     1
-- Samsung RS85          638     386     475     192     5
-- Samsung RS85          638     386     475     240     9
-- Samsung RS85          638     386     475     288     7
-- Samsung RS85          638     386     475     324     1
-- Samsung RS85          638     386     475     337     6
-- Samsung RS85          638     386     475     346     1
-- Samsung RS85          638     386     475     347     7
-- Samsung RS85          638     386     475     348     4
-- Samsung RS85          638     386     475     385     4
-- Samsung RS85          1023    598     346     597     1
-- Samsung RS85          1023    598     516     597     2
-- Samsung RS85          1023    598     525     597     1
-- Samsung RS85          1023    598     535     597     6
-- Samsung RS85          1023    598     543     597     2
-- Samsung RS85          1023    598     589     597     7
-- Samsung RS85          1023    598     642     597     1
-- Samsung RS85          1023    598     655     466     2
-- Samsung RS85          1023    598     655     597     9
-- Samsung RS85          1023    598     737     466     6
-- Samsung RS85          1023    598     737     502     6
-- Samsung RS85          1023    598     737     517     5
-- Samsung RS85          1023    598     737     551     5
-- Samsung RS85          1023    598     737     597     42
-- Samsung RS85          1023    598     843     466     6
-- Samsung RS85          1023    598     843     502     6
-- Samsung RS85          1023    598     843     512     1
-- Samsung RS85          1023    598     843     517     1
-- Samsung RS85          1023    598     843     597     23
-- Samsung RS85          1023    598     900     384     1
-- Samsung RS85          1023    598     928     192     2
-- Samsung RS85          1023    598     929     288     4
-- Samsung RS85          1023    598     929     359     9
-- Samsung RS85          1023    598     929     384     16
-- Samsung RS85          1023    598     929     385     1
-- Samsung RS85          1023    598     929     408     5
-- Samsung RS85          1023    598     929     444     2
-- Samsung RS85          1023    598     929     455     1
-- Samsung RS85          1023    598     929     456     2
-- Samsung RS85          1023    598     929     482     17
-- Samsung RS85          1023    598     929     492     1
-- Samsung RS85          1023    598     929     578     20
-- Samsung RS85          1024    599     655     597     4
-- Samsung RS85          1024    599     737     519     2
-- Samsung RS85          1024    599     737     597     2
-- Samsung RS85          1128    727     1037    695     1
-- Samsung RS85          1128    756     748     753     2
-- Samsung RS85          1128    756     934     753     2
-- Samsung RS85          1128    756     1021    753     1
-- Samsung RS85          1128    756     1027    755     1
-- Samsung RS85          1128    756     1065    714     1
-- Samsung RS85          1128    756     1065    753     2
-- Samsung RS85          1151    757     696     561     5
-- Samsung RS85          1151    757     988     597     1
-- Samsung RS85          1179    598     558     561     2
-- Samsung RS85          1179    598     848     597     8
-- Samsung RS85          1179    598     930     561     1
-- Samsung RS85          1180    753     1043    727     1
-- Samsung RS85          1180    753     1088    727     3
-- Samsung RS85          1180    776     1088    773     2
-- Samsung RS85          1251    757     696     561     1
-- Samsung RS85          1280    630     778     627     1
-- Samsung RS85          1280    630     890     627     6
-- Samsung RS85          1280    630     1038    627     1

--                       w       h       crop_w  crop_h
--                       ---     ---     ------  ------
-- Voluson E8            976     662     780     656     3
-- Voluson E8            1136    782     949     769     3
-- Voluson E8            1136    782     959     712     1



-- In[]


-- select ImageID, Image_filename, Tumor_mask_filename  from exported_images

-- In[]

select count(*) from exported_images
where Tumor_mask_filename not NULL
order by Tumor_mask_filename;


-- In[]

select * from exported_images limit 10;


-- In[]

-- Cropping and resizing
 select ImageID from exported_images;

-- In[]


select ImageID,
        Type, BIRADS,
        round(Pixel_width_X, 4),
        round(Pixel_width_Y, 4),
        orig_width as "orig_w",
        orig_height as "orig_h",
        crop_width as "crop_w",
        crop_height as "crop_w",
        resize_width as "resize_w",
        resize_height as "resize_w"
        from exported_images;


-- In[]

select avg(resize_width) as avg_resize_w,
       avg(resize_height) as avg_resize_h
        from exported_images;

-- avg_resize_w      avg_resize_h
-- ----------------  ----------------
-- 543.512941831339  487.890620651266


-- In[]

-- "OTHER" MASKS

select * from exported_images where Other_mask_filename not NULL;

-- PatientID TumorID ExaminationID ImageID Type      BIRADS Image_filename Tumor_mask_filename         Other_mask_filename                                     Pixel_width_X      Pixel_width_Y      LocX0 LocY0 LocX1 LocY1 Device_name        ImageWidth ImageHeight
-- --------- ------- ------------- ------- --------- ------ -------------- --------------------------- ------------------------------------------------------- ------------------ ------------------ ----- ----- ----- ----- ------------------ ---------- -----------
--       423     423           436 2605    benign    4b     2605.jpg       2605_92.png                 2605_93.png                                             0.0064620355411954 0.0064620355411954    36    11   796   619 Philips                   930         641
--       408     408           419 2547              3      2547.jpg       2547_135.png                2547_136.png;2547_137.png                               0.0064620355411954 0.0064620355411954    36    11   796   619 Philips                   930         641
--       408     408           419 2548              3      2548.jpg       2548_138.png                2548_139.png;2548_140.png                               0.0064620355411954 0.0064620355411954    36    11   796   619 Philips                   930         641
--       825     850           881 5031    benign    4b     5031.jpg       5031_360.png                5031_361.png;5031_362.png                                 0.00442477876106   0.00442477876106    56     7  1148   684 Alpinion XCUBE 90        1149         692
--       814     831           862 4967              4a     4967.jpg       4967_428.png                4967_429.png                                            0.0064620355411954 0.0064620355411954    36    11   796   619 Philips                   930         641
--       814     831           862 4968              4a     4968.jpg       4968_430.png                4968_431.png                                            0.0064620355411954 0.0064620355411954    36    11   796   619 Philips                   930         641
--       814     831           862 4969              4a     4969.jpg       4969_433.png                4969_434.png                                            0.0064620355411954 0.0064620355411954    36    11   796   619 Philips                   930         641
--       814     831           862 4970              4a     4970.jpg       4970_435.png                4970_436.png                                            0.0064620355411954 0.0064620355411954    36    11   796   619 Philips                   930         641
--       811     826           857 4951    benign    2      4951.jpg       4951_452.png                4951_451.png                                            0.0064620355411954 0.0064620355411954    36    11   796   619 Philips                   930         641
--       988    1017          1051 5337    benign    4b     5337.png       5337_793.png                5337_794.png                                            0.0076999999582767 0.0076999999582767    57     2   559   471 Esaote 6150               616         480
--       987    1016          1050 5336    benign    4a     5336.png       5336_795.png                5336_797.png                                             0.009239999949932  0.009239999949932    57     2   559   471 Esaote 6150               616         480
--       984    1013          1047 5333    benign    4b     5333.png       5333_801.png                5333_802.png;5333_803.png                                0.009239999949932  0.009239999949932    57     2   559   471 Esaote 6150               616         480
--       955     984          1018 5303    benign    4b     5303.png       5303_1030.png               5303_1032.png                                           0.0138600006699562 0.0138600006699562   141     2   475   471 Esaote 6150               616         480
--      1143    1200          1236 5701    benign    4a     5701.png       5701_1144.png               5701_1146.png;5701_1147.png                              0.009239999949932  0.009239999949932    57     2   559   471 Esaote 6150               616         480
--      1071    1121          1155 5541    benign    2      5541.jpg       5541_1162.png               5541_1161.png                                           0.0072697899838449 0.0072697899838449    78    11   753   628 Philips                   930         641
--      1071    1121          1155 5542    benign    2      5542.jpg       5542_1163.png               5542_1164.png                                           0.0072697899838449 0.0072697899838449    59     3   769   582 Philips                   930         641
--      1071    1121          1155 5543    benign    2      5543.jpg       5543_1165.png               5543_1166.png                                           0.0072697899838449 0.0072697899838449    59     3   769   610 Philips                   930         641
--      1071    1118          1152 5535    benign    2      5535.jpg       5535_1169.png               5535_1170.png;5535_1171.png                             0.0064620355411954 0.0064620355411954    36    11   796   582 Philips                   930         641
--      1068    1108          1142 5489    benign    3      5489.jpg       5489_1203.png               5489_1204.png;5489_1205.png                             0.0051903114186851 0.0051903114186851    58   155   987   443 Samsung RS85             1023         598
--      1068    1108          1142 5490    benign    3      5490.jpg       5490_1206.png               5490_1207.png                                           0.0051903114186851 0.0051903114186851    58   155   987   443 Samsung RS85             1023         598
--      1068    1108          1142 5491    benign    3      5491.jpg       5491_1208.png               5491_1209.png;5491_1210.png;5491_1211.png;5491_1212.png 0.0103448275862069 0.0103448275862069    82   121   557   265 Samsung RS85              638         386
--      1068    1106          1140 5479    benign    2      5479.jpg       5479_1223.png               5479_1224.png;5479_1225.png                             0.0066889632107023 0.0066889632107023   143     0   880   597 Samsung RS85             1023         598
--      1068    1106          1140 5480    benign    2      5480.jpg       5480_1226.png               5480_1227.png;5480_1228.png                             0.0066889632107023 0.0066889632107023   143     0   880   551 Samsung RS85             1023         598
--      1068    1106          1140 5481    benign    2      5481.jpg       5481_1229.png               5481_1230.png                                           0.0066889632107023 0.0066889632107023   143     0   880   597 Samsung RS85             1023         598
--      1068    1106          1140 5482    benign    2      5482.jpg       5482_1231.png               5482_1232.png                                           0.0103626943005181 0.0103626943005181    82     0   557   347 Samsung RS85              638         386
--      1068    1106          1140 5478    benign    2      5478.jpg       5478_1218.png               5478_1219.png;5478_1220.png;5478_1221.png;5478_1222.png 0.0066889632107023 0.0066889632107023   143     0   880   551 Samsung RS85             1023         598
--      1030    1067          1101 5428    benign    2      5428.jpg       5428_1242.png               5428_1243.png                                           0.0072697899838449 0.0072697899838449    78    11   753   628 Philips                   930         641
--      1178    1237          1274 5759    malignant 4c     5759.jpg       5759_1338.png               5759_1339.png                                           0.0044247787610619 0.0044247787610619    56     7  1148   684 Alpinion XCUBE 90        1149         692
--      1189    1248          1285 5780    malignant 5      5780.png       5780_1413.png               5780_1416.png                                           0.0107799999415874 0.0107799999415874    93     2   523   471 Esaote 6150               616         480
--       911     940           974 5255    benign    4b     5255.png       5255_1481.png               5255_1482.png                                            0.009239999949932  0.009239999949932    57     2   559   471 Esaote 6150               616         480
--      1364    1424          1461 5994    benign    4a     5994.png       5994_1911.png               5994_1913.png;5994_1914.png;5994_1915.png                0.009239999949932  0.009239999949932    57     2   559   471 Esaote 6150               616         480
--      1432    1493          1530 6088    malignant 4c     6088.png       6088_2027.png               6088_2029.png                                            0.009239999949932  0.009239999949932    57     2   559   471 Esaote 6150               616         480
--      1768    1862          1900 6903    benign    4a     6903.png       6903_2176.png               6903_2177.png;6903_2178.png                                      0.0078125          0.0078125   270    39   754   575 Hitachi ARIETTA 70       1024         576
--      1768    1862          1900 6904    benign    4a     6904.png       6904_2181.png               6904_2182.png;6904_2183.png                                      0.0078125          0.0078125   270    39   754   575 Hitachi ARIETTA 70       1024         576
--      1768    1862          1900 6905    benign    4a     6905.png       6905_2186.png               6905_2187.png;6905_2188.png                                      0.0078125          0.0078125   270    39   754   575 Hitachi ARIETTA 70       1024         576
--      1702    1785          1823 6584    benign    2      6584.jpg       6584_2314.png               6584_2315.png;6584_2316.png;6584_2317.png               0.0058823529411764 0.0058823529411764     0    65   796   564 Philips                   930         641
--      1700    1782          1819 6572    benign    2      6572.jpg       6572_2326.png               6572_2327.png                                           0.0091973244147157 0.0091973244147157   244     0   779   597 Samsung RS85             1023         598
--      1700    1782          1819 6574    benign    2      6574.jpg       6574_2328.png               6574_2329.png                                           0.0091973244147157 0.0091973244147157   244     0   779   597 Samsung RS85             1023         598
--      1925    2022          2060 7387    benign    4b     7387.png       7387_2362.png               7387_5538.png;7387_5539.png                                      0.0078125          0.0078125    97    38   927   575 Hitachi ARIETTA 70       1024         576
--      1562    1623          1660 6219    benign    4a     6219.png       6219_2668.png               6219_2669.png;6219_2670.png                              0.009239999949932  0.009239999949932    57     2   559   471 Esaote 6150               616         480
--      1570    1631          1668 6227    benign    4a     6227.png       6227_2685.png               6227_2686.png                                            0.009239999949932  0.009239999949932    57     2   559   471 Esaote 6150               616         480
--      1600    1661          1698 6257    benign    4c     6257.png       6257_2740.png               6257_2742.png                                            0.009239999949932  0.009239999949932    57     2   559   471 Esaote 6150               616         480
--      2030    2134          2172 7743    malignant 5      7743.png       7743_2778.png               7743_2779.png                                                    0.0078125          0.0078125   270    39   754   575 Hitachi ARIETTA 70       1024         576
--      2030    2134          2172 7744    malignant 5      7744.png       7744_2780.png               7744_2781.png                                                    0.0078125          0.0078125   270    39   754   575 Hitachi ARIETTA 70       1024         576
--      1647    1708          1745 6304    benign    4b     6304.png       6304_2864.png               6304_2865.png                                            0.009239999949932  0.009239999949932    57     2   559   471 Esaote 6150               616         480
--      1733    1825          1863 6786    benign    4c     6786.png       6786_2951.png               6786_2952.png;6786_2953.png;6786_2954.png               0.0107799999415874 0.0107799999415874    92     4   524   472 Esaote 6150               616         480
--      1750    1842          1880 6839    malignant 5      6839.png       6839_3029.png               6839_3030.png                                           0.0107799999415874 0.0107799999415874    92     4   524   472 Esaote 6150               616         480
--      1775    1869          1907 6924    malignant 4c     6924.png       6924_3114.png               6924_3117.png                                           0.0107799999415874 0.0107799999415874    92     4   524   472 Esaote 6150               616         480
--      1775    1869          1907 6925    malignant 4c     6925.png       6925_3118.png               6925_3121.png                                           0.0107799999415874 0.0107799999415874    92     4   524   472 Esaote 6150               616         480
--      1775    1869          1907 6926    malignant 4c     6926.png       6926_3122.png               6926_3125.png                                           0.0107799999415874 0.0107799999415874    92     4   524   472 Esaote 6150               616         480
--      1813    1907          1945 7047    benign    4a     7047.png       7047_3284.png               7047_3285.png                                           0.0107799999415874 0.0107799999415874    92     4   524   472 Esaote 6150               616         480
--      1839    1933          1971 7119    malignant 4b     7119.png       7119_3388.png               7119_3390.png                                           0.0107799999415874 0.0107799999415874    92     4   524   472 Esaote 6150               616         480
--      1839    1933          1971 7120    malignant 4b     7120.png       7120_3391.png               7120_3393.png                                           0.0107799999415874 0.0107799999415874    92     4   524   472 Esaote 6150               616         480
--      2084    2192          2231 8029    benign    4b     8029.jpg       8029_3678.png               8029_5540.png                                           0.0059101654846335 0.0059101654846335     3   109   796   521 Philips                   930         641
--      2073    2181          2219 7962    malignant 5      7962.jpg       7962_3714.png               7962_3715.png                                           0.0072697899838449 0.0072697899838449    78    11   753   628 Philips                   930         641
--      1690    1760          1797 6482    benign    3      6482.jpg       6482_3868.png               6482_5541.png                                            0.005181347150259  0.005181347150259    58   107   987   491 Samsung RS85             1023         598
--      1680    1742          1779 6388    benign    3      6388.jpg       6388_4002.png               6388_4003.png                                            0.005181347150259  0.005181347150259    58   107   987   491 Samsung RS85             1023         598
--      1680    1742          1779 6389    benign    3      6389.jpg       6389_4004.png               6389_4005.png                                           0.0103626943005181 0.0103626943005181    82    97   557   289 Samsung RS85              638         386
--      1676    1738          1775 6343    benign    3      6343.jpg       6343_4260.png               6343_4261.png                                           0.0064620355411954 0.0064620355411954    36    11   796   610 Philips                   930         641
--      1887    1981          2019 7271    malignant 5      7271.png       7271_4328.png               7271_4331.png                                           0.0107799999415874 0.0107799999415874    92     4   524   472 Esaote 6150               616         480
--      1888    1982          2020 7275    malignant 5      7275.png       7275_4332.png               7275_4335.png                                           0.0107799999415874 0.0107799999415874    92     4   524   472 Esaote 6150               616         480
--      1891    1985          2023 7283    benign    4b     7283.png       7283_4348.png               7283_4351.png                                           0.0107799999415874 0.0107799999415874    92     4   524   472 Esaote 6150               616         480
--      1906    2001          2039 7326    malignant 5      7326.png       7326_4388.png               7326_4389.png                                           0.0107799999415874 0.0107799999415874    92     4   524   472 Esaote 6150               616         480
--      1909    2004          2042 7333    malignant 4c     7333.png       7333_4434.png               7333_4435.png;7333_4437.png                             0.0107799999415874 0.0107799999415874    92     4   524   472 Esaote 6150               616         480
--      1909    2004          2042 7334    malignant 4c     7334.png       7334_4440.png               7334_4439.png;7334_4442.png                             0.0107799999415874 0.0107799999415874    92     4   524   472 Esaote 6150               616         480
--      1909    2004          2042 7336    malignant 4c     7336.png       7336_4444.png               7336_4443.png;7336_4445.png                             0.0107799999415874 0.0107799999415874    92     4   524   472 Esaote 6150               616         480
--      1953    2053          2091 7482    benign    4b     7482.png       7482_4519.png               7482_4521.png                                           0.0107799999415874 0.0107799999415874    92     4   524   472 Esaote 6150               616         480
--      1953    2053          2091 7483    benign    4b     7483.png       7483_4522.png               7483_4524.png                                           0.0107799999415874 0.0107799999415874    92     4   524   472 Esaote 6150               616         480
--      1954    2054          2092 7485    benign    4a     7485.png       7485_4525.png               7485_4526.png;7485_4527.png                             0.0107799999415874 0.0107799999415874    92     4   524   472 Esaote 6150               616         480
--      1954    2054          2092 7486    benign    4a     7486.png       7486_4528.png               7486_4529.png;7486_4530.png                             0.0107799999415874 0.0107799999415874    92     4   524   472 Esaote 6150               616         480
--      1972    2072          2110 7545    benign    4b     7545.png       7545_4595.png               7545_4598.png;7545_4599.png                             0.0107799999415874 0.0107799999415874    92     4   524   472 Esaote 6150               616         480
--      1985    2085          2123 7588    benign    4a     7588.png       7588_4640.png               7588_4641.png;7588_4642.png                             0.0107799999415874 0.0107799999415874    92     4   524   472 Esaote 6150               616         480
--      1985    2085          2123 7590    benign    4a     7590.png       7590_4643.png               7590_4644.png;7590_4645.png                             0.0107799999415874 0.0107799999415874    92     4   524   472 Esaote 6150               616         480
--      2000    2100          2138 7629    malignant 5      7629.png       7629_4708.png               7629_4711.png                                           0.0107799999415874 0.0107799999415874    92     4   524   472 Esaote 6150               616         480
--      2012    2112          2150 7663    benign    4a     7663.png       7663_4747.png               7663_4748.png                                           0.0107799999415874 0.0107799999415874    92     4   524   472 Esaote 6150               616         480
--      2012    2112          2150 7664    benign    4a     7664.png       7664_4749.png               7664_4750.png                                           0.0107799999415874 0.0107799999415874    92     4   524   472 Esaote 6150               616         480
--      2111    2224          2263 8158    malignant 4a     8158.png       8158_4809.png;8158_4810.png 8158_4811.png                                           0.0107799999415874 0.0107799999415874    92     4   524   472 Esaote 6150               616         480
--      2115    2228          2267 8170    benign    4a     8170.png       8170_4826.png               8170_4827.png                                           0.0107799999415874 0.0107799999415874    92     4   524   472 Esaote 6150               616         480
--      2176    2294          2333 8399    benign    4b     8399.png       8399_5050.png               8399_5051.png                                                    0.0078125          0.0078125   270    39   754   575 Hitachi ARIETTA 70       1024         576
--      2198    2328          2367 8538    malignant 4c     8538.png       8538_5150.png               8538_5151.png                                                    0.0078125          0.0078125   270    39   754   575 Hitachi ARIETTA 70       1024         576
--      2198    2328          2367 8539    malignant 4c     8539.png       8539_5152.png               8539_5153.png                                                    0.0078125          0.0078125   270    39   754   575 Hitachi ARIETTA 70       1024         576
--      2199    2329          2368 8542    benign    4a     8542.png       8542_5155.png               8542_5156.png                                                    0.0078125          0.0078125   270    39   754   575 Hitachi ARIETTA 70       1024         576
--      2200    2331          2370 8549    benign    4a     8549.png       8549_5159.png               8549_5160.png;8549_5161.png                             0.0069444444961845 0.0069444444961845   240    44   784   575 Hitachi ARIETTA 70       1024         576
--      2200    2331          2370 8550    benign    4a     8550.png       8550_5162.png               8550_5163.png                                           0.0069444444961845 0.0069444444961845   240    44   784   575 Hitachi ARIETTA 70       1024         576




-- In[]

-- Pixel size
select min(Pixel_width_X), max(Pixel_width_X), min(Pixel_width_Y), max(Pixel_width_Y) from exported_images;

-- min (Pixel_width_X)  0.0023255813953488
-- MAX (Pixel_width_X)  0.0155440414507772
-- min (Pixel_width_Y)  0.0023255813953488
-- MAX (Pixel_width_Y)  0.0155440414507772


-- Average pixel sizes
select avg(Pixel_width_X), avg(Pixel_width_Y) from exported_images;

-- avg(Pixel_width_X)  0.008367929415049099
-- avg(Pixel_width_Y)  0.008367929415049083

-- My resize Target: 0.0084

-- In[]

-- Image properties database
-- ./image-processing/image-properties.sqlite

-- SQL queries to update the device_name
UPDATE image_properties SET device_name = 'ALPIN_XCUBE' WHERE device_name = 'XCUBE'

UPDATE image_properties SET device_name = 'ALPIN' WHERE device_name = 'Alpinion'



1.  XCUBE
2.  ESAOTE
3.  ARIETTA
4.  Philips
5.  SAM_HERA
6.  SAM_RS85
7.  VOL_E8

name         |type   |
-------------+-------+
file_path    |TEXT   |
image_id     |TEXT   |
type         |TEXT   |
birads       |TEXT   |
file_format  |TEXT   |
device_name  |TEXT   |
height       |INTEGER|
width        |INTEGER|
channels     |INTEGER|
bit_depth    |TEXT   |
alpha_channel|BOOLEAN|
contrast     |REAL   |
texture      |REAL   |
edge_info    |INTEGER|
sharpness    |REAL   |
exif_data    |TEXT   |
summary_noise|REAL   |



PRAGMA table_info(image_properties);

select * from image_properties limit 20;

device_name  contrast   texture edge_info   sharpness   summary_noise
Philips      255.0      42.71   23198       7.79        2.7999128377203384
Philips      255.0      41.5    20021       8.26        2.787725478378127
Philips      255.0      40.16   16750       7.99        2.65717373847407
Philips      255.0      41.29   13797       7.68        2.6253983960323883
Philips      255.0      40.48   15130       8.21        2.7882012258351563
Philips      255.0      42.01   16921       7.83        2.596609312110393
Philips      255.0      42.27   16341       8.14        2.7335248644156724
Philips      255.0      44.89   16611       7.52        2.726321656975995
Philips      247.0      44.14   8569        7.75        2.4723160432745535
Philips      248.0      46.08   8632        7.68        2.4858363642210004
Philips      244.0      45.22   7748        7.59        2.425526304964543
Philips      240.0      41.56   8434        7.64        2.4632887916398762
Philips      247.0      44.2    8671        7.7         2.475814569326893
Philips      250.0      37.91   17501       7.16        2.481029173013243
Philips      255.0      43.41   24292       8.91        3.2882865471002125
Philips      255.0      40.3    23568       8.73        3.175030505931741
Philips      249.0      33.41   18977       7.44        2.5469262807946498
Philips      255.0      43.47   21016       8.4         3.17730139014603
Philips      242.0      33.36   23798       7.66        2.72952981208699
SAM_RS85     254.0      38.49   18180       6.66        2.40618171489822

-- In[]

1.  contrast   
2.  texture 
3.  edge_info   
4.  sharpness   
5.  summary_noise

-- In[]

