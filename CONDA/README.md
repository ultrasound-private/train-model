# Contents
    - Conda environment: Linux
    - Conda environment: Windows Surface
    - Note about Swin models
    - Server GPU info
    - Manually create Conda environment


# Linux

conda create -n pytorch-dev --file conda-env-pytorch-dev.txt --c pytorch --c nvidia


# Windows Surface laptop

conda create -n analiza_env --file conda-env-WINDOWS-SURFACE.txt --channel pytorch --channel conda-forge

    - Or Manually
        - conda create -n analiza_env python=3.8
        - conda activate analiza
        - conda install pytorch torchvision torchaudio cpuonly -c pytorch
        - conda install fastai -c conda-forge
        - conda install timm -c conda-forge
        - conda install scikit-image -c conda-forge

# Note:

- Note: `timm` must be at version `0.6.13` to use `Swin` models (due to bug?)

    - mamba install timm=0.6.13

# Server GPU

- The models were created on the 'liczyk2ZU' server.
- Nvidia Driver: 535.129.03
- CUDA Version:  12.2

Hostname:
`p406pc2.ippt.pan.pl`

nvidia-smi:
```
Thu Dec 14 2023
+---------------------------------------------------------------------------------------+
| NVIDIA-SMI 535.129.03             Driver Version: 535.129.03   CUDA Version: 12.2     |
|-----------------------------------------+----------------------+----------------------+
| GPU  Name                 Persistence-M | Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp   Perf          Pwr:Usage/Cap |         Memory-Usage | GPU-Util  Compute M. |
|                                         |                      |               MIG M. |
|=========================================+======================+======================|
|   0  NVIDIA RTX 6000 Ada Gene...    Off | 00000000:C1:00.0 Off |                  Off |
| 30%   35C    P8              29W / 300W |  12576MiB / 49140MiB |      0%      Default |
|                                         |                      |                  N/A |
+-----------------------------------------+----------------------+----------------------+
```



# Manually create Conda environment

```
conda create -n pytorch-test1 python=3.9
conda activate pytorch-test1
conda install mamba
mamba install pytorch torchvision torchaudio pytorch-cuda=12.1 -c pytorch -c nvidia
mamba install fastai
mamba install matplotlib chardet pyodbc pandas ipython sympy jupyter
mamba install opencv
mamba install seaborn nbdev timm
mamba install scikit-image

# Save package list
conda list -e > conda-env-pytorch-dev.txt

```
