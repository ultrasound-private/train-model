# In[]
from fastai.vision.all import *
import torch.nn.functional as F
import timm
from torchvision.transforms import ToPILImage

from datetime import datetime
import importlib.metadata as metadata
import utils_nn      as my_nn
import utils         as my_utils
import plot_utils
import numpy as np

import os, sys
import importlib
from PIL import Image
import matplotlib
from skimage.transform import resize
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import pandas as pd

import configparser
import argparse






if my_nn.jupyter_running():
    from IPython import get_ipython
    get_ipython().run_line_magic('matplotlib', 'inline')

# # for ipython
# %matplotlib qt

# importlib.reload(my_nn)

# In[]

AIM = 'Train CNN model (fastai library)'



# GET CONFIG FILE
# Pass a training config to this script.  e.g.
#       'config-train-all-devices.ini'
#       'config-train-arietta.ini'
#       'config-train-esaote.ini'
parser = argparse.ArgumentParser(description=AIM)
parser.add_argument('--config', required=True, help='Specify training config. e.g. config-arietta.ini')
args            = parser.parse_args()

CONFIG            = args.config
my_utils.CONFIG   = args.config
plot_utils.CONFIG = args.config


# Read ini
config = configparser.ConfigParser(inline_comment_prefixes=";")
config.read(CONFIG)

# Config
EXPORT_DATA           = config['MODEL']['EXPORT_DATA'] # Nov, June etc
BASE_DIR              = config[EXPORT_DATA]['BASE_DIR']
MODEL_ARCH            = config['MODEL']['MODEL_ARCH']
MODEL_EXPORT_NAME     = config.get('MODEL','MODEL_EXPORT_NAME').replace('.', '_')
RUN_COMMENT           = config['MODEL']['COMMENT']
TRAIN_PIPELINE        = config['MODEL']['TRAIN_PIPELINE']
BATCH_SIZE            = int(config['MODEL']['BATCH_SIZE'])
TRAINING_EPOCHS       = int(config['MODEL']['TRAINING_EPOCHS'])
item_tfms_IMAGE_SIZE  = config['MODEL']['item_tfms_IMAGE_SIZE']
batch_tfms_IMAGE_SIZE = config['MODEL']['batch_tfms_IMAGE_SIZE']
item_tfms_IMAGE_SIZE  = tuple(map(int, item_tfms_IMAGE_SIZE.strip('()').split(',')))
batch_tfms_IMAGE_SIZE = tuple(map(int, batch_tfms_IMAGE_SIZE.strip('()').split(',')))
CREATE_PLOTS          = config.getboolean('MODEL', 'CREATE_PLOTS')
INTERPRET_MODEL       = config.getboolean('MODEL', 'INTERPRET_MODEL')
TRAIN_NEW_MODEL       = config.getboolean('MODEL', 'TRAIN_NEW_MODEL')
TRAIN_DATA            = os.path.join(BASE_DIR, config['MODEL']['TRAIN_DATA'])
TEST_DATA             = os.path.join(BASE_DIR, config['MODEL']['TEST_DATA'])
dynamic_plot_folder   = 'plot-' + os.path.basename(TEST_DATA)
MODEL_EXPORT_DIR      = Path(config['MODEL']['MODEL_EXPORT_DIR'])
PLOT_DIR              = Path(os.path.join(config['MODEL']['PLOT_DIR'], dynamic_plot_folder))
PLOT_MAIN_TITLE       = config.get('MODEL', 'PLOT_MAIN_TITLE')


plot_utils.MASK_FOLDER = os.path.join(BASE_DIR, TRAIN_PIPELINE, '20-Renamed-Images', 'MASKS')



if TRAIN_NEW_MODEL:
    highlight = '■■■ TRAINING NEW MODEL ■■■'
else:
    # using saved model
    highlight = ''


# if not my_nn.jupyter_running():
    # my_utils.print_banner(AIM, os.path.basename(__file__))

print('')
print('PyTorch Version:      ', metadata.version("torch"))
print('FastAI Version:       ', metadata.version("fastai"))
print('OpenCV Version:       ', metadata.version("opencv-python"))
print('PIL Version:          ', metadata.version("pillow") )
print('Timm Version:         ', metadata.version("timm"))
print('')
print('TRAINING_EPOCHS       ', TRAINING_EPOCHS)
print('item_tfms_IMAGE_SIZE  ', item_tfms_IMAGE_SIZE)
print('batch_tfms_IMAGE_SIZE ', batch_tfms_IMAGE_SIZE)
print('BATCH_SIZE            ', BATCH_SIZE)
print('')
print('EXPORT_DATA           ', EXPORT_DATA)
print('TRAIN_DATA            ', TRAIN_DATA)
print('TEST_DATA             ', TEST_DATA)
print('')
print('CREATE_PLOTS          ', CREATE_PLOTS)
print('INTERPRET_MODEL       ', INTERPRET_MODEL)
print('')
print('PLOT_DIR              ', PLOT_DIR)
print('MODEL_EXPORT_DIR      ', MODEL_EXPORT_DIR)
print('MASK_FOLDER           ', plot_utils.MASK_FOLDER)
print('')
print('Config:               ', os.path.basename(CONFIG))
print('RUN_COMMENT           ', RUN_COMMENT)
print('MODEL_ARCH            ', MODEL_ARCH)
print('MODEL_EXPORT_NAME     ', MODEL_EXPORT_NAME)
print('PLOT_MAIN_TITLE       ', PLOT_MAIN_TITLE)
print('')
print('------------------')
print('TRAIN_NEW_MODEL?      ', TRAIN_NEW_MODEL, highlight)
print('------------------')
print('')


if not my_nn.jupyter_running():
    # Will we go ahead?
    response = input("Continue? (y/n): ").lower()
    if response == 'n':
        exit()


# In[]

# Verify folders
my_utils.assert_dir_exists(TRAIN_DATA)
my_utils.assert_dir_exists(TEST_DATA)
my_utils.make_dir_if_none(MODEL_EXPORT_DIR)
# my_utils.recreate_dir(PLOT_DIR)
my_utils.make_dir_if_none(PLOT_DIR)

# In[]

if TRAIN_NEW_MODEL:
    # Save settings used to create this model
    this_script = os.path.abspath(__file__)
    shutil.copy2(CONFIG, MODEL_EXPORT_DIR)
    shutil.copy2(this_script, MODEL_EXPORT_DIR)


# In[]


# METRIC FUNCTIONS
# NOTE: it's a multi-target model
#   learn.dls.vocab:
#     [
#     ['benign', 'malignant'],
#     ['HIGH', 'LOW', 'MODERATE']
#     ]
def combine_loss(inp, lesion, birads):
    return my_nn.lesion_loss(inp, lesion, birads) + my_nn.birads_loss(inp, lesion, birads)

err_metrics = (my_nn.lesion_err, my_nn.birads_err)
all_metrics = err_metrics + (my_nn.lesion_loss, my_nn.birads_loss)
# all_metrics = (my_nn.lesion_err, my_nn.birads_err) + (my_nn.lesion_loss, my_nn.birads_loss)


# Datablock functions
# 'WHICH_DEVICE' could be 'ALL', 'ESAOTE', 'ARIETTA' etc
# my_get_items = partial(my_nn.get_images, device_keyword=WHICH_DEVICE)
my_get_items = get_image_files


# In[]



if not TRAIN_NEW_MODEL:
    # Don't train!
    # Use pre-saved model...

    # Jan 3: This is a good one? No false negatives?
    # convnext_tiny_fb_in22k--pipeline-004-good-no-false-negatives.pkl

    model_path = os.path.join(MODEL_EXPORT_DIR, MODEL_EXPORT_NAME + '.pkl')

    if os.path.isfile(model_path):
        learn = load_learner(model_path)
        print(f"\nLoaded pre-saved model: {model_path}")
    else:
        print(f"\nError loading pre-saved model. Not found: {model_path}")
        sys.exit()

    # print(learn.model)

    print('\n')
    print('------------------------------------------------')
    print('------------------------------------------------')
    print('learn.dls.vocab')
    print(learn.dls.vocab)

    if len(learn.dls.vocab[0]) != 2:
        print('Something wrong with Lesion Type vocab, should be length 2.')
        print('Should be: Benign, Malignant')
        exit()

    if len(learn.dls.vocab[1]) != 3:
        print('Something wrong with Birads vocab, should be length 3')
        print('Should be: LOW, MODERATE, HIGH')
        exit()
    print('------------------------------------------------')
    print('------------------------------------------------')



    # Because we're using a pre-trained model,
    # we can't interpret the model
    # (No matter what it says in config.ini)
    INTERPRET_MODEL = False


# In[]


else:
# In[]

    # Train new model

    # IMAGE_RESIZE = (H, W)
    # IMAGE_RESIZE = (1000, 1000)
    # IMAGE_RESIZE = (1000, 100)
    # IMAGE_RESIZE = (100, 1000)
    # IMAGE_RESIZE = (400, 600)
    # IMAGE_RESIZE = (700, 800)


    # def aug_transforms(
    # mult         : float     = 1.0,                # Multiplication applying to `max_rotate`,`max_lighting`,`max_warp`
    # do_flip      : bool      = True,               # Random flipping
    # flip_vert    : bool      = False,              # Flip vertically
    # max_rotate   : float     = 10.,                # Maximum degree of rotation
    # min_zoom     : float     = 1.,                 # Minimum zoom
    # max_zoom     : float     = 1.1,                # Maximum zoom
    # max_lighting : float     = 0.2,                # Maximum scale of changing brightness
    # max_warp     : float     = 0.2,                # Maximum value of changing warp per
    # p_affine     : float     = 0.75,               # Probability of applying affine transformation
    # p_lighting   : float     = 0.75,               # Probability of changing brightnest and contrast
    # xtra_tfms    : list      = None,               # Custom Transformations
    # size         : int|tuple = None,               # Output size, duplicated if one value is specified
    # mode         : str       = 'bilinear',         # PyTorch `F.grid_sample` interpolation
    # pad_mode                 = PadMode.Reflection, # A `PadMode`
    # align_corners            = True,               # PyTorch `F.grid_sample` align_corners
    # batch                    = False,              # Apply identical transformation to entire batch
    # min_scale                = 1.                  # Minimum scale of the crop, in relation to image area



    # # Get stats to normalise all images
    # # See below: Normalize.from_stats(*my_dataset_stats),
    # stats_file = os.path.join(BASE_DIR, TRAIN_PIPELINE, 'dataset_stats.json')
    # with open(stats_file, 'r') as f:
            # stats_dict = json.load(f)
    # my_dataset_stats = (np.array(stats_dict['mean']), np.array(stats_dict['std']))
    # print(my_dataset_stats)


# In[]

    # Example using Pad
    resize_pad       = Resize(item_tfms_IMAGE_SIZE, method=ResizeMethod.Pad, pad_mode='zeros')

    # item_tfms=[Resize(size=img_size, resamples=(Image.Resampling.LANCZOS, 0))]
    item_tfms=[resize_pad]

    # filters=[
        # ImageFilter.EDGE_ENHANCE_MORE,
        # ImageFilter.EMBOSS,
        # ImageFilter.CONTOUR,
        # ImageFilter.FIND_EDGES,
    # ]

    # xtra_tfms = [Dihedral()]
    xtra_tfms = []

    batch_tfms = aug_transforms(do_flip       = True,
                                flip_vert     = False,
                                max_rotate    = 0.0,
                                max_zoom      = 1.0,
                                min_zoom      = 1.0,
                                p_affine      = 0.0,
                                p_lighting    = 0.0,
                                xtra_tfms     = xtra_tfms,
                                mode          = 'bilinear', pad_mode='zeros',
                                align_corners = True,
                                mult          = 1.0,
                                max_lighting  = 0.0,
                                max_warp      = 0.0,
                                size          = batch_tfms_IMAGE_SIZE,
                                batch         = True,
                                min_scale     = 1.
                                )




    # Define training data
    data_block=DataBlock(
        blocks     = (ImageBlock, CategoryBlock, CategoryBlock),
        n_inp      = 1,
        get_items  = my_get_items,
        splitter   = RandomSplitter(valid_pct=0.2, seed=42),
        get_y      = [my_nn.get_lesion_type, my_nn.get_birads_category],
        item_tfms  = item_tfms,
        batch_tfms = batch_tfms
        )


    # # Test your get_labels function with an example filename
    # # ------------------------------------------------------
    # # test_filename = Path(os.path.join(TRAIN_DATA, "benign_4--ID_bus1059l--249x255--Toshiba.png"))
    # # test_filename = Path(os.path.join(TRAIN_DATA, "malignant_4c--ID_5985--502x469--ESAOTE.png"))
    # print(test_filename)
    # if os.path.isfile(test_filename):
        # print('file exists')

    # test_lesion_type = my_nn.get_lesion_type(test_filename)
    # test_birads_category = my_nn.get_birads_category_xx(test_filename)

    # print("Lesion Type:", test_lesion_type)
    # print("BIRADS Category:", test_birads_category)
    # exit()



    # Create DataLoader from DataBlock
    dls = data_block.dataloaders(TRAIN_DATA, bs=BATCH_SIZE)


    # from pprint import pprint
    # pprint(dls.train.items[:10])

    # for item in dls.train.items:
        # print(os.path.basename(item))


# In[]


    if not my_nn.jupyter_running():
        my_utils.recreate_dir('./tmp')
        # Save batch to disk
        print('Saving one batch to disk...')
        batch = dls.one_batch()
        for i, (x, _) in enumerate(zip(batch[0], batch[1])):
            # Convert tensor to PIL image
            img = ToPILImage()(x).convert("RGB")
            # Save image
            img.save(os.path.join('./tmp', f"transformed_image_{i}.png"))
    else:
        # Visualize a batch of images
        dls.show_batch(nrows=4, ncols=4)


# In[]



    # Use pre-trained 'resnet' architecture
    if MODEL_ARCH == 'resnet34':
        # learn =vision_learner(dls, resnet34, metrics=accuracy).to_fp16()
        print('training resnet')
        learn = vision_learner(dls, resnet34, loss_func=combine_loss, metrics=all_metrics, n_out=5).to_fp16()

    elif MODEL_ARCH == 'resnet18':
        learn =vision_learner(dls, resnet18, metrics=accuracy).to_fp16()

    elif MODEL_ARCH == 'resnet50':
        learn =vision_learner(dls, resnet50, metrics=accuracy).to_fp16()

    else:
        # TODO:
        # learn = vision_learner(dls, MODEL_ARCH, metrics=error_rate).to_fp16() <--- ??? use error_rate
        # learn = vision_learner(dls, MODEL_ARCH, metrics=accuracy).to_fp16()

        learn = vision_learner(dls, MODEL_ARCH, loss_func=combine_loss, metrics=all_metrics, n_out=5).to_fp16()

    # else:
        # print ('Model not found: ', MODEL)
        # print("Exiting... "); import sys; sys.exit()

        # learn.dls.valid_ds.items[0]
        # learn.dls.valid_ds.items
        # print('Summary:')
        # print(learn.metrics)

        # dls         : DataLoaders,                                      # `DataLoaders` containing fastai or PyTorch `DataLoader`s
        # model       : callable,                                         # PyTorch model for training or inference
        # loss_func   : callable|None                 = None,             # Loss function. Defaults to `dls` loss
        # opt_func    : Optimizer|OptimWrapper        = Adam,             # Optimization function for training
        # lr          : float|slice                   = defaults.lr,      # Default learning rate
        # splitter    : callable                      = trainable_params, # Split model into parameter groups. Defaults to one parameter group
        # cbs         : Callback|MutableSequence|None = None,             # `Callback`s to add to `Learner`
        # metrics     : callable|MutableSequence|None = None,             # `Metric`s to calculate on validation set
        # path        : str|Path|None                 = None,             # Parent directory to save, load, and export models. Defaults to `dls` `path`
        # model_dir   : str|Path                      = 'models',         # Subdirectory to save and load models
        # wd          : float|int|None                = None,             # Default weight decay
        # wd_bn_bias  : bool                          = False,            # Apply weight decay to normalization and bias parameters
        # train_bn    : bool                          = True,             # Train frozen normalization layers
        # moms        : tuple                         = (0.95,0.85,0.95), # Default momentum for schedulers
        # default_cbs : bool                          = True              # Include default `Callback`s



    # learn.show_results()

    # print('Summary')
    # print(learn.summary())

# In[]

    learn.dls = dls

    print('\n')
    print('------------------------------------------------')
    print('------------------------------------------------')
    print('learn.dls.vocab')
    print(learn.dls.vocab)
    if len(learn.dls.vocab[0]) != 2:
        print('Something wrong with Lesion Type vocab, should be length 2.')
        print('Should be: Benign, Malignant')
        exit()

    if len(learn.dls.vocab[1]) != 3:
        print('Something wrong with Birads vocab, should be length 3')
        print('Should be: LOW, MODERATE, HIGH')
        exit()
    print('------------------------------------------------')
    print('------------------------------------------------')

    print(f"# training items:   {len(learn.dls.train.items)}")
    print(f"# validation items: {len(learn.dls.valid.items)}")

    # Display labels for validation set
    my_nn.show_vocab_labels(learn.dls.valid)

    lr_slide, lr_valley = learn.lr_find(show_plot=True, suggest_funcs=(slide, valley))
    print('\n')
    print('\n')
    print('lr_slide:    ', lr_slide)
    print('lr_valley:   ', lr_valley)

    # lr   = (lr_slide + lr_valley) / 2
    lr   = lr_slide
    # lr   = lr_valley
    # lr   = 0.001

    print('Picked lr: ', np.round(lr, 4))
    print('\n')

    # Which is better??
    # -----------------
    learn.fit_one_cycle(TRAINING_EPOCHS, lr)
    # learn.fine_tune(TRAINING_EPOCHS, lr)

    # EXPORT model
    model_path = os.path.join(MODEL_EXPORT_DIR, MODEL_EXPORT_NAME + '.pkl')
    learn.export(model_path)
    print("Exported model: ", model_path)


# In[]

# Show top loses

def do_interp(model):
    interp = ClassificationInterpretation.from_learner(model)

    losses, idxs = interp.top_losses()
    len(dls.valid_ds)==len(losses)==len(idxs)

    # Example usage
    if my_nn.jupyter_running():
        interp.show_results(idxs[:CREATE_PLOTS])
        interp.plot_top_losses(CREATE_PLOTS, nrows=int(CREATE_PLOTS / 3), figsize=(12,16))
        interp.plot_confusion_matrix(figsize=(7,7))
    else:
        print("Not in Jupyter: don't plot anything...")

    worst_idxs = idxs[:CREATE_PLOTS].numpy()
    worst_filenames = [dls.valid_ds.items[i] for i in worst_idxs]

    # Uncomment to print filenames
    print("\nWorst performing images:")
    print("\n".join(map(str, worst_filenames)))

    # for i in range(CREATE_PLOTS):
        # my_nn.make_prediction(learn, worst_filenames[i])


if INTERPRET_MODEL:
    do_interp(learn)


# In[]

# df_results = my_nn.predict_folder(learn, my_get_items, TEST_DATA)
# print(df_results.head())

# test_dl.show_batch(max_n=40)

# # handy
# test_dl.items
# learn.dls.vocab
# acc = accuracy(preds, targs)
# test_loss, test_accuracy = learn.validate(dl=test_dl)

# POINTING_GAME_CSV = os.path.join(MODEL_EXPORT_DIR, 'pointing_game.csv')
# title_extra  = '\n'
# title_extra += 'Model:    ' + MODEL_EXPORT_NAME + '\n'
# title_extra +=  RUN_COMMENT       + '\n'


# plot_path = os.path.join(PLOT_DIR, 'pointing-game.png')
# plot_utils.plot_pointing_game(POINTING_GAME_CSV, PLOT_MAIN_TITLE + title_extra, plot_path)


test_dl = learn.dls.test_dl(my_get_items(TEST_DATA), with_labels=True)
preds, targs  = learn.get_preds(dl=test_dl)
# preds, targs  = learn.tta(dl=test_dl)

lesions_vocab   = learn.dls.vocab[0]
birads_vocab    = learn.dls.vocab[1]

# Decode predictions
lesion_preds = preds[:,:2].argmax(dim=1)
birads_preds = preds[:,2:].argmax(dim=1)

# Decode targets
lesion_targs, birads_targs = targs

# Check if predictions are correct
lesion_outcome = (lesion_preds == lesion_targs).numpy()
birads_outcome = (birads_preds == birads_targs).numpy()

# Convert 'outcome' arrays to 'Good'/'Bad'
lesion_outcome = ['Good' if outcome else 'Bad' for outcome in lesion_outcome]
birads_outcome = ['Good' if outcome else 'Bad' for outcome in birads_outcome]

# Get filenames from the test dataloader
filepaths = [item for item in test_dl.items]


# Get probabilities
lesion_probs = preds[:,:2].softmax(dim=1).numpy()
birads_probs = preds[:,2:].softmax(dim=1).numpy()

df = pd.DataFrame({
    'lesion_outcome' : lesion_outcome,
    'birads_outcome' : birads_outcome,
    'lesion_pred'    : [lesions_vocab[p] for p in lesion_preds.numpy()],
    'lesion_true'    : [lesions_vocab[t] for t in lesion_targs.numpy()],
    # 'lesion_probs'   : list(lesion_probs),
    'birads_pred'    : [birads_vocab[p] for p in birads_preds.numpy()],
    'birads_true'    : [birads_vocab[t] for t in birads_targs.numpy()],
    # 'birads_probs'   : list(birads_probs),
    'filepath'       : filepaths
})


# Example for lesion_probs (assuming 2 classes - benign and malignant)
lesion_prob_columns = [f'lesion_prob_{cls}' for cls in lesions_vocab]
lesion_probs_df = pd.DataFrame(lesion_probs, columns=lesion_prob_columns)

# Example for birads_probs (assuming 3 classes - low, moderate, high)
birads_prob_columns = [f'birads_prob_{cls}' for cls in birads_vocab]
birads_probs_df = pd.DataFrame(birads_probs, columns=birads_prob_columns)

df = pd.concat([df, lesion_probs_df, birads_probs_df], axis=1)


# Reverse sort the DataFrame
df = df.sort_values(by=['lesion_outcome', 'birads_outcome'], ascending=[True, True])

# Convert the sorted DataFrame to a string with aligned columns
df_string = df.to_string(index=False)

# Write the string to a text file
RESULTS_FILE      = os.path.join(MODEL_EXPORT_DIR, 'results-' + MODEL_EXPORT_NAME + '.txt')
CSV_FILE          = os.path.join(MODEL_EXPORT_DIR, 'results-' + MODEL_EXPORT_NAME + '.csv')
ACCURACY_CSV      = os.path.join('/mnt/dysk_roboczy/efarell/repos/output', 'accuracy.csv')
POINTING_GAME_CSV = os.path.join(PLOT_DIR, 'pointing_game.csv')

plot_utils.POINTING_GAME_CSV = POINTING_GAME_CSV


def calculate_class_accuracy(preds, targs, vocab):
    class_accuracies = {}
    for i, cls in enumerate(vocab):
        cls_preds = preds == i
        cls_targs = targs == i
        correct_preds = cls_preds & cls_targs
        total_cls = cls_targs.sum()
        accuracy = (correct_preds.sum() / total_cls).item() if total_cls > 0 else 0
        class_accuracies[cls] = round(accuracy, 3)
    return class_accuracies

# class accuracies
lesion_class_acc = calculate_class_accuracy(lesion_preds, lesion_targs, lesions_vocab)
birads_class_acc = calculate_class_accuracy(birads_preds, birads_targs, birads_vocab)

# Overall accuracies
lesion_overall_acc = round(lesion_outcome.count('Good') / len(lesion_outcome), 3)
birads_overall_acc = round(birads_outcome.count('Good') / len(birads_outcome), 3)

# Preparing accuracy string to write in file
accuracy_string  = f"Lesion Overall Accuracy: {lesion_overall_acc:.2f}\n"
accuracy_string += f"BIRADS Overall Accuracy: {birads_overall_acc:.2f}\n"
accuracy_string += "\nLesion Class Accuracies:\n"
for cls, acc in lesion_class_acc.items():
    accuracy_string += f"  {cls:<10}: {acc:.2f}\n"
accuracy_string += "\nBIRADS Class Accuracies:\n"
for cls, acc in birads_class_acc.items():
    accuracy_string += f"  {cls:<10}: {acc:.2f}\n"

# Write the accuracy information to the RESULTS_FILE
with open(RESULTS_FILE, 'w') as file:
    file.write(accuracy_string + "\n" + df_string)

print('\nResults:')
print(accuracy_string)


df.to_csv(CSV_FILE, index=False)

print(f'Created: {RESULTS_FILE}')
print(f'Created: {CSV_FILE}')

# Current date and time
current_datetime = datetime.now().strftime("%Y-%b-%d %H:%M:%S")
pointing_accuracy = 0


# In[]

# ROC AUC
plot_utils.plot_auc(lesion_probs, lesion_targs, lesions_vocab, PLOT_MAIN_TITLE, "Lesion_Model", PLOT_DIR)
plot_utils.plot_auc(birads_probs, birads_targs, birads_vocab,  PLOT_MAIN_TITLE, "BIRADS_Model", PLOT_DIR)

title_extra  = '\n'
title_extra += 'Model:    ' + MODEL_EXPORT_NAME + '\n'
# title_extra +=  RUN_COMMENT       + '\n'


# -----------------------------------------------------------------------------------------------------------------------------------------------------------------
#               LESION TYPE
# -----------------------------------------------------------------------------------------------------------------------------------------------------------------
# Mapping true labels to binary for Precision-Recall Curve
lesion_true_binary = df['lesion_true'].map({'benign': 0, 'malignant': 1})

# Create figure for lesion type predictions
fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(16, 20))


fig.suptitle(PLOT_MAIN_TITLE + title_extra, fontsize=20, y=1.00)

# Plot each chart on separate subplot
plot_utils.plot_confusion_matrix(df['lesion_true'], df['lesion_pred'], ['benign', 'malignant'],     'Lesion Confusion', ax=axes[0, 0])
plot_utils.plots_probs_v_cases(df, ax=axes[1, 1])


plot_utils.plot_precision_recall_curve(lesion_true_binary, df['lesion_prob_malignant'], 'Lesion Precision-Recall', ax=axes[0 , 1])

# plot_utils.plot_probability_distribution(df, ['lesion_prob_malignant'],   'Lesion Probability Distribution', ax=axes[1, 0], colors=['rosybrown'])
plot_utils.plot_lesion_probs_with_errors(df, 'Lesion Predictions (+ False Negatives)', ax=axes[1, 0])

# Save figure
plot_path = os.path.join(PLOT_DIR, 'metrics-lesion-type.png')
plt.tight_layout()
# plt.subplots_adjust(top=0.95)
plt.savefig(plot_path, bbox_inches='tight')
print('Created: ', plot_path)



# -----------------------------------------------------------------------------------------------------------------------------------------------------------------
#               BIRADS
# -----------------------------------------------------------------------------------------------------------------------------------------------------------------
# Mapping 'birads_true' to binary for each class
birads_high_binary     = df['birads_true'].apply(lambda x: 1 if x == 'HIGH'     else 0)
birads_moderate_binary = df['birads_true'].apply(lambda x: 1 if x == 'MODERATE' else 0)
birads_low_binary      = df['birads_true'].apply(lambda x: 1 if x == 'LOW'      else 0)


fig    = plt.figure(figsize=(18, 22))
gs     = gridspec.GridSpec(3, 3, figure=fig)
add_ax = fig.add_subplot

fig.suptitle(PLOT_MAIN_TITLE + title_extra, fontsize=20, y=1.00)

plot_utils.plot_confusion_matrix(df['birads_true'], df['birads_pred'], ['LOW', 'MODERATE', 'HIGH'], 'BIRADS Confusion', ax=add_ax(gs[0, :2]))

plot_utils.plot_probability_distribution(df, ['birads_prob_LOW'],      'BIRADS LOW (Predictions)',      ax=add_ax(gs[1, 0]), colors=['Green'])
plot_utils.plot_probability_distribution(df, ['birads_prob_MODERATE'], 'BIRADS MODERATE (Predictions)', ax=add_ax(gs[1, 1]), colors=['Orange'])
plot_utils.plot_probability_distribution(df, ['birads_prob_HIGH'],     'BIRADS HIGH (Predictions)',     ax=add_ax(gs[1, 2]), colors=['Red'])

plot_utils.plot_precision_recall_curve(birads_low_binary      , df['birads_prob_LOW']       , 'BIRADS LOW'      , ax=add_ax(gs[2 , 0]))
plot_utils.plot_precision_recall_curve(birads_moderate_binary , df['birads_prob_MODERATE']  , 'BIRADS MODERATE' , ax=add_ax(gs[2 , 1]))
plot_utils.plot_precision_recall_curve(birads_high_binary     , df['birads_prob_HIGH']      , 'BIRADS HIGH'     , ax=add_ax(gs[2 , 2]))



# Save figure
plot_path = os.path.join(PLOT_DIR, 'metrics-birads.png')
plt.tight_layout()
plt.subplots_adjust(top=0.90)
plt.savefig(plot_path, bbox_inches='tight')
print('Created: ', plot_path)


# In[]


# split into good and bad predictions
df_wrong   = df[(df['lesion_outcome'] == 'Bad')  | (df['birads_outcome'] == 'Bad')].reset_index(drop=True)
df_correct = df[~df.index.isin(df_wrong.index)].reset_index(drop=True)

# In[]



if CREATE_PLOTS:
    # re-create plot folders
    my_utils.recreate_dir(os.path.join(PLOT_DIR, 'good'))
    my_utils.recreate_dir(os.path.join(PLOT_DIR, 'bad'))

    # Create 'pointing game' log file
    with open(POINTING_GAME_CSV, 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerow(["Image", "Mask", "max_val_coords", "pointing_game"])

    # plot failures
    # for idx, row in df_wrong.iterrows():
    for idx, row in df.iterrows():

        probabilities = [
                        [row['lesion_prob_benign'], row['lesion_prob_malignant']],
                        [row['birads_prob_HIGH'], row['birads_prob_LOW'], row['birads_prob_MODERATE']],
                        ]


        plot_utils.plot_prediction_multi(PLOT_MAIN_TITLE,
                                        row['filepath'],
                                        learn,
                                        row['lesion_outcome'], row['birads_outcome'],
                                        row['lesion_pred'],    row['birads_pred'],
                                        row['lesion_true'],    row['birads_true'],
                                        probabilities,
                                        PLOT_DIR)



# In[]

if os.path.isfile(POINTING_GAME_CSV):
    plot_path = os.path.join(PLOT_DIR, 'pointing-game.png')
    pointing_accuracy = plot_utils.plot_pointing_game(POINTING_GAME_CSV, PLOT_MAIN_TITLE + title_extra, plot_path)


# Write out summary of everything
# Handy to compare models

# Preparing data for CSV
data_for_csv = {
    "date": [current_datetime],
    "model": [MODEL_EXPORT_NAME],
    "lesion_overall_accuracy": [f"{lesion_overall_acc:.2f}"],
    "birads_overall_accuracy": [f"{birads_overall_acc:.2f}"],
    "pointing_game": [pointing_accuracy]
}

# Adding class accuracies to the CSV data
for cls, acc in lesion_class_acc.items():
    data_for_csv[f"lesion_accuracy_{cls}"] = [f"{acc:.2f}"]

for cls, acc in birads_class_acc.items():
    data_for_csv[f"birads_accuracy_{cls}"] = [f"{acc:.2f}"]


# Creating df and writing to CSV
file_exists = os.path.isfile(ACCURACY_CSV)
accuracy_df = pd.DataFrame(data_for_csv)
accuracy_df.to_csv(ACCURACY_CSV, mode='a', index=False, header=not file_exists)
print('Updated file:', ACCURACY_CSV)
print(data_for_csv)

# In[]

