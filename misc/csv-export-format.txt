# CSV file format 1:

- PatientID:                Patient ID (Numeric).
- TumorID:                  Tumour ID (Numeric).
- ExaminationID:            Examination ID (Numeric).
- ImageID:                  Unique. One image per row in this CSV.
- Type:                     4 Possible values: "Benign", "Malignant", "Normal", "NULL".
- BIRADS:                   Number indicating BIRADS assessment category.
- Image_filename:           Filename generated from Image ID (numerical digits).
- Tumor_mask_filename:      Filenames of any tumour masks. May be multiple files, separated by ";".
- Other_mask_filename:      Filenames of any other masks of suspicious areas in the image. May be multiple files, separated by ";".
- PixelX:                   Pixel width:  cm per pixel e.g. 0.1 cm / pixel.
- PixelY:                   Pixel height: cm per pixel e.g. 0.2 cm / pixel.
- LocX0:                    BOUNDING BOX: Top-Left corner `x`.
- LocY0:                    BOUNDING BOX: Top-Left corner `y`.
- LocX1:                    BOUNDING BOX: Bottom-right corner `x`.
- LocY1:                    BOUNDING BOX: Bottom-right corner `y`.
- Device_name:              e.g. Samsung RS85 etc.
- ImageWidth:               Image dimension (added by me)
- ImageHeight:              Image dimension (added by me)


# CSV file format 2:

- PatientID:                Integer;
- TumorID:                  Integer;
- ExaminationID:            Integer;
- ImageID:                  Text; # Text: e.g. ImageID = 4974a
- Type:                     Text;
- BIRADS:                   Text; # e.g. 0, 1, 2, 3, 4, 4a, 4b, 4c, 5
- Image_filename:           Text;
- Tumor_mask_filename:      Text;
- Other_mask_filename:      Text;
- PixelX:                   Real;
- PixelY:                   Real;
- LocX0:                    Integer;
- LocY0:                    Integer;
- LocX1:                    Integer;
- LocY1:                    Integer;
- Device_name:              Text;
- ImageWidth:               Integer;
- ImageHeight:              Integer;


# Notes:

- each row corresponds to one image;

- In the `type` column:
    - `NULL` means a change is under observation or the biopsy
      result is not yet available (this will be updated in
      subsequent exports)

- the data directory is an exported set from the main database.
  If there are any doubts regarding the data contained therein,
  we would ask you to send information about the appropriate ID
  to apawlow@ippt.pan.pl and dziennik@ippt.pan.pl
    - Making changes to export files is pointless, because the
      original will not be updated.
