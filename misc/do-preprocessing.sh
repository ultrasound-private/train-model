#!/bin/bash

if [ -z "$1" ]; then
    ALL_STEPS="false"
else
    ALL_STEPS="true"
fi

# Nov
BASE="/mnt/dysk_roboczy/efarell/repos/US-Data/2023-11-12"

# # June
# BASE="/mnt/dysk_roboczy/efarell/repos/US-Data/2023-06-21"

printf "\nDeleting old output...\n"

if [ $ALL_STEPS = "true" ]; then
    command rm -rf "$BASE/data-15"
    command rm -rf "$BASE/data-20"
fi

command rm -rf "$BASE/data-30"
command rm -rf "$BASE/data-40"
command rm -rf "$BASE/data-50"

ls -l "$BASE"

# ------------------------------------
# Python scripts to pre-process images
# ------------------------------------

if [ $ALL_STEPS = "true" ]; then
    # Takes a while: Cropping / resizing 
    python  ./10-crop-images.py
    # python  ./15-pixel-width-resize.py
fi

python  ./20-rename-organise.py
python  ./30-test-train-split.py
python  ./40-two-target-classes.py


printf "\nOutput folders:\n"
ls -l "$BASE"
