
from pathlib import Path
import re
import os


FILENAME_FORMAT = "{type}_{birads}--ID_{image_id}--{width}x{height}--{device}{ext}"
# TRAIN_PATH =  '/mnt/dysk_roboczy/efarell/repos/US-Data/2023-11-12/pipeline-005/data-30/BENIGN'
TRAIN_PATH =  '/mnt/dysk_roboczy/efarell/repos/US-Data/2023-11-12/pipeline-005/data-30/MALIGNANT'

def generate_regex_pattern(filename_format, keyword):
    # Adjust for the file extension
    filename_format = filename_format.replace("{ext}", ".{ext}")
    pattern = filename_format
    placeholders = ['type', 'birads', 'image_id', 'width', 'height', 'device', 'ext']
    for placeholder in placeholders:
        if placeholder == keyword:
            pattern = pattern.replace(f'{{{placeholder}}}', f'(?P<{placeholder}>[^--]*)')
        else:
            pattern = pattern.replace(f'{{{placeholder}}}', '[^--]*')
    pattern = pattern.replace('.', '\.')  # Escape the period for the file extension
    return pattern


def my_custom_get_images(path, device_keyword=None):
    regex = re.compile(generate_regex_pattern(FILENAME_FORMAT, 'device'))
    
    matching_images = []
    for img_path in Path(path).rglob('*'):
        match = regex.match(img_path.name)
        if match:
            # If device_keyword is None or the empty string, add all devices
            if device_keyword in [None, '']:
                matching_images.append(img_path)
            # Otherwise, only add images that match the device keyword
            elif match.group('device') == device_keyword:
                matching_images.append(img_path)
    
    return matching_images


# def my_custom_get_images(path, device_keyword):
    # regex = re.compile(generate_regex_pattern(FILENAME_FORMAT, 'device'))
    # matching_images = []

    # for img_path in Path(path).rglob('*'):
        # match = regex.match(img_path.name)
        # if match and match.group('device') == device_keyword:
            # matching_images.append(img_path)

    # return matching_images

def my_custom_get_labels(file_path, label_keyword):
    regex = re.compile(generate_regex_pattern(FILENAME_FORMAT, label_keyword))
    match = regex.match(file_path.name)
    return match.group(label_keyword) if match else None

# data_block = DataBlock(
    # # ...
    # get_items = partial(my_custom_get_images, device_keyword='ARIETTA'),
    # get_y     = partial(my_custom_get_labels, label_keyword='birads'),
    # # ...
# )


# file_list = my_custom_get_images(TRAIN_PATH, device_keyword='ARIETTA')
file_list = my_custom_get_images(TRAIN_PATH)
print(*file_list, sep = "\n")

for file_path in file_list:
    print('\n', os.path.basename(file_path))
    # label = my_custom_get_labels(file_path, 'birads')
    label = my_custom_get_labels(file_path, 'type')
    print(label)

# print('len file_list:', len(file_list))
