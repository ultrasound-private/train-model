import os
import shutil
import subprocess
import configparser
import argparse
import utils


AIM = 'Create pipeline of images, to be used for CNN training'

def run_script(script_path, args_dict):
    # create list of args from dictionary
    command_line_args = [f"--{key}={value}" for key, value in args_dict.items()]
    try:
        # execute script
        subprocess.run(["python", script_path] + command_line_args, check=True)
        print(f"SUCCESS: {os.path.basename(script_path)}.")
    except subprocess.CalledProcessError as e:
        print(f"ERROR: {e}")
        exit()

# argument parser
parser = argparse.ArgumentParser(description=AIM)
parser.add_argument('--config',  required=True, help='Which config to use')
parser.add_argument('--filter',  required=True, help='Filter string for filenames', default='all', type=str.lower)
args = parser.parse_args()


# Read ini
config = configparser.ConfigParser(inline_comment_prefixes=";")
config.read(args.config)


EXPORT_DATA         = config['PIPELINE']['EXPORT_DATA'] # Nov, June etc
BASE_DIR            = config[EXPORT_DATA]['BASE_DIR']
PIPELINE_DIR        = os.path.join(BASE_DIR, config['PIPELINE']['NAME'])
DO_IMAGE_PROCESSING = config.getboolean('PIPELINE', 'DO_IMAGE_PROCESSING')


utils.print_banner(AIM, os.path.basename(__file__))
print('')
print('Config              ', args.config)
print('PIPELINE_DIR        ', PIPELINE_DIR)
print('DO_IMAGE_PROCESSING ', DO_IMAGE_PROCESSING)
print('FILTER              ', args.filter)
print('')

# User confirms...
response = input("Continue? (y/n): ").lower()
if response == 'n':
        exit()


args1 = {'config':args.config,  'in_dir':'ORIGINAL-DATA',                        'out_dir':'10-Processed-Images'                     }
args2 = {'config':args.config,  'in_dir':'10-Processed-Images',                  'out_dir':'20-Renamed-Images'                       }
args2 = {'config':args.config,  'in_dir':'20-Renamed-Images/VALID-FOR-TRAINING', 'out_dir':'Training-Pool-',    'filter':args.filter }

run_script('./10-image-pre-processing.py', args1)
run_script('./20-rename-organise.py',      args2)
run_script('./30-test-train-split.py',     args3)



print("\n\nOUTPUT FOLDERS:\n")
for entry in sorted(os.listdir(PIPELINE_DIR)):
    print(entry)
