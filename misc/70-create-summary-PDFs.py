import os, sys
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from PIL import Image
import configparser
import utils
from CONSTANTS import CONFIG_PATH


# Read ini
config = configparser.ConfigParser(inline_comment_prefixes=";")
config.read(CONFIG_PATH)

AIM = 'Create summary PDFs from a folder of Images'

# define paths
EXPORT_DATA   = config['PIPELINE']['EXPORT_DATA']
BASE_DIR      = config[EXPORT_DATA]['BASE_DIR']
BASE_DIR_PDFS = config['MODEL']['BASE_DIR_PDFS']
OUTPUT_DIR    = os.path.join(BASE_DIR_PDFS)

# Verify paths
utils.assert_dir_exists(OUTPUT_DIR)


def get_subfolders(directory):
    """Return a list of subdirectories for the given directory."""
    return [os.path.join(directory, name) for name in os.listdir(directory) 
            if os.path.isdir(os.path.join(directory, name))]


def extract_fname_size(filename):
    # Split filename to get device and image size
    # Sample filename: plot-benign_4a--ID_5466--502x469--ESAOTE.png 
    parts          = filename.split('--')
    size           = parts[2] # e.g. '504x468'
    device_and_ext = parts[3]                      # e.g. ARIETTA.png
    device_name    = device_and_ext.split('.')[0]  # e.g. ARIETTA
    width, height  = map(int, size.split('x'))
    return device_name, (width, height)


def images_to_pdf(image_folder, output_pdf):
    image_files = [f for f in os.listdir(image_folder) if f.lower().endswith(('.jpg', '.png', '.jpeg', '.bmp'))]

    if len(image_files) == 0:
        print('No images found')
    else:
        # sort images by device name, then image size
        # image_files_sorted = sorted(image_files, key=lambda f: extract_fname_size(f))
        image_files_sorted = image_files

        print(f"\n{output_pdf}: Adding {len(image_files_sorted)} images")

        with PdfPages(output_pdf) as pdf:
            # process 12 images at a time
            for i in range(0, len(image_files_sorted), 12):  

                # Page layout
                # 3 x 3
                fig, axes = plt.subplots(3, 3, figsize=(12, 8))
                # 4 x 2
                # fig, axes = plt.subplots(4, 2, figsize=(8, 12))

                for ax, image_file in zip(axes.ravel(), image_files_sorted[i:i+12]):
                    img = Image.open(os.path.join(image_folder, image_file))
                    ax.imshow(img)
                    ax.axis('off')

                    # set plot title to filename (minus extension)
                    plot_title, _ = os.path.splitext(image_file)
                    ax.set_title(plot_title, fontsize=10, pad=2)

                fig.tight_layout()
                pdf.savefig(fig)
                plt.close(fig)
        print('Done.')


def main():
    # command-line argument?
    if len(sys.argv) == 1:
        print('\nCannot continue: Please supply a folder path')
        exit()
    else:
        image_dirs = [sys.argv[1]] 

    utils.print_banner(AIM, os.path.basename(__file__))
    print('\nInput folder(s)  ')
    print('\n'.join(image_dirs))
    print('\nOutput folder ')
    print(OUTPUT_DIR)
    print('')

    # Generate pdf summary of each folder
    for image_dir in image_dirs:
        utils.assert_dir_exists(image_dir)

        # First, Process the main folder

        # Create pdf file name
        # e.g. ./pdfs/test_BENIGN.pdf
        end_folders = os.path.join(*image_dir.split('/')[-2:])
        pdf_name    = end_folders.replace('/', '-') + '.pdf'
        pdf_path    = os.path.join(OUTPUT_DIR, pdf_name)

        print("Processing main folder:", end_folders)
        print("PDF name:", pdf_name)
        print("PDF path:", pdf_path)

        # Convert
        images_to_pdf(image_dir, pdf_path)

        # Process any subfolders
        for subfolder in get_subfolders(image_dir):
            end_folders_sub = os.path.join(*subfolder.split('/')[-2:])
            pdf_name_sub = end_folders_sub.replace('/', '-') + '.pdf'
            pdf_path_sub = os.path.join(OUTPUT_DIR, pdf_name_sub)
            
            print("Processing subfolder:", end_folders_sub)
            print("PDF name:", pdf_name_sub)
            print("PDF path:", pdf_path_sub)

            # Convert subfolder images to PDF
            images_to_pdf(subfolder, pdf_path_sub)


# Generate pdf summary for each folder
if __name__ == "__main__":
    main()
