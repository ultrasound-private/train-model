#!/bin/bash

# root_folder="/g/MISC/ILR_MASTER_SQLITE"
root_folder="/g/Resources/AD-HOC/05-JAN-2020/01-TLA-Survey-Analysis/DATA/ILR-R04" # <--- temp change for tla survey

db_name="$root_folder/ILR_MASTER.sqlite"
csv_separator=","

# make a backup first
if test -f "$db_name"; then
    echo BAcking up existing DB
    cp --verbose "$db_name" "$root_folder/backup.sqlite"
fi

# import ILR csv files into SQLITE
sqlite3 -batch ${db_name} <<EOF

.echo on
.separator "${csv_separator}"

begin transaction;
.import DATA/CSV/Rulebase_FM25_Learner.csv    Rulebase_FM25_Learner
.import DATA/CSV/Valid_Learner.csv            Valid_Learner
.import DATA/CSV/Valid_LearningDelivery.csv   Valid_LearningDelivery
.import DATA/CSV/Valid_ProviderSpec.csv       Valid_ProviderSpecDeliveryMonitoring
commit;

-- remove any extra CSV headers
delete from  Rulebase_FM25_Learner                 where ILR_YEAR = 'ILR_YEAR';
delete from  valid_learner                         where ILR_YEAR = 'ILR_YEAR';
delete from  Valid_LearningDelivery                where ILR_YEAR = 'ILR_YEAR';
delete from  Valid_ProviderSpecDeliveryMonitoring  where ILR_YEAR = 'ILR_YEAR';

EOF


# verify import worked:
# show most recent ILR Month from each table
sqlite3 -batch ${db_name} <<EOF

.echo off
.mode column
select * from (select 'Most recent  Rulebase_FM25_Learner'    as "desc", ilr_year, ilr_month, count(*) from Rulebase_FM25_Learner                group by ilr_year, ilr_month order by ILR_YEAR desc, ilr_month desc limit 1)  union
select * from (select 'Most recent  valid_learner'            as "desc", ilr_year, ilr_month, count(*) from valid_learner                        group by ilr_year, ilr_month order by ILR_YEAR desc, ilr_month desc limit 1)  union
select * from (select 'Most recent  Valid_LearningDelivery'   as "desc", ilr_year, ilr_month, count(*) from Valid_LearningDelivery               group by ilr_year, ilr_month order by ILR_YEAR desc, ilr_month desc limit 1)  union
select * from (select 'Most recent  Valid_ProviderSpecDelMon' as "desc", ilr_year, ilr_month, count(*) from Valid_ProviderSpecDeliveryMonitoring group by ilr_year, ilr_month order by ILR_YEAR desc, ilr_month desc limit 1) ;

EOF
