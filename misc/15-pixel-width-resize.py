import csv
import os, sys
import shutil
from PIL import Image
import numpy as np
import cv2
from tqdm import tqdm
import utils
import configparser
from constants import CONFIG_PATH

AIM = 'Resize all images to a new Pixel width'

# resize image based on new pixel widths
NEW_PIXEL_WIDTH_X = 0.0084
NEW_PIXEL_WIDTH_Y = 0.0084


# read ini
config = configparser.ConfigParser()
config.read(CONFIG_PATH)

# get paths
EXPORT_DATA  = config['PIPELINE']['EXPORT_DATA']
BASE_DIR     = config[EXPORT_DATA]['BASE_DIR']
PIPELINE_DIR = os.path.join(BASE_DIR, config['PIPELINE']['NAME'])
META_DIR     = config[EXPORT_DATA]['META_DIR']
INPUT_DIR    = os.path.join(PIPELINE_DIR, "data-15")
OUTPUT_DIR   = os.path.join(PIPELINE_DIR, "data-20")
METADATA_CSV = os.path.join(META_DIR, config[EXPORT_DATA]['CSV_CROP'])
CSV_RESIZE   = os.path.join(META_DIR, config[EXPORT_DATA]['CSV_RESIZE'])

utils.print_banner(AIM, os.path.basename(__file__))   
print('EXPORT_DATA      ',  EXPORT_DATA)
print('INPUT_DIR        ',  INPUT_DIR)
print('OUTPUT_DIR       ',  OUTPUT_DIR)
print('METADATA_CSV     ',  METADATA_CSV)
print('CSV_RESIZE       ',  CSV_RESIZE)
print('')


# Setup input/output folders
utils.assert_dir_exists(INPUT_DIR)
utils.assert_dir_not_empty(INPUT_DIR)
utils.folder_summary(INPUT_DIR)
utils.recreate_dir(OUTPUT_DIR)


def standardise_pixel_width(input_path, target_px_x, target_px_y, cur_px_x, cur_px_y):
    # Resize image to desired new pixel size
    #   target_px_x: target  pixel size x
    #   target_px_y: target  pixel size y
    #   cur_px_x:    current pixel size x
    #   cur_px_y:    current pixel size y
    with Image.open(input_path) as img:
        factor_x = float(cur_px_x) / target_px_x
        factor_y = float(cur_px_y) / target_px_y

        new_w = int(img.width * factor_x)
        new_h = int(img.height * factor_y)

        # note: 'Lanczos' is an anti-alias algorithm
        resized_img = img.resize((new_w, new_h), Image.Resampling.LANCZOS)
        return np.array(resized_img)


# We'll create a new CSV with extra columns(s)
updated_rows = []
total_rows = utils.count_lines(METADATA_CSV) - 1 # subtract 1 for header

# Process each row from CSV
with open(METADATA_CSV, 'r', newline='') as csv_file:
    print("\nProcessing...")
    csv_reader = csv.DictReader(csv_file)

    for row in tqdm(csv_reader, total=total_rows):
        # Resize image based on desired pixel widths
        image_path = os.path.join(INPUT_DIR, row['Image_filename'])
        resized_img = standardise_pixel_width(image_path,
                                              NEW_PIXEL_WIDTH_X,
                                              NEW_PIXEL_WIDTH_Y,
                                              row['PixelX'], row['PixelY'])

        # Store image size before cropping
        row['resize_height'] = resized_img.shape[0]
        row['resize_width']  = resized_img.shape[1]

        # update METADATA csv
        updated_rows.append(row)

        # move processed image to output folder
        output_path = os.path.join(OUTPUT_DIR, row['Image_filename'])
        cv2.imwrite(output_path, resized_img)



print("Done.")
utils.folder_summary(OUTPUT_DIR)


# Write updated rows to a new CSV file
new_fieldnames = csv_reader.fieldnames + ['resize_width', 'resize_height']

with open(CSV_RESIZE, 'w', newline='') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames=new_fieldnames)
    writer.writeheader()
    for row in updated_rows:
        writer.writerow(row)

print(f"New metadata CSV: {CSV_RESIZE}")
