# In[]
from fastai.vision.all import *

# In[]

import torch.nn.functional as F
import timm
from torchvision.transforms import ToPILImage

import importlib.metadata as metadata
import utils_nn as my_nn_utils
import utils    as my_utils

import os, sys
import importlib
from PIL import Image
import matplotlib.pyplot as plt
import configparser
from CONSTANTS import CONFIG_PATH
import pandas as pd

# handy for showing pandas dataframes
from IPython.display import display



if my_nn_utils.jupyter_running():
    from IPython import get_ipython
    get_ipython().run_line_magic('matplotlib', 'inline')

# # for ipython
# %matplotlib qt

# importlib.reload(my_nn_utils)

# In[]

AIM = 'Train CNN model (fastai library)'

# Read ini
config = configparser.ConfigParser(inline_comment_prefixes=";")
config.read(CONFIG_PATH)

# Config
EXPORT_DATA = config['MODEL']['EXPORT_DATA'] # Nov, June etc
BASE_DIR    = config[EXPORT_DATA]['BASE_DIR']

MODEL_ARCH        = config['MODEL']['MODEL_ARCH']
MODEL_EXPORT_NAME = config.get('MODEL','MODEL_EXPORT_NAME').replace('.', '_')
RUN_COMMENT       = config['MODEL']['COMMENT']
TRAIN_PIPELINE  =  config['MODEL']['TRAIN_PIPELINE']

BATCH_SIZE        = int(config['MODEL']['BATCH_SIZE'])
TRAINING_EPOCHS   = int(config['MODEL']['TRAINING_EPOCHS'])
PLOT_FAILURES = int(config['MODEL']['PLOT_FAILURES'])

IMAGE_RESIZE      = config['MODEL']['IMAGE_RESIZE']
IMAGE_RESIZE      = tuple(map(int, IMAGE_RESIZE.strip('()').split(',')))

PLOT_CAMS         = config.getboolean('MODEL', 'PLOT_CAMS')
INTERPRET_MODEL   = config.getboolean('MODEL', 'INTERPRET_MODEL')
TRAIN_NEW_MODEL   = config.getboolean('MODEL', 'TRAIN_NEW_MODEL')

TRAIN_DATA        = os.path.join(BASE_DIR, config['MODEL']['TRAIN_DATA'])
TEST_DATA         = os.path.join(BASE_DIR, config['MODEL']['TEST_DATA'])

BASE_DIR_PLOTS    = config['MODEL']['BASE_DIR_PLOTS']
BASE_DIR_MODELS   = config['MODEL']['BASE_DIR_MODELS']
PLOT_DIR          = Path(os.path.join(BASE_DIR_PLOTS,  MODEL_EXPORT_NAME))
MODEL_EXPORT_DIR  = Path(BASE_DIR_MODELS)

# Get key software versions
torch_version = metadata.version("torch")
fastai_version = metadata.version("fastai")
opencv_version = metadata.version("opencv-python")
pil_version = metadata.version("pillow")


if TRAIN_NEW_MODEL:
    highlight = '■■■ TRAINING NEW MODEL ■■■'
else:
    # using saved model
    highlight = ''


# if not my_nn_utils.jupyter_running():
    # my_utils.print_banner(AIM, os.path.basename(__file__))

print('')
print('PyTorch Version:  ', torch_version)
print('FastAI Version:   ', fastai_version)
print('OpenCV Version:   ', opencv_version)
print('PIL Version:      ', pil_version)
print('')
print('MODEL_ARCH        ', MODEL_ARCH)
print('MODEL_EXPORT_NAME ', MODEL_EXPORT_NAME)
print('------------------')
print('TRAIN_NEW_MODEL   ', TRAIN_NEW_MODEL, highlight)
print('------------------')
print('RUN_COMMENT       ', RUN_COMMENT)
print('TRAINING_EPOCHS   ', TRAINING_EPOCHS)
print('IMAGE_RESIZE      ', IMAGE_RESIZE)
print('BATCH_SIZE        ', BATCH_SIZE)
print('PLOT_FAILURES ', PLOT_FAILURES)
print('PLOT_CAMS         ', PLOT_CAMS)
print('INTERPRET_MODEL   ', INTERPRET_MODEL)
print('')
print('PLOT_DIR          ', PLOT_DIR)
print('MODEL_EXPORT_DIR  ', MODEL_EXPORT_DIR)
print('')
print('EXPORT_DATA       ', EXPORT_DATA)
print('TRAIN_DATA        ', TRAIN_DATA)
print('TEST_DATA         ', TEST_DATA)
print('')


if not my_nn_utils.jupyter_running():
    # Will we go ahead?
    response = input("Continue? (y/n): ").lower()
    if response == 'n':
        exit()


# In[]



# Verify folders
my_utils.assert_dir_exists(TRAIN_DATA)
my_utils.assert_dir_exists(TEST_DATA)
my_utils.assert_dir_exists(MODEL_EXPORT_DIR)
my_utils.recreate_dir(PLOT_DIR)


# In[]


# Don't train!
# Use pre-saved model...

# Jan 3: This is a good one? No false negatives?
# convnext_tiny_fb_in22k--pipeline-004-good-no-false-negatives.pkl


MODEL_EXPORT_NAME = 'convnext_tiny_fb_in22k--pipeline-004-good-no-false-negatives'

model_path = os.path.join(MODEL_EXPORT_DIR, MODEL_EXPORT_NAME + '.pkl')
if os.path.isfile(model_path):
    learn = load_learner(model_path)
    print(f"\nLoaded pre-saved model: {model_path}")
else:
    print(f"\nError loading pre-saved model. Not found: {model_path}")
    sys.exit()

learn.default_cfg()

# Because we're using a pre-trained model,
# we can't interpret the model or plot Class activation maps.
# (No matter what it says in config.ini)
PLOT_CAMS = False
INTERPRET_MODEL = False


# In[]


# Get stats to normalise all images
# See below: Normalize.from_stats(*my_dataset_stats), 
stats_file = os.path.join(BASE_DIR, TRAIN_PIPELINE, 'dataset_stats.json')
with open(stats_file, 'r') as f:
        stats_dict = json.load(f)
my_dataset_stats = (np.array(stats_dict['mean']), np.array(stats_dict['std']))
print(my_dataset_stats)


# In[]

# Example using Pad
resize_pad       = Resize(IMAGE_RESIZE, method=ResizeMethod.Pad, pad_mode='zeros')  

# Define training data
data_block=DataBlock(
    blocks     = (ImageBlock, CategoryBlock),                       
    get_items  = get_image_files,                                   
    splitter   = RandomSplitter(valid_pct=0.2, seed=42),            
    get_y      = parent_label,                                      
    item_tfms  = resize_pad,
    batch_tfms = aug_transforms(size=224, min_scale=0.75)
            # Normalize.from_stats(*my_dataset_stats)]
)

# Create DataLoader from DataBlock
dls = data_block.dataloaders(TRAIN_DATA, bs=BATCH_SIZE)

# dls.train.items



# In[]


# Visualize a batch of images
dls.show_batch(nrows=4, ncols=4)


# In[]

learn.dls = dls

# lr_min = learn.lr_find(show_plot=False)
# lr_min = learn.lr_find(show_plot=True)

lr_slide, lr_valley = learn.lr_find(show_plot=True, suggest_funcs=(slide, valley))

lr_slide
lr_valley

# hard-code
# lr_min = 0.01
# lr_min = 0.008
# print(f"\nlr: {lr_min}")

# In[]


# Train model using 'one-cycle' policy
# TODO: this should be 'fine_tune'???
# TODO: because I'm using a pre-trained model???
# learn.fit_one_cycle(TRAINING_EPOCHS, lr_min) # <--- no because using pre-trained model???

TRAINING_EPOCHS = 6
learn.fine_tune(TRAINING_EPOCHS, lr_slide)


# In[]

# # Continue training for more epochs
# extra_epochs = 2
# lr_min = learn.lr_find(show_plot=False)
# print(f"\nMinimum/10: {lr_min[0]:.2e}")
# learn.fit_one_cycle(extra_epochs, lr_min)

# TODO: try tta: test time agumentation?


# TODO: use a train func
# def train(arch, item, batch, epochs=5):
    # dls = ImageDataLoaders.from_folder(trn_path, seed=42, valid_pct=0.2, item_tfms=item, batch_tfms=batch)
    # learn = vision_learner(dls, arch, metrics=error_rate).to_fp16()
    # learn.fine_tune(epochs, 0.01)
    # return learn

    # learn = train(arch, item=Resize((256,192), method=ResizeMethod.Pad, pad_mode=PadMode.Zeros),
                # batch=aug_transforms(size=(171,128), min_scale=0.75))



# In[]


# Export model for future use
MODEL_EXPORT_NAME = 'convnext_tiny_fb_in22k--pipeline-004-good-no-false-negatives-extra-training'
model_path = os.path.join(MODEL_EXPORT_DIR, MODEL_EXPORT_NAME + '.pkl')
learn.export(model_path)
print("Exported model: ", model_path)



# In[]

# Show top loses

PLOT_FAILURES = 30
interp = ClassificationInterpretation.from_learner(learn)

losses, idxs = interp.top_losses()

len(dls.valid_ds)==len(losses)==len(idxs)

interp.show_results(idxs[:PLOT_FAILURES])
interp.plot_top_losses(PLOT_FAILURES, nrows=int(PLOT_FAILURES / 3), figsize=(12,16))
interp.plot_confusion_matrix(figsize=(7,7))

worst_idxs = idxs[:PLOT_FAILURES].numpy()
worst_filenames = [dls.valid_ds.items[i].name for i in worst_idxs]

# worst
# benign_4c--ID_4630--760x608--Philips.jpg',
# 'benign_4a--ID_8007--793x412--Philips.png',
# 'benign_5--ID_5987--502x469--ESAOTE.png',
# 'benign_5--ID_6693--737x597--SAM_RS85.jpg',
# 'malignant_4c--ID_2567b--484x609--Philips.png',
# 'benign_4c--ID_4696--760x571--Philips.png',
# 'malignant_4b--ID_5896--502x469--ESAOTE.png',
# 'benign_4b--ID_6987--432x468--ESAOTE.png',
# 'malignant_5--ID_9171_f83--595x475--Philips.png',
# 'benign_4a--ID_7210--432x468--ESAOTE.png',
# 'benign_5--ID_6007--430x469--ESAOTE.png',
# 'benign_4c--ID_4672--908x725--Philips.jpg',
# 'malignant_4c--ID_5885--502x469--ESAOTE.png',
# 'malignant_4b--ID_7539--432x468--ESAOTE.png',
# 'benign_4a--ID_6829--432x468--ESAOTE.png',
# 'benign_4b--ID_7049--432x468--ESAOTE.png',
# 'malignant_4b--ID_7540--432x468--ESAOTE.png',
# 'benign_4a--ID_9163--795x583--Philips.png',
# 'malignant_5--ID_4805--475x240--SAM_RS85.JPG',
# 'malignant_5--ID_4804--475x240--SAM_RS85.JPG',
# 'malignant_5--ID_9141--796x504--Philips.jpg',
# 'benign_4b--ID_6226--502x469--ESAOTE.png',
# 'malignant_5--ID_9282_f4--477x483--Philips.png',
# 'benign_5--ID_2611--929x578--SAM_RS85.png',
# 'benign_4a--ID_6228--502x469--ESAOTE.png',
# 'malignant_4b--ID_7350--605x472--ESAOTE.png',
# 'benign_4b--ID_7473--378x470--ESAOTE.png',
# 'benign_4a--ID_7802--432x468--ESAOTE.png',
# 'benign_3--ID_6747--475x346--SAM_RS85.jpg',
# 'malignant_4b--ID_5893--502x469--ESAOTE.png'


# In[]

# Run a test set:
# ---------------

test_dl = learn.dls.test_dl(get_image_files(TEST_DATA), with_labels=True)
test_dl.show_batch(max_n=100)

# # handy
# test_dl.items
# learn.dls.vocab



# In[]

preds, targs, preds_decoded = learn.get_preds(dl=test_dl, with_decoded=True)

# acc = accuracy(preds, targs)
test_loss, test_accuracy = learn.validate(dl=test_dl)

print('test_accuracy: ' + str(np.round(test_accuracy, 3)))
print('test_loss:     ' + str(np.round(test_loss, 3)))


# In[]

# Extract file names
# file_paths       = [str(path) for path in test_dl.items]
file_paths       = [str(path.name) for path in test_dl.items]
results          = ["Correct" if pred == targ else "Wrong" for pred, targ in zip(preds_decoded, targs)]
predicted_labels = list(learn.dls.vocab[preds_decoded])
actual_labels    = list(learn.dls.vocab[targs])
prob_benign      = np.round(preds[:, 0].numpy(), 3)
prob_malignant   = np.round(preds[:, 1].numpy(), 3)

# Create DataFrame
df = pd.DataFrame({
    "result"         : results,
    "predicted"      : predicted_labels,
    "actual"         : actual_labels,
    "prob_Benign"    : prob_benign,
    "prob_Malignant" : prob_malignant,
    "files"          : file_paths
})

# show 'wrong' ones at the top
df_sorted = df.sort_values(by=['result', 'actual'], ascending=False)
df_sorted = df_sorted.reset_index(drop=True)

display(df_sorted.style)

# In[]

# Convert DataFrame to a nicely formatted string
formatted_string = df_sorted.to_string(justify='right', index=True)

RESULTS_FILE = './sorted_df_good_model_all_devices_extra_training.txt'

# Save to a text file
with open(RESULTS_FILE, 'w') as file:
    file.write('test_accuracy: ' + str(np.round(test_accuracy, 3)))
    file.write('\n')
    file.write('test_loss:     ' + str(np.round(test_loss, 3)))
    file.write('\n')
    file.write('\n')
    file.write(formatted_string)


# In[]


good_files, bad_files = my_nn_utils.predict_folder(learn, get_image_files, TEST_DATA)

my_utils.recreate_dir(os.path.join(PLOT_DIR, 'good'))
my_utils.recreate_dir(os.path.join(PLOT_DIR, 'bad'))

# for f in good_files:
    # my_nn_utils.make_prediction(learn, f, show_image=False, do_cams=PLOT_CAMS)

for f in bad_files:
    my_nn_utils.make_prediction(learn, f, show_image=False, do_cams=PLOT_CAMS)

# In[]
