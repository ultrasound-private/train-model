import os
from pathlib import Path
from rembg import remove, new_session

session = new_session()

input_dir = "./remove-bg-test"
output_dir = "/mnt/dysk_roboczy/efarell/repos/bg-test-output"

# Ensure output directory exists
if not os.path.exists(output_dir):
    os.makedirs(output_dir)


for filename in os.listdir(input_dir):
    # input_path = str(filename)
    input_path = os.path.join(input_dir, filename)
    output_path = os.path.join(output_dir, f"no_bg_{filename}")

    print (input_path)
    print (output_path)

    with open(input_path, 'rb') as i:
        with open(output_path, 'wb') as o:
            input = i.read()
            output = remove(input, session=session)
            o.write(output)
            print(output_path)
