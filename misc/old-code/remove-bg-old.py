import os
from rembg import remove
from PIL import Image
from io import BytesIO

def remove_background_from_dir(input_dir: str, output_dir: str):
    """
    Removes the background from all images in the given directory and saves
    the new images to the specified output directory.
    
    Parameters:
    - input_dir (str): Directory containing the input images.
    - output_dir (str): Directory to save the processed images.
    """
    
    # Ensure output directory exists
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
        
    # List all files in the input directory
    for filename in os.listdir(input_dir):
        if filename.lower().endswith(('.png', '.jpg', '.jpeg')):
            input_filepath = os.path.join(input_dir, filename)
            output_filepath = os.path.join(output_dir, f"no_bg_{filename}")
            
            # Read image and remove background
            with open(input_filepath, "rb") as img_file:
                output = remove(img_file.read())
                output_image = Image.open(BytesIO(output))
                output_image.save(output_filepath)
                
    print(f"Processed {len(os.listdir(input_dir))} images. Results saved in {output_dir}.")

# Usage:
input_dir = "./remove-bg-test"
output_dir = "/mnt/dysk_roboczy/efarell/repos/bg-test-output"

remove_background_from_dir(input_dir, output_dir)

