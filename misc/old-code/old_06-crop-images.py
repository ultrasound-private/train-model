import cv2
import os

def remove_border(input_folder, output_folder):
    for image_name in os.listdir(input_folder):
        image_path = os.path.join(input_folder, image_name)
        img = cv2.imread(image_path)
        
        print("Processing: ", image_name)

        # Convert the image to grayscale
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # Threshold the image
        _, thresh = cv2.threshold(gray, THRESHOLD_VALUE, 255, cv2.THRESH_BINARY_INV)

        # Find the contours
        contours, _ = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        
        # If no contours are found, continue with the next image
        if not contours:
            print('No contours found.')
            continue

        # Find the largest contour by area
        largest_contour = max(contours, key=cv2.contourArea)
        # print(type(largest_contour))
        print(largest_contour)

        # Get the bounding box of the largest contour
        x, y, w, h = cv2.boundingRect(largest_contour)
        print('x, y, w, h: ', x, y, w, h)

        # Crop the image to the bounding box
        cropped = img[y:y+h, x:x+w]

        # Save the cropped image
        output_path = os.path.join(output_folder, image_name)
        cv2.imwrite(output_path, cropped)

# Usage example:
input_dir = "./remove-bg-test"
output_dir = "/mnt/dysk_roboczy/efarell/repos/bg-test-output2"

THRESHOLD_VALUE = 240

# Ensure output directory exists
if not os.path.exists(output_dir):
    os.makedirs(output_dir)


remove_border(input_dir, output_dir)

