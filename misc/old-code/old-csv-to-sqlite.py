import csv
import sqlite3

def csv_to_sqlite(csv_filename, db_filename, table_name):
    # Connect to the SQLite database (or create it if it doesn't exist)
    conn = sqlite3.connect(db_filename)
    cursor = conn.cursor()

    # Read the CSV file
    with open(csv_filename, 'r') as csv_file:
        csv_reader = csv.reader(csv_file)
        
        # Extract the column names from the first row of the CSV
        column_names = next(csv_reader)
        columns = ', '.join(column_names)
        placeholders = ', '.join(['?'] * len(column_names))

        # Create a table using the column names
        cursor.execute(f"CREATE TABLE {table_name} ({', '.join(['{} TEXT'.format(col) for col in column_names])})")

        # Insert rows from the CSV into the SQLite table
        for row in csv_reader:
            cursor.execute(f"INSERT INTO {table_name} ({columns}) VALUES ({placeholders})", row)

    # Commit changes and close the connection
    conn.commit()
    conn.close()

# Example usage
csv_to_sqlite('your_input.csv', 'output_database.db', 'your_table_name')

