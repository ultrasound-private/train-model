# In[]

import pandas as pd
import duckdb
import os


# In[]

input_csv = './data/pierwsi_export_2023-06-21_21-04-08_birads.csv'
output_db = './data/pierwsi_export_2023-06-21_21-04-08_birads.db'


# In[]

# Remove pre-existing database
try:
    os.remove(output_db)
except FileNotFoundError:
    pass


# In[]

# Read the CSV into a pandas DataFrame
df = pd.read_csv(input_csv)


# In[]

# Ensure data types
df['PatientID']           = df['PatientID'].astype(int)
df['TumorID']             = df['TumorID'].astype(int)
df['ExaminationID']       = df['ExaminationID'].astype(int)
df['ImageID']             = df['ImageID'].astype(str)  # String: e.g. ImageID = 4974a
df['Type']                = df['Type'].astype(str)
df['BIRADS']              = df['BIRADS'].astype(str)  # e.g. 0, 1, 2, 3, 4, 4a, 4b, 4c, 5
df['Image_filename']      = df['Image_filename'].astype(str)
df['Tumor_mask_filename'] = df['Tumor_mask_filename'].astype(str)
df['Other_mask_filename'] = df['Other_mask_filename'].astype(str)
df['PixelX']              = df['PixelX'].astype(float)
df['PixelY']              = df['PixelY'].astype(float)
df['LocX0']               = df['LocX0'].astype(int)
df['LocY0']               = df['LocY0'].astype(int)
df['LocX1']               = df['LocX1'].astype(int)
df['LocY1']               = df['LocY1'].astype(int)
df['Device_name']         = df['Device_name'].astype(str)


# In[]

# Connect to DuckDB database
con = duckdb.connect(output_db)

# Connect to DuckDB database
con = duckdb.connect(output_db)

# Insert into duckdb (new table: 'exported_images')
con.execute("CREATE TABLE exported_images AS SELECT * FROM df")
con.execute("INSERT INTO exported_images SELECT * FROM df")

# In[]


# Check insert worked...
result = con.execute('SELECT count(*) FROM exported_images').fetchall()
print ("Rows inserted: ", result)

result = con.execute('SELECT * FROM exported_images limit 10').fetchall()
print ("Sample")
print(*result, sep='\n')

# Close connection (optional)
con.close()

# In[]
