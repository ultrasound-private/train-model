# AIM:
#   Reduce number of Target Prediction classes. 
#   Move all "NORMAL" images into "BENIGN" folder.
#
# Reason:
#   Only 2 classification classes now: "BENIGN" and "MALIGNANT".
#   New Folder structure:

#   ▶ "OUTPUT_DIR"
#       ▶ training
#           ▶ BENIGN
#           ▶ MALIGNANT
#       ▶ test
#           ▶ BENIGN
#           ▶ MALIGNANT


import os, sys
import shutil
from math import ceil
from random import shuffle
import configparser
import utils

from CONSTANTS import CONFIG_PIPELINE_PATH
from CONSTANTS import DEVICE_MAP

AIM = 'Reduce target classes. Combine NORMAL and BENIGN images.'

config = configparser.ConfigParser(inline_comment_prefixes=";") 
config.read(CONFIG_PIPELINE_PATH) 

# Define paths/files
EXPORT_DATA  = config['PIPELINE']['EXPORT_DATA']
BASE_DIR     = config[EXPORT_DATA]['BASE_DIR']
PIPELINE_DIR = os.path.join(BASE_DIR, config['PIPELINE']['NAME'])
INPUT_DIR    = os.path.join(PIPELINE_DIR, "data-40")
OUTPUT_DIR   = os.path.join(PIPELINE_DIR, "data-50")

utils.print_banner(AIM, os.path.basename(__file__))  
print('EXPORT_DATA      ',  EXPORT_DATA)
print('INPUT_DIR        ',  INPUT_DIR)
print('OUTPUT_DIR       ',  OUTPUT_DIR)
print('')


utils.assert_dir_exists(INPUT_DIR)
utils.assert_dir_not_empty(INPUT_DIR)
utils.folder_summary(INPUT_DIR)

# Remove existing output (if exists)
if os.path.isdir(OUTPUT_DIR): shutil.rmtree(OUTPUT_DIR)

# First, copy everything from "input folder" -> "output folder"
shutil.copytree(INPUT_DIR, OUTPUT_DIR)
print(f"\nCopied everything to: {OUTPUT_DIR}")


# Move files from one folder to another
def move_files(src_dir, dest_dir):
    for f in os.listdir(src_dir):
        src_path  = os.path.join(src_dir, f)
        dest_path = os.path.join(dest_dir, f)

        # Check it's a file (don't move subdirectories)
        if os.path.isfile(src_path):
            shutil.move(src_path, dest_path)


# Move "Normal" images into "BENIGN"
move_files(OUTPUT_DIR + '/training/NORMAL', OUTPUT_DIR + '/training/BENIGN')
move_files(OUTPUT_DIR + '/test/NORMAL',     OUTPUT_DIR + '/test/BENIGN')

# Cleanup: Remove (empty) "Normal" folders
shutil.rmtree(OUTPUT_DIR + '/training/NORMAL')
shutil.rmtree(OUTPUT_DIR + '/test/NORMAL')

print("\nReorg Done!")
utils.folder_summary(OUTPUT_DIR)
