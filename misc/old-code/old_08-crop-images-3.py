

# LocX0 = 112
# LocY0 = 10
# LocX1 = 720
# LocY1 = 628


# In[]

# to display images in jupyter notebook

import matplotlib.pyplot as plt

def show_image(img):
    # Convert BGR to RGB
    img_rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    # Display image using matplotlib
    plt.imshow(img_rgb)
    plt.axis('off')  
    plt.show()


# In[]

import sys
import os
import cv2
import numpy as np


# Define locations
in_folder  = "./test-remove-gb"
out_folder = "/mnt/dysk_roboczy/efarell/repos/bg-test-output4"
test_image = "4720-malignant-4c-762-773-803-Philips-4720.jpg"

# Ensure output folder exists
if not os.path.exists(out_folder):
    os.makedirs(out_folder)


# In[]


orig = cv2.imread(os.path.join(in_folder, test_image))
img  = orig.copy()
H, W = img.shape[:2]

show_image(orig)

# In[]

# rows
LocY0 = 10
LocY1 = 628

# cols
LocX0 = 112
LocX1 = 720


# cropped = img[start_row:end_row, start_col:end_col]
# cropped_img = orig[y:y+h, x:x+w]

cropped_img = orig[LocY0:LocY1, LocX0:LocX1]

show_image(cropped_img)


# In[]


# Convert image to grayscale
img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# remove noise
img = cv2.GaussianBlur(img, (21, 21), 21)
img = cv2.erode(img, np.ones((5, 5)))

show_image(img)


# In[]


# remove horizantal line
# img = cv2.GaussianBlur(img, (5, 0), 21)

blr = img.copy()

# make binary image
img = cv2.threshold(img, 5, 255, cv2.THRESH_BINARY)[1]

# draw black border around image to better detect blobs:
cv2.rectangle(img, (0, 0), (W, H), 0, thickness=W//25)

show_image(img)


# In[]

bw = img.copy()

# Invert black and white colors
img = ~img

show_image(img)

# In[]

# Find contours and sort them by width
cnts, _ = cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

cnts = list(cnts)
cnts.sort(key=lambda x: cv2.boundingRect(x)[2], reverse=True)

# Change the type and channels of image copies
img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
bw  = cv2.cvtColor(bw,  cv2.COLOR_GRAY2BGR)
blr = cv2.cvtColor(blr, cv2.COLOR_GRAY2BGR)

# Find the *SECOND* biggest blob
x, y, w, h = cv2.boundingRect(cnts[1])
cv2.rectangle(orig, (x, y), (x + w, y + h), (128, 0, 255), 10)
cv2.rectangle(img,  (x, y), (x + w, y + h), (128, 255, 0), 10)
print(x, y, w, h)

# In[]

print(type(cnts))
print(len(cnts))
print(cnts)
print(cnts[1])

# In[]


# Save final result
top = np.hstack((blr, bw))
btm = np.hstack((img, orig))

stacked = np.vstack((top, btm))

# cv2.imwrite(in_folder + '/img_.png', stacked)
show_image(stacked)



# In[]

# Bounding-Box area:

# 133 25 736 635

# Cut and save the final image:

orig = cv2.imread(os.path.join(in_folder, test_image))

out_path    = os.path.join(out_folder, "crop_" + test_image)

cropped_img = orig[y:y+h, x:x+w]
# cropped_img = orig[x:x+w, y:y+h]
show_image(cropped_img)

cv2.imwrite(out_path, cropped_img)


# In[]

