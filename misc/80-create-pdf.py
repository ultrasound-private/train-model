import matplotlib.pyplot as plt
import os
from matplotlib.backends.backend_pdf import PdfPages
from PIL import Image

# Folder containing images
# folder_path = './do-image-processing/plots'
folder_path = '/mnt/dysk_roboczy/efarell/repos/plots-roc'
# folder_path = '/home/efarell/stripped-lines/MALIGNANT/'
# folder_path = '/home/efarell/stripped-lines/BENIGN/'
# folder_path = '/home/efarell/stripped-lines/NULL/'

# Image formats to consider
image_formats = ('.jpg', '.jpeg', '.png')

# PDF output file
pdf_filename = 'output_matplotlib.pdf'
# pdf_filename = 'stripped-lines-malignant.pdf'
# pdf_filename = 'stripped-lines-benign.pdf'

def get_images(folder):
    """ Retrieve image paths from the folder """
    return [os.path.join(folder, f) for f in os.listdir(folder) if f.endswith(image_formats)]

def plot_images(image_paths):
    """ Plot a set of images on grid """
    fig, axs = plt.subplots(2, 1, figsize=(8, 11)) # Adjust figsize if needed
    # fig, axs = plt.subplots(4, 2, figsize=(8, 11)) # Adjust figsize if needed
    axs = axs.flatten()

    for ax, img_path in zip(axs, image_paths):
        img = Image.open(img_path)
        ax.imshow(img)
        ax.axis('off')  # Hide axes

    # Hide any unused subplots
    for i in range(len(image_paths), len(axs)):
        axs[i].axis('off')

    plt.tight_layout()
    return fig

# Initialize PDF
with PdfPages(pdf_filename) as pdf:
    images = get_images(folder_path)
    for i in range(0, len(images), 2):
        print('.', end='', flush=True)
        fig = plot_images(images[i:i+2])
        pdf.savefig(fig)
        plt.close(fig)

print("PDF created successfully.")
