#!/bin/bash


# Extra re-run, just to add re-formatted PDF front pages (plots)

python 100-summarise-training.py  '//mnt/dysk_roboczy/efarell/repos/output/convnext_tiny.fb_in22k--pipeline-004--all-devices-no-masks'
python 100-summarise-training.py  '//mnt/dysk_roboczy/efarell/repos/output/convnext_tiny.fb_in22k--pipeline-004--arietta-baseline'
python 100-summarise-training.py  '//mnt/dysk_roboczy/efarell/repos/output/convnext_tiny.fb_in22k--pipeline-004--Esaote-no-masks'

python 100-summarise-training.py  '//mnt/dysk_roboczy/efarell/repos/output/convnext_tiny.fb_in22k--pipeline-005--all-devices-bigger'
python 100-summarise-training.py  '//mnt/dysk_roboczy/efarell/repos/output/convnext_tiny.fb_in22k--pipeline-005--arietta-bigger'
python 100-summarise-training.py  '//mnt/dysk_roboczy/efarell/repos/output/convnext_tiny.fb_in22k--pipeline-005--esaote-masks'




# python 100-summarise-training.py  '/mnt/dysk_roboczy/efarell/repos/output/SAVE-GOOD/convnext_tiny.fb_in22k--pipeline-004--All-devices-no-masks'
# python 100-summarise-training.py  '/mnt/dysk_roboczy/efarell/repos/output/SAVE-GOOD/convnext_tiny.fb_in22k--pipeline-004--Esaote-no-masks'
# python 100-summarise-training.py  '/mnt/dysk_roboczy/efarell/repos/output/SAVE-GOOD/convnext_tiny.fb_in22k--pipeline-004--Philips-no-masks'
# python 100-summarise-training.py  '/mnt/dysk_roboczy/efarell/repos/output/SAVE-GOOD/convnext_tiny.fb_in22k--pipeline-004--arietta-baseline'

# python 100-summarise-training.py  '/mnt/dysk_roboczy/efarell/repos/output/SAVE-GOOD/convnext_tiny.fb_in22k--pipeline-005--all-devices-bigger'
# python 100-summarise-training.py  '/mnt/dysk_roboczy/efarell/repos/output/SAVE-GOOD/convnext_tiny.fb_in22k--pipeline-005--arietta-bigger'
# python 100-summarise-training.py  '/mnt/dysk_roboczy/efarell/repos/output/SAVE-GOOD/convnext_tiny.fb_in22k--pipeline-005--esaote-masks'
# python 100-summarise-training.py  '/mnt/dysk_roboczy/efarell/repos/output/SAVE-GOOD/convnext_tiny.fb_in22k--pipeline-005--Philips-masks'

# python 100-summarise-training.py  '/mnt/dysk_roboczy/efarell/repos/output/SAVE-GOOD/resnet34--pipeline-004--all-devices-no-masks'
# python 100-summarise-training.py  '/mnt/dysk_roboczy/efarell/repos/output/SAVE-GOOD/resnet34--pipeline-005--all-devices-masks'
