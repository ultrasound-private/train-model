import os
import glob
import sys
from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas
from reportlab.lib import colors

# Check if an argument has been provided
if len(sys.argv) < 2:
    print("Usage: python script.py [ROOT_FOLDER]")
    sys.exit(1)

# Set the root folder path from command-line argument
ROOT_FOLDER = sys.argv[1]
PLOT_FOLDER = os.path.join(ROOT_FOLDER, 'plot-test')


def create_pdf(ROOT_FOLDER, PLOT_FOLDER):

    # Create a new PDF file
    Model = os.path.basename(ROOT_FOLDER.rstrip('/'))
    # pdf_path = os.path.join('./report', Model + '.pdf')

    # Because of rerun
    pdf_path = os.path.join('./report', Model + '_reprint.pdf')

    print('Processing: ', pdf_path)

    c = canvas.Canvas(pdf_path, pagesize=letter)

    # Page 1
    c.setFont("Helvetica-Bold", 16)
    c.drawString(72, 750, Model.replace('-', ' '))

    # Adding text from the results file
    results_file = glob.glob(os.path.join(ROOT_FOLDER, 'results-*.txt'))[0]
    with open(results_file, 'r') as file:
        lines = file.readlines()
        c.setFont("Helvetica", 12)
        for i, line in enumerate(lines[:12]):
            c.drawString(72, 700 - 15 * i, line.strip())

    # Adding pointing-game.png
    # c.drawImage(os.path.join(PLOT_FOLDER, 'pointing-game.png'), 72, 350, width=468, height=324, preserveAspectRatio=True)
    c.drawImage(os.path.join(PLOT_FOLDER, 'pointing-game.png'), 72, 150, width=468, height=324, preserveAspectRatio=True)
    c.showPage()

    # Page 2
    c.drawImage(os.path.join(PLOT_FOLDER, 'ROC-Lesion_Model.png'), 72, 450, width=468, height=324, preserveAspectRatio=True)
    c.drawImage(os.path.join(PLOT_FOLDER, 'ROC-BIRADS_Model.png'), 72, 72, width=468, height=324, preserveAspectRatio=True)
    c.showPage()

    # Page 3
    c.drawImage(os.path.join(PLOT_FOLDER, 'metrics-lesion-type.png'), 72, 72, width=468, height=684, preserveAspectRatio=True)
    c.showPage()

    # Because of rerun
    # # Adding plots from 'bad' and 'good' folders
    # for folder in ['bad', 'good']:
        # # Adding heading page for Bad/Good Predictions
        # c.setFont("Helvetica-Bold", 26)
        # c.drawCentredString(297, 400, folder.upper() + ' Predictions')
        # c.showPage()
        # for image in glob.glob(os.path.join(PLOT_FOLDER, folder, '*.png')):
            # c.drawImage(image, 72, 72, width=468, height=684, preserveAspectRatio=True)
            # c.showPage()

    # Save the PDF
    c.save()

if __name__ == "__main__":
    create_pdf(ROOT_FOLDER, PLOT_FOLDER)
