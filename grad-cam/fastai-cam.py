from fastai.vision.all import *
import torch.nn.functional as F


import argparse
import os
import cv2
import numpy as np
import torch
from torchvision import models

from pytorch_grad_cam import (
    GradCAM, HiResCAM, ScoreCAM, GradCAMPlusPlus,
    AblationCAM, XGradCAM, EigenCAM, EigenGradCAM,
    LayerCAM, FullGrad, GradCAMElementWise
)

from pytorch_grad_cam import GuidedBackpropReLUModel
from pytorch_grad_cam.utils.image import (
    show_cam_on_image, deprocess_image, preprocess_image
)
from pytorch_grad_cam.utils.model_targets import ClassifierOutputTarget
from pytorch_grad_cam.utils.find_layers import find_layer_types_recursive


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--use-cuda', action='store_true', default=True,
                        help='Use NVIDIA GPU acceleration')

    parser.add_argument(
        '--image-path',
        type=str,
        default='./examples/both.png',
        help='Input image path')

    parser.add_argument('--aug-smooth', action='store_true',
                        help='Apply test time augmentation to smooth the CAM')

    parser.add_argument(
        '--eigen-smooth',
        action='store_true',
        help='Reduce noise by taking the first principle component'
        'of cam_weights*activations')

    parser.add_argument('--method', type=str, default='gradcam',
                        choices=[
                            'gradcam', 'hirescam', 'gradcam++',
                            'scorecam', 'xgradcam', 'ablationcam',
                            'eigencam', 'eigengradcam', 'layercam',
                            'fullgrad', 'gradcamelementwise'
                        ],
                        help='CAM method')

    parser.add_argument('--output-dir', type=str, default='./',
                        help='Output directory to save the images')

    args = parser.parse_args()
    args.use_cuda = args.use_cuda and torch.cuda.is_available()
    if args.use_cuda:
        print('Using GPU for acceleration')
    else:
        print('Using CPU for computation')

    return args


if __name__ == '__main__':
    """ python cam.py -image-path <path_to_image>
    Example usage of loading an image and computing:
        1. CAM
        2. Guided Back Propagation
        3. Combining both
    """

    args = get_args()


    methods = {
        # "gradcam"            : GradCAM,
        # "hirescam"           : HiResCAM,
        # "scorecam"           : ScoreCAM,
        # "gradcam++"          : GradCAMPlusPlus,
        # "ablationcam"        : AblationCAM,
        # "xgradcam"           : XGradCAM,
        # "eigencam"           : EigenCAM,
        # "eigengradcam"       : EigenGradCAM,
        "layercam"           : LayerCAM,
        # "fullgrad"           : FullGrad,
        # "gradcamelementwise" : GradCAMElementWise
    }


    args.use_cuda     = True

    # args.aug_smooth   = True
    # args.eigen_smooth = True
    args.aug_smooth   = False
    args.eigen_smooth = False


    # We must specify which target to generate the CAM for.
    # If targets is None, the highest scoring category (for every member in the batch) will be used.
    # You can target specific categories by
    # targets = [ClassifierOutputTarget(0), ClassifierOutputTarget(1), ClassifierOutputTarget(2)]
    # targets = [ClassifierOutputTarget(0)]
    # targets = [ClassifierOutputTarget(2)]
    targets = None


    # ConvNeXt_Base_Weights
    # ConvNeXt_Large_Weights
    # ConvNeXt_Small_Weights
    # ConvNeXt_Tiny_Weights

    # model = models.resnet50(pretrained=True)
    # model = models.resnet34(pretrained=True)

    MODEL_EXPORT_NAME = 'convnext_tiny_fb_in22k--pipeline-005--all'
    MODEL_EXPORT_DIR  = '/mnt/dysk_roboczy/efarell/repos/output/convnext_tiny.fb_in22k--pipeline-005--all'
    target_layer_name = "0.model.stages.3.blocks.2"
    # target_layer_name = "0.model.stages.3.blocks.2.conv_dw"

     # MODEL_EXPORT_NAME = 'resnet34--pipeline-005--all'
     # MODEL_EXPORT_DIR  = '/mnt/dysk_roboczy/efarell/repos/output/resnet34--pipeline-005--all/'
     # target_layer_name = "0.7.2"
     # target_layer_name = "0.7.2.conv2"

    model_path = os.path.join(MODEL_EXPORT_DIR, MODEL_EXPORT_NAME + '.pkl')
    learn      = load_learner(model_path)
    model      = learn.model

    # print(model)
    target_layers = [learn.get_submodule(target_layer_name)]
    print('Model:         ', MODEL_EXPORT_NAME)
    print('Target layers: ', target_layers)

    # print(dir(learn))
    # print(learn.normalize)

    # print(dir(model))


    # print(dir(model))
    # print(model)

    target_files_arietta = [
                'benign_2--ID_7450--622x525--ARIETTA.png', 
                'benign_3--ID_5121--484x536--ARIETTA.png', 
                'benign_4a--ID_8971--544x531--ARIETTA.png', 
                'malignant_4c--ID_6507--362x546--ARIETTA.png', 
                'benign_2--ID_8100--484x536--ARIETTA.png', 
                'benign_4a--ID_7369--544x531--ARIETTA.png', 
                'benign_4b--ID_8608--726x517--ARIETTA.png', 
                'malignant_5--ID_8624--544x531--ARIETTA.png', 
                'benign_2--ID_8793--544x531--ARIETTA.png', 
                'benign_4a--ID_8947--484x536--ARIETTA.png', 
                'benign_4--ID_6901--544x531--ARIETTA.png', 
                ]




    target_files_all = [
                    'benign_2--ID_6404b--555x597--SAM_RS85.jpg',
                    'benign_2--ID_6632--794x582--Philips.png',
                    'benign_2--ID_8100--484x536--ARIETTA.png',
                    'benign_2--ID_9265--793x413--Philips.jpg',
                    'benign_3--ID_5121--484x536--ARIETTA.png',
                    'benign_3--ID_5218--502x469--ESAOTE.png',
                    'benign_3--ID_5422--793x412--Philips.png',
                    'benign_3--ID_6337b--484x609--Philips.png',
                    'benign_3--ID_7950--793x412--Philips.png',
                    'benign_3--ID_7952--793x412--Philips.png',
                    'benign_3--ID_9187--737x519--SAM_RS85.png',
                    'benign_3--ID_9508_f294--484x536--ARIETTA.png',
                    'benign_4a--ID_5221--502x469--ESAOTE.png',
                    'benign_4a--ID_5277--502x469--ESAOTE.png',
                    'benign_4a--ID_5517--475x240--SAM_RS85.jpg',
                    'benign_4a--ID_6132--502x469--ESAOTE.png',
                    'benign_4a--ID_6645--929x482--SAM_RS85.jpg',
                    'benign_4a--ID_7369--544x531--ARIETTA.png',
                    'benign_4a--ID_7412--484x536--ARIETTA.png',
                    'benign_4a--ID_8713--484x536--ARIETTA.png',
                    'benign_4a--ID_8971--544x531--ARIETTA.png',
                    'benign_4b--ID_4604--608x617--Philips.png',
                    'benign_4b--ID_6963--432x468--ESAOTE.png',
                    'benign_4b--ID_7052--432x468--ESAOTE.png',
                    'benign_4b--ID_7302--432x468--ESAOTE.png',
                    'benign_4b--ID_7641--432x468--ESAOTE.png',
                    'benign_4b--ID_7661--432x468--ESAOTE.png',
                    'benign_4b--ID_9119--795x583--Philips.jpg',
                    'benign_4b--ID_9283--796x500--Philips.png',
                    'benign_4c--ID_4670--760x599--Philips.png',
                    'benign_4c--ID_7497--432x468--ESAOTE.png',
                    'benign_4c--ID_7818--929x482--SAM_RS85.jpg',
                    'benign_4c--ID_8293--432x468--ESAOTE.png',
                    'benign_4--ID_6901--544x531--ARIETTA.png',
                    'benign_5--ID_6300--502x469--ESAOTE.png',
                    'malignant_0--ID_7645--432x468--ESAOTE.png',
                    'malignant_4b--ID_5297--430x469--ESAOTE.png',
                    'malignant_4b--ID_5725--502x469--ESAOTE.png',
                    'malignant_4b--ID_5973--502x469--ESAOTE.png',
                    'malignant_4b--ID_7506--432x468--ESAOTE.png',
                    'malignant_4b--ID_7578--432x468--ESAOTE.png',
                    'malignant_4c--ID_5268--502x469--ESAOTE.png',
                    'malignant_4c--ID_5908--502x469--ESAOTE.png',
                    'malignant_4c--ID_6014--502x469--ESAOTE.png',
                    'malignant_4c--ID_6284--502x469--ESAOTE.png',
                    'malignant_4c--ID_8247--432x468--ESAOTE.png',
                    'malignant_4c--ID_9092--484x536--ARIETTA.png',
                    'malignant_4--ID_4722--793x412--Philips.png',
                    'malignant_5--ID_5561--502x469--ESAOTE.png',
                    'malignant_5--ID_6101--502x469--ESAOTE.png',
                    'malignant_5--ID_6779--432x468--ESAOTE.png',
                    'normal_1--ID_8034--760x608--Philips.jpg'
                    ]

    target_files = target_files_arietta

    print('')
    print('')
    print('')

    # image_dir  = '/mnt/dysk_roboczy/efarell/repos/US-Data/2023-11-12/pipeline-005/data-60/test/'
    image_dir  = '/mnt/dysk_roboczy/efarell/repos/US-Data/2023-11-12/pipeline-005/Device-ARIETTA/test'
    # image_dir  = '/mnt/dysk_roboczy/efarell/repos/US-Data/2023-11-12/pipeline-005/Device-ESAOTE/test'

    # remove any output
    for file in glob.glob('*.jpg'):
        print('Removing:', file)
        os.remove(file)


    for root, dirs, files in os.walk(image_dir):
        # find each image
        for image_path in glob.glob(os.path.join(root, '*')):

            if os.path.isfile(image_path) and os.path.basename(image_path) not in target_files:
                print(image_path)
                print('interested')

                # this is the 'batched' image at 224 x 224
                # We don't pass this as the 'input_tensor' to grad-cam??
                    # img  = PILImage.create(image_path)
                    # input_tensor, = first(learn.dls.test_dl([img]))
                    # print('first tensor:')
                    # print(input_tensor.shape)

                rgb_img = cv2.imread(image_path, 1)[:, :, ::-1]
                rgb_img = np.float32(rgb_img) / 255


                input_tensor = preprocess_image(rgb_img,
                                                mean=[0.485, 0.456, 0.406],
                                                std=[0.229, 0.224, 0.225])

                # print('')
                # print('')
                # print('second tensor:')
                # print(input_tensor.shape)
                # print(input_tensor)


                # Using the with statement ensures the context is freed, and you can
                # recreate different CAM objects in a loop.
                for method in methods:

                    cam_algorithm = methods[method]

                    with cam_algorithm(model      = model,
                                    target_layers = target_layers,
                                    use_cuda      = args.use_cuda) as cam:


                        # AblationCAM and ScoreCAM have batched implementations.
                        # You can override the internal batch size for faster computation.
                        cam.batch_size = 32

                        grayscale_cam = cam(input_tensor = input_tensor,
                                            targets      = targets,
                                            aug_smooth   = args.aug_smooth,
                                            eigen_smooth = args.eigen_smooth)

                        grayscale_cam = grayscale_cam[0, :]

                        cam_image = show_cam_on_image(rgb_img, grayscale_cam, use_rgb=True)
                        cam_image = cv2.cvtColor(cam_image, cv2.COLOR_RGB2BGR)


                    output_dir = './'
                    output_path    = os.path.join(output_dir, f'{os.path.basename(image_path)}_{method}.jpg')
                    os.makedirs(output_dir, exist_ok=True)
                    cv2.imwrite(output_path, cam_image)

                # break
