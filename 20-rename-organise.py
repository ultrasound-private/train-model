#   1. RENAME files
#      ------
#   2. Create new FOLDER STRUCTURE for images
#                 ----------------
#               ▶ "output_dir"
#                      ▶ NULL
#                      ▶ BAD-DATA
#                      ▶ BENCHMARK-256
#                      ▶ VALID-FOR-TRAINING

# Info:
#   Scan 'INPUT_DIR' full of ultrasound images, and move each image into
#   correct sub-folder based on the "type" field in METADATA_CSV.

import csv
import os, sys
import shutil
from PIL import Image
import argparse
import configparser
import utils

# Get handy CONSTANTS
from CONSTANTS import  BENCHMARK_IMAGES
from CONSTANTS import  FILENAME_FORMAT
from CONSTANTS import  DEVICE_MAP
from CONSTANTS import  BAD_IMAGE_IDS


AIM = 'Rename images, remove BAD images, organise folders (BENIGN, MALIGNANT)'

# Set up argument parser
parser = argparse.ArgumentParser(description=AIM)
parser.add_argument('--config',  required=True,  help='Which config to use')
parser.add_argument('--in_dir',  required=True, help='Name of input folder')
parser.add_argument('--out_dir', required=True, help='Name of output folder')
args = parser.parse_args()

# Subfolders to organise / catagorise all images
BAD_DATA_FOLDER  = "BAD-DATA"
BENCHMARK_FOLDER = "BENCHMARK-256"
NULL_FOLDER      = "NULL"
NORMAL_FOLDER    = "NORMAL"
VALID_FOLDER     = "VALID-FOR-TRAINING"
MASK_FOLDER      = "MASKS"
NEW_SUBFOLDERS   = [VALID_FOLDER, NULL_FOLDER, BAD_DATA_FOLDER, BENCHMARK_FOLDER, NORMAL_FOLDER, MASK_FOLDER]


config = configparser.ConfigParser(inline_comment_prefixes=";")
config.read(args.config)

# Define paths/files
EXPORT_DATA  = config['PIPELINE']['EXPORT_DATA']
BASE_DIR     = config[EXPORT_DATA]['BASE_DIR']
META_DIR     = config[EXPORT_DATA]['META_DIR']
PIPELINE_DIR = os.path.join(BASE_DIR, config['PIPELINE']['NAME'])
INPUT_DIR    = os.path.join(PIPELINE_DIR, args.in_dir)
OUTPUT_DIR   = os.path.join(PIPELINE_DIR, args.out_dir)
CSV_META     = os.path.join(META_DIR, config[EXPORT_DATA]['CSV_PREPROCESS'])

utils.print_banner(AIM, os.path.basename(__file__))
print('')
print('EXPORT_DATA ', EXPORT_DATA)
print('INPUT_DIR   ', INPUT_DIR)
print('OUTPUT_DIR  ', OUTPUT_DIR)
print('CSV_META  ', CSV_META)
print('')


# Setup input/output folders
utils.assert_dir_exists(INPUT_DIR)
utils.assert_dir_not_empty(INPUT_DIR)
utils.folder_summary(INPUT_DIR)
utils.recreate_dir(OUTPUT_DIR)


# Create subfolders
for o in NEW_SUBFOLDERS:
    os.makedirs(os.path.join(OUTPUT_DIR, o), exist_ok=False)


# Process CSV
with open(CSV_META, 'r', newline='') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    print("\nProcessing...")

    for row in csv_reader:
        # More readable device name
        row['Device_name_short'] = DEVICE_MAP.get(row['Device_name'], row['Device_name'])

        # Get image extension (.png, .jpg etc)
        file_root, file_ext = os.path.splitext(row['Image_filename'])

        # Create new filename
        new_name = FILENAME_FORMAT.format(
                type     = row['Type'],
                birads   = row['BIRADS'],
                image_id = row['ImageID'],
                width    = row['processed_width'],
                height   = row['processed_height'],
                device   = row['Device_name_short'],
                ext      = file_ext
                )

        # move/rename file
        src  = os.path.join(INPUT_DIR,  row['Image_filename'])

        if row['ImageID'] in BAD_IMAGE_IDS:
            # move bad data to separate folder
            # (don't use for training)
            # print(f"Bad image: {row['ImageID']:<12} {new_name}")
            dest = os.path.join(OUTPUT_DIR, BAD_DATA_FOLDER, new_name)
            shutil.copy(src, dest)

        elif row['Type'].upper() == 'NULL':
            dest = os.path.join(OUTPUT_DIR, NULL_FOLDER, new_name)
            shutil.copy(src, dest)

        elif row['Type'].upper() == 'NORMAL':
            dest = os.path.join(OUTPUT_DIR, NORMAL_FOLDER, new_name)
            shutil.copy(src, dest)

        else:
            dest = os.path.join(OUTPUT_DIR, VALID_FOLDER, new_name)
            shutil.copy(src, dest)


        # ALSO copy some images to 'Curated benchmark dataset' (Norbert, Ania paper)
        if row['ImageID'] in BENCHMARK_IMAGES:
            dest = os.path.join(OUTPUT_DIR, BENCHMARK_FOLDER, new_name)
            shutil.copy(src, dest)


        # Finally... the masks...
        # Save mask file too (only save first mask i.e. [0])
        mask_filename = row['Tumor_mask_filename'].split(';')[0]
        # print(mask_filename)

        mask_src  = os.path.join(INPUT_DIR,  mask_filename)
        mask_dest = os.path.join(OUTPUT_DIR, MASK_FOLDER, mask_filename)

        if os.path.isfile(mask_src):
            shutil.copy(mask_src, mask_dest)


print("Re-org done!")
utils.folder_summary(OUTPUT_DIR)
