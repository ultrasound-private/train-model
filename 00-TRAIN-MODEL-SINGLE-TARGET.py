# In[]
from fastai.vision.all import *
import torch.nn.functional as F
import timm
from torchvision.transforms import ToPILImage

import importlib.metadata as metadata
import utils_nn as my_nn
import utils    as my_utils
import numpy as np

import os, sys
import importlib
from PIL import Image
import matplotlib.pyplot as plt
import pandas as pd

import configparser
from CONSTANTS import CONFIG_PATH

import matplotlib
from matplotlib.font_manager import FontProperties
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from skimage.transform import resize

import cv2
from pytorch_grad_cam                     import (GradCAM, LayerCAM, GradCAMElementWise)
from pytorch_grad_cam                     import GuidedBackpropReLUModel
from pytorch_grad_cam.utils.image         import (show_cam_on_image, deprocess_image, preprocess_image)
from pytorch_grad_cam.utils.model_targets import ClassifierOutputTarget




if my_nn.jupyter_running():
    from IPython import get_ipython
    get_ipython().run_line_magic('matplotlib', 'inline')

# # for ipython
# %matplotlib qt

# importlib.reload(my_nn)

# In[]

AIM = 'Train CNN model (fastai library)'

# Read ini
# CONFIG_PATH = 'config-base-model.ini'
config = configparser.ConfigParser(inline_comment_prefixes=";")
config.read(CONFIG_PATH)


# Config
EXPORT_DATA     =  config['MODEL']['EXPORT_DATA'] # Nov, June etc
BASE_DIR        =  config[EXPORT_DATA]['BASE_DIR']

MODEL_ARCH        = config['MODEL']['MODEL_ARCH']
MODEL_EXPORT_NAME = config.get('MODEL','MODEL_EXPORT_NAME').replace('.', '_')
RUN_COMMENT       = config['MODEL']['COMMENT']
TRAIN_PIPELINE    = config['MODEL']['TRAIN_PIPELINE']

BATCH_SIZE        = int(config['MODEL']['BATCH_SIZE'])
TRAINING_EPOCHS   = int(config['MODEL']['TRAINING_EPOCHS'])

item_tfms_IMAGE_SIZE  = config['MODEL']['item_tfms_IMAGE_SIZE']
batch_tfms_IMAGE_SIZE = config['MODEL']['batch_tfms_IMAGE_SIZE']
item_tfms_IMAGE_SIZE  = tuple(map(int, item_tfms_IMAGE_SIZE.strip('()').split(',')))
batch_tfms_IMAGE_SIZE = tuple(map(int, batch_tfms_IMAGE_SIZE.strip('()').split(',')))


CREATE_PLOTS    = config.getboolean('MODEL', 'CREATE_PLOTS')
INTERPRET_MODEL = config.getboolean('MODEL', 'INTERPRET_MODEL')
TRAIN_NEW_MODEL = config.getboolean('MODEL', 'TRAIN_NEW_MODEL')

TRAIN_DATA        = os.path.join(BASE_DIR, config['MODEL']['TRAIN_DATA'])
TEST_DATA         = os.path.join(BASE_DIR, config['MODEL']['TEST_DATA'])

dynamic_plot_folder = 'plot-' + os.path.basename(TEST_DATA)
MODEL_EXPORT_DIR    = Path(config['MODEL']['MODEL_EXPORT_DIR'])
PLOT_DIR            = Path(os.path.join(config['MODEL']['PLOT_DIR'], dynamic_plot_folder))

# Get key software versions
torch_version  = metadata.version("torch")
fastai_version = metadata.version("fastai")
opencv_version = metadata.version("opencv-python")
pil_version    = metadata.version("pillow")
timm_version   = metadata.version("timm")


if TRAIN_NEW_MODEL:
    highlight = '■■■ TRAINING NEW MODEL ■■■'
else:
    # using saved model
    highlight = ''


# if not my_nn.jupyter_running():
    # my_utils.print_banner(AIM, os.path.basename(__file__))

print('')
print('PyTorch Version:      ', torch_version)
print('FastAI Version:       ', fastai_version)
print('OpenCV Version:       ', opencv_version)
print('PIL Version:          ', pil_version)
print('Timm Version:         ', timm_version)
print('')
print('TRAINING_EPOCHS       ', TRAINING_EPOCHS)
print('item_tfms_IMAGE_SIZE  ', item_tfms_IMAGE_SIZE)
print('batch_tfms_IMAGE_SIZE ', batch_tfms_IMAGE_SIZE)
print('BATCH_SIZE            ', BATCH_SIZE)
print('')
print('CREATE_PLOTS          ', CREATE_PLOTS)
print('INTERPRET_MODEL       ', INTERPRET_MODEL)
print('')
print('PLOT_DIR              ', PLOT_DIR)
print('MODEL_EXPORT_DIR      ', MODEL_EXPORT_DIR)
print('')
print('EXPORT_DATA           ', EXPORT_DATA)
print('TRAIN_DATA            ', TRAIN_DATA)
print('TEST_DATA             ', TEST_DATA)
print('')
print('Config:               ', os.path.basename(CONFIG_PATH))
print('RUN_COMMENT           ', RUN_COMMENT)
print('MODEL_ARCH            ', MODEL_ARCH)
print('MODEL_EXPORT_NAME     ', MODEL_EXPORT_NAME)
print('------------------')
print('TRAIN_NEW_MODEL?      ', TRAIN_NEW_MODEL, highlight)
print('------------------')
print('')


if not my_nn.jupyter_running():
    # Will we go ahead?
    response = input("Continue? (y/n): ").lower()
    if response == 'n':
        exit()


# In[]

# Verify folders
my_utils.assert_dir_exists(TRAIN_DATA)
my_utils.assert_dir_exists(TEST_DATA)
my_utils.make_dir_if_none(MODEL_EXPORT_DIR)
my_utils.recreate_dir(PLOT_DIR)

# In[]

if TRAIN_NEW_MODEL:
    # Save settings used to create this model
    this_script = os.path.abspath(__file__)
    shutil.copy2(CONFIG_PATH, MODEL_EXPORT_DIR)
    shutil.copy2(this_script, MODEL_EXPORT_DIR)


# In[]



# Function to display file names and labels for a given DataLoader
def show_labels(dl):
    for i, batch in enumerate(dl):
        images, labels = batch
        for j in range(len(images)):
            # Get the index of the item in the entire dataset (train or valid)
            index = i * dl.bs + j
            
            # Ensure the index is within the length of the items list
            if index < len(dl.items):
                file_name = dl.items[index].name  # Extract file name
                label = dls.vocab[labels[j]]      # Translate label index to label name
                print(f"{label:<15}  {file_name}")


if not TRAIN_NEW_MODEL:
    # Don't train!
    # Use pre-saved model...

    # Jan 3: This is a good one? No false negatives?
    # convnext_tiny_fb_in22k--pipeline-004-good-no-false-negatives.pkl

    model_path = os.path.join(MODEL_EXPORT_DIR, MODEL_EXPORT_NAME + '.pkl')

    if os.path.isfile(model_path):
        learn = load_learner(model_path)
        print(f"\nLoaded pre-saved model: {model_path}")
    else:
        print(f"\nError loading pre-saved model. Not found: {model_path}")
        sys.exit()

    # print(learn.model)


    # Because we're using a pre-trained model,
    # we can't interpret the model
    # (No matter what it says in config.ini)
    INTERPRET_MODEL = False


# In[]


else:
# In[]

    # Train new model

    # IMAGE_RESIZE = (H, W)
    # IMAGE_RESIZE = (1000, 1000)
    # IMAGE_RESIZE = (1000, 100)
    # IMAGE_RESIZE = (100, 1000)
    # IMAGE_RESIZE = (400, 600)
    # IMAGE_RESIZE = (700, 800)


    # def aug_transforms(
    # mult         : float     = 1.0,                # Multiplication applying to `max_rotate`,`max_lighting`,`max_warp`
    # do_flip      : bool      = True,               # Random flipping
    # flip_vert    : bool      = False,              # Flip vertically
    # max_rotate   : float     = 10.,                # Maximum degree of rotation
    # min_zoom     : float     = 1.,                 # Minimum zoom
    # max_zoom     : float     = 1.1,                # Maximum zoom
    # max_lighting : float     = 0.2,                # Maximum scale of changing brightness
    # max_warp     : float     = 0.2,                # Maximum value of changing warp per
    # p_affine     : float     = 0.75,               # Probability of applying affine transformation
    # p_lighting   : float     = 0.75,               # Probability of changing brightnest and contrast
    # xtra_tfms    : list      = None,               # Custom Transformations
    # size         : int|tuple = None,               # Output size, duplicated if one value is specified
    # mode         : str       = 'bilinear',         # PyTorch `F.grid_sample` interpolation
    # pad_mode                 = PadMode.Reflection, # A `PadMode`
    # align_corners            = True,               # PyTorch `F.grid_sample` align_corners
    # batch                    = False,              # Apply identical transformation to entire batch
    # min_scale                = 1.                  # Minimum scale of the crop, in relation to image area



    # # Get stats to normalise all images
    # # See below: Normalize.from_stats(*my_dataset_stats),
    # stats_file = os.path.join(BASE_DIR, TRAIN_PIPELINE, 'dataset_stats.json')
    # with open(stats_file, 'r') as f:
            # stats_dict = json.load(f)
    # my_dataset_stats = (np.array(stats_dict['mean']), np.array(stats_dict['std']))
    # print(my_dataset_stats)


# In[]

    # Example using Pad
    resize_pad       = Resize(item_tfms_IMAGE_SIZE, method=ResizeMethod.Pad, pad_mode='zeros')

    # item_tfms=[Resize(size=img_size, resamples=(Image.Resampling.LANCZOS, 0))]
    item_tfms=[resize_pad]

    # filters=[
        # ImageFilter.EDGE_ENHANCE_MORE,
        # ImageFilter.EMBOSS,
        # ImageFilter.CONTOUR,
        # ImageFilter.FIND_EDGES,
    # ]

    # xtra_tfms = [Dihedral()]
    xtra_tfms = []

    batch_tfms = aug_transforms(do_flip       = True,
                                flip_vert     = False,
                                max_rotate    = 0.0,
                                max_zoom      = 1.0,
                                min_zoom      = 1.0,
                                p_affine      = 0.0,
                                p_lighting    = 0.0,
                                xtra_tfms     = xtra_tfms,
                                mode          = 'bilinear', pad_mode='zeros',
                                align_corners = True,
                                mult          = 1.0,
                                max_lighting  = 0.0,
                                max_warp      = 0.0,
                                size          = batch_tfms_IMAGE_SIZE,
                                batch         = True,
                                min_scale     = 1.
                                )

    # db = DataBlock(blocks=(ImageBlock, CategoryBlock),
                    # get_x=Pipeline([ColReader(0, pref=pref, suff=suff), ApplyPILFilter(filters, p=0.5)]),
                    # get_y=ColReader(1),
                    # splitter=splitter,
                    # item_tfms=item_tfms,
                    # batch_tfms=batch_tfms)


    # # Define training data
    # data_block=DataBlock(
        # blocks     = (ImageBlock, CategoryBlock),
        # get_items  = get_image_files,
        # splitter   = RandomSplitter(valid_pct=0.2, seed=42),
        # get_y      = parent_label,
        # item_tfms  = item_tfms,
        # batch_tfms = batch_tfms
        # )

    get_arietta       = partial(my_nn.get_images, device_keyword='ARIETTA')
    # get_esatoe      = partial(my_nn.get_images, device_keyword='ESAOTE')
    # get_all_devices = partial(my_nn.get_images)
    # get_all_devices = get_image_files
    get_birads_label  = partial(my_nn.get_labels, label_keyword='birads')
    # lesion_type   = partial(my_nn.get_labels, label_keyword='type')

    my_get_items         = get_arietta

    # Define training data
    data_block=DataBlock(
        blocks     = (ImageBlock, CategoryBlock),
        get_items  = my_get_items,
        splitter   = RandomSplitter(valid_pct=0.2, seed=42),
        get_y      = get_birads_label,
        item_tfms  = item_tfms,
        batch_tfms = batch_tfms
        )



    # Create DataLoader from DataBlock
    dls = data_block.dataloaders(TRAIN_DATA, bs=BATCH_SIZE)

    print(f"Number of training items:   {len(dls.train.items)}")
    print(f"Number of validation items: {len(dls.valid.items)}")

    # Display labels for validation set
    print("\nLabels for validation set:")
    show_labels(dls.valid)

    # from pprint import pprint
    # pprint(dls.train.items[:10])  

    # for item in dls.train.items:  
        # print(os.path.basename(item))


# In[]


    if not my_nn.jupyter_running():
        # Save batch to disk
        print('Saving one batch to disk...')
        batch = dls.one_batch()
        for i, (x, _) in enumerate(zip(batch[0], batch[1])):
            # Convert tensor to PIL image
            img = ToPILImage()(x).convert("RGB")
            # Save image
            img.save(os.path.join('./tmp', f"transformed_image_{i}.png"))
    else:
        # Visualize a batch of images
        dls.show_batch(nrows=4, ncols=4)


# In[]



    # Use pre-trained 'resnet' architecture
    if MODEL_ARCH == 'resnet34':
        learn =vision_learner(dls, resnet34, metrics=accuracy).to_fp16()
    elif MODEL_ARCH == 'resnet18':
        learn =vision_learner(dls, resnet18, metrics=accuracy).to_fp16()
    elif MODEL_ARCH == 'resnet50':
        learn =vision_learner(dls, resnet50, metrics=accuracy).to_fp16()
    else:
        # TODO:
        # learn = vision_learner(dls, MODEL_ARCH, metrics=error_rate).to_fp16() <--- ??? use error_rate
        # learn = vision_learner(dls, MODEL_ARCH, metrics=accuracy).to_fp16()
        learn = vision_learner(dls, MODEL_ARCH, metrics=error_rate).to_fp16()
    # else:
        # print ('Model not found: ', MODEL)
        # print("Exiting... "); import sys; sys.exit()

        # learn.dls.valid_ds.items[0]
        # learn.dls.valid_ds.items
        # learn.metrics

        # dls         : DataLoaders,                                      # `DataLoaders` containing fastai or PyTorch `DataLoader`s
        # model       : callable,                                         # PyTorch model for training or inference
        # loss_func   : callable|None                 = None,             # Loss function. Defaults to `dls` loss
        # opt_func    : Optimizer|OptimWrapper        = Adam,             # Optimization function for training
        # lr          : float|slice                   = defaults.lr,      # Default learning rate
        # splitter    : callable                      = trainable_params, # Split model into parameter groups. Defaults to one parameter group
        # cbs         : Callback|MutableSequence|None = None,             # `Callback`s to add to `Learner`
        # metrics     : callable|MutableSequence|None = None,             # `Metric`s to calculate on validation set
        # path        : str|Path|None                 = None,             # Parent directory to save, load, and export models. Defaults to `dls` `path`
        # model_dir   : str|Path                      = 'models',         # Subdirectory to save and load models
        # wd          : float|int|None                = None,             # Default weight decay
        # wd_bn_bias  : bool                          = False,            # Apply weight decay to normalization and bias parameters
        # train_bn    : bool                          = True,             # Train frozen normalization layers
        # moms        : tuple                         = (0.95,0.85,0.95), # Default momentum for schedulers
        # default_cbs : bool                          = True              # Include default `Callback`s



    print(learn.show_results())

    # print('Summary')
    # print(learn.summary())

# In[]

    learn.dls = dls

    # TODO: experiment with lr... don't just take recommended one
    # lr_min = learn.lr_find(show_plot=False)
    # lr_min = learn.lr_find(show_plot=True)

    # lr_min = learn.lr_find(show_plot=True, suggest_funcs=(slide, valley))
    # print(f"\nMinimum/10: {lr_min[0]}")

    # lr_slide, lr_valley = learn.lr_find(show_plot=True, suggest_funcs=(slide, valley))
    # print('\n')
    # print('\n')
    # print('lr_slide:    ', lr_slide)
    # print('lr_valley:   ', lr_valley)

    lr = 0.001
    # lr = 0.008
    # lr   = (lr_slide + lr_valley) / 2
    # lr   = lr_slide
    # lr   = lr_valley

    print('Picked lr: ', np.round(lr, 4))
    print('\n')

    # lr_min = 0.01
    # lr_min = 0.008
    # print(f"\nlr: {lr_min}")

    # Which is better??
    # -----------------
    learn.fit_one_cycle(TRAINING_EPOCHS, lr)

    # learn.fine_tune(TRAINING_EPOCHS, lr)

    # # Continue training for more epochs
    # extra_epochs = 2
    # lr_min = learn.lr_find(show_plot=False)
    # print(f"\nMinimum/10: {lr_min[0]:.2e}")
    # learn.fit_one_cycle(extra_epochs, lr_min)

    # TODO: try tta: test time agumentation?


    # TODO: use a train func
    # def train(arch, item, batch, epochs=5):
        # dls = ImageDataLoaders.from_folder(trn_path, seed=42, valid_pct=0.2, item_tfms=item, batch_tfms=batch)
        # learn = vision_learner(dls, arch, metrics=error_rate).to_fp16()
        # learn.fine_tune(epochs, 0.01)
        # return learn

    # learn = train(arch, item=Resize((256,192), method=ResizeMethod.Pad, pad_mode=PadMode.Zeros),
                # batch=aug_transforms(size=(171,128), min_scale=0.75))



    # EXPORT model
    model_path = os.path.join(MODEL_EXPORT_DIR, MODEL_EXPORT_NAME + '.pkl')
    learn.export(model_path)
    print("Exported model: ", model_path)



# In[]

# Show top loses

def do_interp(model):
    interp = ClassificationInterpretation.from_learner(model)

    losses, idxs = interp.top_losses()
    len(dls.valid_ds)==len(losses)==len(idxs)

    # Example usage
    if my_nn.jupyter_running():
        interp.show_results(idxs[:CREATE_PLOTS])
        interp.plot_top_losses(CREATE_PLOTS, nrows=int(CREATE_PLOTS / 3), figsize=(12,16))
        interp.plot_confusion_matrix(figsize=(7,7))
    else:
        print("Not in Jupyter: don't plot anything...")

    worst_idxs = idxs[:CREATE_PLOTS].numpy()
    worst_filenames = [dls.valid_ds.items[i] for i in worst_idxs]

    # Uncomment to print filenames
    print("\nWorst performing images:")
    print("\n".join(map(str, worst_filenames)))

    # for i in range(CREATE_PLOTS):
        # my_nn.make_prediction(learn, worst_filenames[i])


if INTERPRET_MODEL:
    do_interp(learn)

# In[]


def plot_pred_summary(image_path, learner, pred_class, true_class, probabilities):
    # Class Activation Maps (CAM)

    classes        = list(learner.dls.vocab)
    true_class_idx = classes.index(true_class)

    # LayerCAM: Exploring Hierarchical Class Activation Maps for Localization
    # https://ieeexplore.ieee.org/document/9462463
    cam_algorithm = LayerCAM
    # cam_algorithm = GradCAM
    # cam_algorithm = GradCAMElementWise


    # Convnext models
    target_layer_id = "0.model.stages.3.blocks.2"
    # target_layer_id = "0.model.stages.3.blocks.2.conv_dw"

    # Resnet models
     # target_layer_id = "0.7.2"
     # target_layer_id = "0.7.2.conv2"


    # Get target layer and convert to list
    target_layers = [learner.get_submodule(target_layer_id)]

    # Specify which target to generate the CAM for.
    # If targets is "None", the highest scoring category (for every member in the batch) will be used.
    # You can target specific categories by:
    # targets = [ClassifierOutputTarget(0), ClassifierOutputTarget(1), ClassifierOutputTarget(2)]
    # targets = [ClassifierOutputTarget(0)]
    # targets = [ClassifierOutputTarget(2)]
    # targets = [ClassifierOutputTarget(true_class_idx)]
    targets = None



    # print('')
    # print(image_path)
    # print(true_class)
    # print(true_class_idx)


    if os.path.splitext(image_path)[1].lower() == '.bmp':
        print(f"Not creating CAM. Invalid image type: {os.path.basename(image_path)}")
        return

    # read image
    img = PILImage.create(image_path)
    # plt.imshow(img)

    # Save plot: Class Activation Map
    if true_class == str(pred_class):
        cam_plot_dir = os.path.join(PLOT_DIR, 'good')
    else:
        cam_plot_dir = os.path.join(PLOT_DIR, 'bad')

    # does it exist?
    # my_utils.assert_dir_exists(PLOT_DIR)


    rgb_img = cv2.imread(image_path, 1)[:, :, ::-1]
    rgb_img = np.float32(rgb_img) / 255

    input_tensor = preprocess_image(rgb_img,
                                    mean=[0.485, 0.456, 0.406],
                                    std=[0.229, 0.224, 0.225])


    with cam_algorithm(model         = learner.model,
                        target_layers = target_layers,
                        use_cuda      = True) as cam:


        grayscale_cam = cam(input_tensor = input_tensor,
                            targets      = targets,
                            aug_smooth   = False,
                            eigen_smooth = False)

        grayscale_cam = grayscale_cam[0, :]

        cam_image = show_cam_on_image(rgb_img, grayscale_cam, use_rgb=True)
        # cam_image = cv2.cvtColor(cam_image, cv2.COLOR_RGB2BGR)

    info_string = (
        f"Prediction:    {pred_class}\n"
        f"Actual:        {true_class}\n"
        f"Classes:       {learner.dls.vocab}\n"
        f"probabilities: {probabilities}\n"
        f"Image :        {os.path.basename(image_path)}\n"
    )

    # Create fig and set up the grid
    fig = plt.figure(figsize=(8, 12))
    gs = gridspec.GridSpec(3, 1, height_ratios=[1, 3, 3])  # Adjust height ratios to control subplot size
    bar_colors = ['yellowgreen', 'orange', 'tomato']

    # Horizontal bar graph for probabilities
    ax0 = plt.subplot(gs[0, 0])  # Bar chart spans all columns of row 0
    display_class_name = [my_nn.replace_with_birads(cls) for cls in classes]

    y_pos = range(len(classes))  # y positions for each class
    bars = ax0.barh(y_pos, probabilities, color=bar_colors)
    ax0.set_xlabel('Probability')
    ax0.set_yticks(y_pos)
    ax0.set_yticklabels(display_class_name, fontweight='bold', fontsize='large')

    # Ensure x-axis starts at 0 and ends at 1
    ax0.set_xlim(0, 1)

    # Remove x and y axis numbers (ticks)
    # Remove ticks and tick labels
    ax0.tick_params(axis='both', which='both', length=0)
    ax0.tick_params(labelbottom=False)

    plot_title = os.path.splitext(os.path.basename(image_path))[0]
    plot_title = plot_title.replace('-', ' ').replace('_', ' ')
    ax0.set_title(f'{plot_title}', fontsize=16)

    # Add probability labels with class names at end of bars
    for class_idx, (prob, cls) in enumerate(zip(probabilities, classes)):
        horizontal_position = bars[class_idx].get_width()
        if prob < 0.7:
            offset = 0.08
        else:
            offset = 0
        ax0.text(horizontal_position + offset, class_idx, f"{prob:.2f}", fontsize=10, va='center', ha='right')

    # identify the true class
    horizontal_position = bars[true_class_idx].get_width()   # Center of true class bar
    offset = 0.30
    ax0.text(horizontal_position + offset, true_class_idx, "⇦TRUE",
            va='center', ha='right', color='dimgray', fontsize=18)  # Center text with bigger font


    # First image (Original)
    ax1 = plt.subplot(gs[1, 0])  # Image on row 1
    ax1.imshow(img)
    ax1.set_title('Original')
    ax1.axis('off')

    # Second image with CAM overlay
    ax2 = plt.subplot(gs[2, 0])  # Image on row 2
    # ax2.imshow(img)
    # ax2.imshow(cam_resized, alpha=0.6, cmap='magma')
    ax2.imshow(cam_image)
    ax2.set_title('Class Activation Map')
    ax2.axis('off')

    plt.tight_layout()

    # Save to file
    cam_image_path = os.path.join(cam_plot_dir, os.path.basename(image_path))
    plt.savefig(cam_image_path, bbox_inches='tight')
    plt.close()  # Close figure to free memory
    print(f'Saved: {cam_image_path}')


# In[]

df_results = my_nn.predict_folder_single_targ(learn, my_get_items, TEST_DATA)
# print(df_results)


# In[]

print(df_results.head())
exit()
# df_results.info()
# df_results.columns
# 'result', 'actual', 'predicted', 'prob_1234_LOW', 'prob_4a4b_MODERATE', 'prob_4c5_HIGH', 'files'

# In[]


# tidy up dataframe
df_results['filename']           = df_results['files'].apply(os.path.basename)
# df_results['prob_1234_LOW']      = df_results['prob_1234_LOW'].round(3)
# df_results['prob_4a4b_MODERATE'] = df_results['prob_4a4b_MODERATE'].round(3)
# df_results['prob_4c5_HIGH']      = df_results['prob_4c5_HIGH'].round(3)

# split into good and bad predictions
df_wrong   = df_results[df_results['result'] == 'Wrong']
df_correct = df_results[df_results['result'] == 'Correct']

# In[]



if CREATE_PLOTS:
    # re-create plot folders
    my_utils.recreate_dir(os.path.join(PLOT_DIR, 'good'))
    my_utils.recreate_dir(os.path.join(PLOT_DIR, 'bad'))

    # plot failures
    # for idx, row in df_wrong.iterrows():
    for idx, row in df_results.iterrows():
        # if idx > 2:
            # break
        probabilities = [row['prob_1234_LOW'],
                        row['prob_4a4b_MODERATE'],
                        row['prob_4c5_HIGH']]
        plot_pred_summary(row['files'], learn, row['predicted'], row['actual'], probabilities)


# In[]
