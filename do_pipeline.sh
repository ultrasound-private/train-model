#!/bin/bash

# Pipelines created
# -----------------
# pipline-005      `LEGION` CONTOURS, no red cross, `YES cropping`, no pixel resize, GREYSCALE
# pipline-004      no legion contour, no red cross, `YES cropping`, no pixel resize, GREYSCALE
# pipline-003      no legion contour, no red cross, `YES cropping`, no pixel resize
# pipline-002      no legion contour, no red cross, no cropping,    no pixel resize
# pipline-001      yellow contour,       red cross, no cropping,    no pixel resize



# # initial processing
# python 10-image-pre-processing.py --config='./CONFIGS/config-PIPELINE.ini'  --in_dir='ORIGINAL-DATA'  --out_dir='10-Processed-Images'
# python 20-rename-organise.py      --config='./CONFIGS/config-PIPELINE.ini'  --in_dir='10-Processed-Images' --out_dir='20-Renamed-Images'


# # # create training pool
# python 30-test-train-split.py  --config=./CONFIGS/config-PIPELINE.ini  --filter=all     --in_dir="20-Renamed-Images/VALID-FOR-TRAINING" --out_dir="Training-Pool-"
# python 30-test-train-split.py  --config=./CONFIGS/config-PIPELINE.ini  --filter=esaote  --in_dir="20-Renamed-Images/VALID-FOR-TRAINING" --out_dir="Training-Pool-"
# python 30-test-train-split.py  --config=./CONFIGS/config-PIPELINE.ini  --filter=arietta --in_dir="20-Renamed-Images/VALID-FOR-TRAINING" --out_dir="Training-Pool-"
python 30-test-train-split.py  --config=./CONFIGS/config-PIPELINE.ini  --filter=Philips --in_dir="20-Renamed-Images/VALID-FOR-TRAINING" --out_dir="Training-Pool-"

# python 30-test-train-split.py  --config=./CONFIGS/config-PIPELINE.ini  --filter=432x    --in_dir="20-Renamed-Images/VALID-FOR-TRAINING" --out_dir="Training-Pool-"




