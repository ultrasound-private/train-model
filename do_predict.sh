#!/bin/bash

# Execute inference / prediction on small Windows Surface Laptop

python 03-windows-11-predict.py --config='./CONFIGS/config-windows-surface.ini'
