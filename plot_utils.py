import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
import matplotlib.gridspec as gridspec
import numpy as np
import os
from fastai.vision.all import Path
import csv
import pandas as pd

import utils_nn      as my_nn

import configparser

import cv2
from pytorch_grad_cam                     import (GradCAM, LayerCAM, GradCAMElementWise)
from pytorch_grad_cam                     import GuidedBackpropReLUModel
from pytorch_grad_cam.utils.image         import (show_cam_on_image, deprocess_image, preprocess_image)
from pytorch_grad_cam.utils.model_targets import ClassifierOutputTarget

import seaborn as sns
from sklearn.metrics import confusion_matrix, precision_recall_curve, auc, roc_curve
from matplotlib.patches import Circle

from sklearn.preprocessing import label_binarize
from matplotlib.font_manager import FontProperties

# Function to find the first matching file path
def find_mask_file(folder, id_str):
    for filename in os.listdir(folder):
        if filename.startswith(id_str):
            return os.path.join(folder, filename)
    return None  # Return None if no match

def insert_cross(image_arr, center, size=14, color=(255, 255, 255), thickness=2):
    # Draw vertical line of the cross
    cv2.line(image_arr, (center[0], center[1] - size), (center[0], center[1] + size), color, thickness)
    # Draw horizontal line of the cross
    cv2.line(image_arr, (center[0] - size, center[1]), (center[0] + size, center[1]), color, thickness)

def draw_bounding_box(image, bbox, color=(255, 0, 0), thickness=2):
    """
    Draw a rectangle (bounding box) on the image.
    """
    x_min, y_min, x_max, y_max = bbox
    cv2.rectangle(image, (x_min, y_min), (x_max, y_max), color, thickness)

# def draw_bounding_box(image, bbox, color=(255, 0, 0), thickness=2):
    # """
    # Draw a rectangle (bounding box) on the image.

    # Parameters:
    # - image: The image array where the rectangle is to be drawn.
    # - bbox: The bounding box coordinates as a tuple (x_min, y_min, x_max, y_max).
    # - color: Rectangle color (default is green).
    # - thickness: Thickness of the rectangle edges.
    # """
    # x_min, y_min, x_max, y_max = bbox
    # # cv2.rectangle(image, (x_min, y_min), (x_max, y_max), color, thickness)
    # cv2.rectangle(image, (y_min, x_min), (y_max, x_max), color, thickness)


def add_hit_miss_text(image, text, position, font=cv2.FONT_HERSHEY_SIMPLEX, 
                      font_scale=1, color=(255, 255, 255), thickness=2):
    """
    Add "hit" or "miss" text to the bottom right-hand corner of an image.

    Parameters:
    - image: The image array where the text is to be added.
    - text: The text to draw ("hit" or "miss").
    - position: The bottom-right corner position to start the text.
    - font: The font type.
    - font_scale: Font scale (font size).
    - color: Text color.
    - thickness: Thickness of the text.
    """
    text_size = cv2.getTextSize(text, font, font_scale, thickness)[0]
    text_x = position[0] - text_size[0]
    text_y = position[1] - text_size[1]
    cv2.putText(image, text, (text_x, text_y), font, font_scale, color, thickness)

def plot_auc(probs, targets, class_labels, plot_title, model_export_name, plot_base_dir):
    """
    Generate ROC AUC plot for both binary and multi-class classification.

    Args:
    - probs (array)           : Predicted probabilities for each class.
    - targets (array)         : True target labels.
    - class_labels (list)     : List of class labels.
    - plot_title (str)        : Title for the plot.
    - model_export_name (str) : Model name for saving the plot.
    """

    # Read ini
    config = configparser.ConfigParser(inline_comment_prefixes=";")
    config.read(CONFIG)

    # Settings
    EXPORT_DATA           = config['MODEL']['EXPORT_DATA'] # Nov, June etc
    BASE_DIR              = config[EXPORT_DATA]['BASE_DIR']
    RUN_COMMENT           = config.get('MODEL','COMMENT')
    MODEL_EXPORT_NAME     = config.get('MODEL','MODEL_EXPORT_NAME').replace('.', '_')
    TRAIN_NEW_MODEL       = config.getboolean('MODEL', 'TRAIN_NEW_MODEL')
    BATCH_SIZE            = int(config['MODEL']['BATCH_SIZE'])
    TRAINING_EPOCHS       = int(config['MODEL']['TRAINING_EPOCHS'])
    item_tfms_IMAGE_SIZE  = config['MODEL']['item_tfms_IMAGE_SIZE']
    batch_tfms_IMAGE_SIZE = config['MODEL']['batch_tfms_IMAGE_SIZE']
    item_tfms_IMAGE_SIZE  = tuple(map(int, item_tfms_IMAGE_SIZE.strip('()').split(',')))
    batch_tfms_IMAGE_SIZE = tuple(map(int, batch_tfms_IMAGE_SIZE.strip('()').split(',')))
    TRAIN_DATA            = os.path.join(BASE_DIR, config['MODEL']['TRAIN_DATA'])
    TEST_DATA             = os.path.join(BASE_DIR, config['MODEL']['TEST_DATA'])


    str_train_dir = '/'.join(Path(TRAIN_DATA).parts[-3:])
    str_test_dir  = '/'.join(Path(TEST_DATA).parts[-3:])

    title_extra  = '\n'
    title_extra += 'Model:    ' + MODEL_EXPORT_NAME + '\n'
    title_extra += 'Comment:  ' + RUN_COMMENT       + '\n'
    title_extra += 'Train:    ' + str_train_dir     + '\n'
    title_extra += 'Test:     ' + str_test_dir


    # Dynamically determine number of classes
    unique_classes = np.unique(targets)
    n_classes      = len(unique_classes)

    # Dynamically binarize the output labels for multi-class
    true_labels_bin = label_binarize(targets, classes=unique_classes)

    # Check if it's a binary classification
    is_binary_classification = n_classes == 2

    # Compute ROC curve and ROC area for each class
    fpr, tpr, roc_auc = dict(), dict(), dict()
    if is_binary_classification:
        # assume 'malignant' is in index 1 in 'probs' array
        positive_class = 1
        fpr[0], tpr[0], _ = roc_curve(true_labels_bin, probs[:, positive_class])
        roc_auc[0] = auc(fpr[0], tpr[0])
    else:
        for i in range(n_classes):
            fpr[i], tpr[i], _ = roc_curve(true_labels_bin[:, i], probs[:, i])
            roc_auc[i] = auc(fpr[i], tpr[i])

    # Plot settings
    colors = ['Green', 'Orange', 'Red', 'Blue', 'Purple', 'Cyan'][:n_classes]
    font_prop = FontProperties(family='DejaVu Sans Mono', size=16)

    plt.figure(figsize=(12, 8))

    ax = plt.gca()
    ax.spines['top'].set_visible(False)

    if is_binary_classification:
        plt.plot(fpr[0], tpr[0], color='blue', lw=3, label=f'ROC: {class_labels[1]:<15} (area: {roc_auc[0]:.3f})')
    else:
        for i, color in zip(range(n_classes), colors):
            plt.plot(fpr[i], tpr[i], color=color, lw=3, label=f'ROC: {class_labels[i]:<15} (area: {roc_auc[i]:.3f})')

    plt.plot([0, 1], [0, 1], 'k--', lw=2)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.00])
    plt.xlabel('False Positive Rate', fontsize=20)
    plt.ylabel('True Positive Rate', fontsize=20)
    plt.title(plot_title + title_extra, loc='left', fontname="DejaVu Sans Mono", pad=20)
    plt.legend(loc="lower right", prop=font_prop)

    # Save plot to disk
    plot_path = os.path.join(plot_base_dir, f'ROC-{model_export_name}.png')
    print(f'Saving ROC plot: {plot_path}')
    plt.savefig(plot_path)
    plt.close()


def create_cam (rgb_img, mask_img, learner, target_idx_lesion, target_idx_birads):
    # LayerCAM: Exploring Hierarchical Class Activation Maps for Localization
    # https://ieeexplore.ieee.org/document/9462463
    cam_algorithm = LayerCAM
    # cam_algorithm = GradCAM
    # cam_algorithm = GradCAMElementWise


    # Convnext models
    # ---------------
    target_layer_id = "0.model.stages.3.blocks.2"
    # target_layer_id = "0.model.stages.3.blocks.2.conv_dw"

    # Resnet models
    # -------------
    # target_layer_id = "0.7.2"
    # target_layer_id = "0.7.2.conv2"


    # Get target layer and convert to list
    target_layers = [learner.get_submodule(target_layer_id)]

    # Specify which target to generate the CAM for.
    # If targets is "None", the highest scoring category (for every member in the batch) will be used.
    # targets = None
    lesion_targets = [ClassifierOutputTarget(target_idx_lesion)]
    birads_targets = [ClassifierOutputTarget(target_idx_birads)]


    input_tensor = preprocess_image(rgb_img,
                                    mean=[0.485, 0.456, 0.406],
                                    std=[0.229, 0.224, 0.225])


    with cam_algorithm(model         = learner.model,
                        target_layers = target_layers,
                        use_cuda      = True) as cam:

        # Two versions of CAM plot:
        #   'cam_image_lesion': CAM for 'benign' or 'malignant'
        #   'cam_image_birads': CAM for birads '4a', '4b', '5' etc

        grayscale_cam_lesion = cam(input_tensor = input_tensor,
                                   targets      = lesion_targets,
                                   aug_smooth   = False,
                                   eigen_smooth = False)


        grayscale_cam_birads = cam(input_tensor = input_tensor,
                                   targets      = birads_targets,
                                   aug_smooth   = False,
                                   eigen_smooth = False)

        grayscale_cam_lesion = grayscale_cam_lesion[0, :]
        grayscale_cam_birads = grayscale_cam_birads[0, :]


        # Pointing game
        # -------------
        max_val_coords = np.unravel_index(np.argmax(grayscale_cam_lesion), grayscale_cam_lesion.shape)

        # Step 1: Find the coordinates of the non-zero pixels
        non_zero_coords = np.argwhere(mask_img == 255)

        # Calculate the bounding box by finding the min and max of the coordinates
        y_min, x_min = non_zero_coords.min(axis=0)
        y_max, x_max = non_zero_coords.max(axis=0)

        # Expand bounding box 
        padding = 15

        # Calculate the expanded bounding box, ensuring it doesn't go beyond the image boundaries
        x_min = max(x_min - padding, 0)
        y_min = max(y_min - padding, 0)
        x_max = min(x_max + padding, mask_img.shape[1] - 1)
        y_max = min(y_max + padding, mask_img.shape[0] - 1)

        # Step 2: Check if max_val_coords falls within bounding box
        y, x = max_val_coords  
        is_in_lesion = x_min <= x <= x_max and y_min <= y <= y_max
        pointing_game = int(is_in_lesion)


        cam_image_lesion = show_cam_on_image(rgb_img, grayscale_cam_lesion, use_rgb=True)
        cam_image_birads = show_cam_on_image(rgb_img, grayscale_cam_birads, use_rgb=True)


        # draw CROSS on max activation point
        insert_cross(cam_image_lesion, (x, y))

        # draw BOUNDING BOX around lesion
        bbox = (x_min, y_min, x_max, y_max)  
        draw_bounding_box(cam_image_lesion, bbox)

        # Insert text to say if 'hit' or 'miss'
        text = 'hit' if is_in_lesion else 'miss'
        image_height, image_width = cam_image_lesion.shape[:2]
        position = (image_width - 10, image_height - 10)  # 10 pixels from the bottom right corner
        add_hit_miss_text(cam_image_lesion, text, position)


            # DEBUGGING CODE
            # img_bgr = cv2.cvtColor(cam_image_lesion, cv2.COLOR_RGB2BGR)
            # name = str(cam_image_lesion.shape).replace('(', '').replace(')', '').replace(',', '-').replace(' ', '-')
            # name += name + '.png'
            # print(name)
            # cv2.imwrite(name, img_bgr)


        return (cam_image_lesion, cam_image_birads, max_val_coords, pointing_game)



def plot_prediction_multi(main_title, image_path, learner, lesion_outcome, birads_outcome, pred_class_lesion, pred_class_birads, true_class_lesion, true_class_birads, probabilities, plot_base_dir):
    # Assuming probabilities is a list of two lists, one for each group of classes
    prob_lesion, prob_birads = probabilities

    # Extract classes for each group
    lesion_classes = list(learner.dls.vocab[0])
    birads_classes = list(learner.dls.vocab[1])


    # Determine the index of the true classes in their respective groups
    true_idx_lesion = lesion_classes.index(true_class_lesion) if true_class_lesion in lesion_classes else -1
    true_idx_birads = birads_classes.index(true_class_birads) if true_class_birads in birads_classes else -1
    pred_idx_lesion = lesion_classes.index(pred_class_lesion) if pred_class_lesion in lesion_classes else -1
    pred_idx_birads = birads_classes.index(pred_class_birads) if pred_class_birads in birads_classes else -1

    # print('predicted:', pred_class_lesion, pred_idx_lesion)
    # print('true:     ', true_class_lesion, true_idx_lesion)

    # Save plot: Class Activation Map
    if lesion_outcome == 'Bad' or birads_outcome == 'Bad':
        plot_dir = os.path.join(plot_base_dir, 'bad')
    else:
        plot_dir = os.path.join(plot_base_dir, 'good')


    # Read the image using cv2.imread and convert it to RGB format
    rgb_img = cv2.imread(str(image_path), cv2.IMREAD_COLOR)[:, :, ::-1]

    # Create grayscale copy too
    image_gray = cv2.cvtColor(rgb_img, cv2.COLOR_RGB2GRAY)

    # Now normalise rgb_img
    rgb_img = np.float32(rgb_img) / 255


    # Now get the mask
    # ----------------

    # Get first matching file path
    ID = my_nn.get_labels(image_path, 'image_id')
    mask_path = find_mask_file(MASK_FOLDER, ID)
    # print(ID)
    # print(mask_path)

    # Read image with all channels, then convert to grayscale
    mask_img = cv2.imread(mask_path, cv2.IMREAD_UNCHANGED)
    mask_img = cv2.cvtColor(mask_img, cv2.COLOR_BGR2GRAY)

    # create class activation map of the PREDICTIONS (i.e. the highest prob)
    cam_image_lesion, cam_image_birads, max_val_coords, pointing_game = create_cam(rgb_img, mask_img, learner, pred_idx_lesion, pred_idx_birads)

    # which will we show on the final plots?
    plot_cam_overlay = cam_image_lesion
    # plot_cam_overlay = cam_image_birads

    # Append data to the csv file
    with open(POINTING_GAME_CSV, 'a', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerow([os.path.basename(image_path), os.path.basename(mask_path), max_val_coords, pointing_game])

    # Create fig
    fig = plt.figure(figsize=(12, 10))
    # GridSpec: 3 rows, 2 columns
    gs = gridspec.GridSpec(3, 2, height_ratios=[0.7, 1, 3])
    lesion_colors = ['darkseagreen', 'rosybrown']

    main_title += '\n' + os.path.basename(image_path)
    fig.suptitle(main_title, fontsize=18, y=1.05)

    # lesion type: Create bar chart  ('benign' etc)
    ax0 = plt.subplot(gs[0, 0])
    y_pos_lesion = range(len(lesion_classes))
    ax0.barh(y_pos_lesion, prob_lesion, color=lesion_colors)
    ax0.set_yticks(y_pos_lesion)
    ax0.set_yticklabels(lesion_classes, fontweight='bold', fontsize='large')
    ax0.set_xlim(0, 1)
    ax0.set_xticks([0, 0.25, 0.5, 0.75])  # Set x-ticks
    ax0.grid(axis='x', color='lightgrey', linestyle='-', linewidth=0.5)  # Add vertical gridlines
    ax0.tick_params(axis='both', which='both', length=0)
    ax0.set_title('Lesion Probability', fontsize=14)


    # BIRADS rating: Create 2nd bar chart (share x-axis with 1st bar chart)
    ax1 = plt.subplot(gs[1, 0], sharex=ax0)
    # Define custom order for the y-ticks and labels
    custom_order = ['LOW', 'MODERATE', 'HIGH']
    birads_colors = ['darkseagreen', 'goldenrod', 'rosybrown']
    custom_y_pos = range(len(custom_order))
    # Map probabilities to the custom order
    # Assumes: prob_birads is ordered as in birads_classes
    # Adjust if 'prob_birads' is not in same order as 'birads_classes'
    mapped_probabilities = [prob_birads[birads_classes.index(rating)] for rating in custom_order]
    # ax1.barh(custom_y_pos, mapped_probabilities, color='lightsteelblue')
    ax1.barh(custom_y_pos, mapped_probabilities, color=birads_colors)
    ax1.set_yticks(custom_y_pos)
    ax1.set_yticklabels(custom_order, fontweight='bold', fontsize='large')
    ax1.set_xlim(0, 1)
    ax1.grid(axis='x', color='lightgrey', linestyle='-', linewidth=0.5)  # Add vertical gridlines
    ax1.tick_params(axis='both', which='both', length=0)
    ax1.set_title('BIRADS Probability', fontsize=14)


    # lesion type: Mark ground truth
    for idx, value in enumerate(prob_lesion):
        # Position the text at the end of each bar
        ax0.text(value, idx, ' True' if idx == true_idx_lesion else '',
                va='center', color='gray', fontweight='bold')

    # birads: mark ground truth
    true_class_position_birads = custom_order.index(birads_classes[true_idx_birads]) if true_idx_birads != -1 else -1
    for idx, value in enumerate(mapped_probabilities):
        # Position the text at the end of each bar
        ax1.text(value, idx, ' True' if idx == true_class_position_birads else '',
                va='center', color='gray', fontweight='bold')


    # Plot pixel histogram
    ax_pixel = plt.subplot(gs[0:2, 1])
    # ax_pixel.hist(image_gray.ravel(), bins=256, range=[0, 256], density=True)
    # ax_pixel.set_ylim([0.0, 0.2])
    ax_pixel.hist(image_gray.ravel(), bins=256, range=[0, 256])
    ax_pixel.set_ylim([0.0, 6000])
    ax_pixel.set_xlim([0, 255])
    ax_pixel.set_title('Pixel Intensity')


    # First image (Original)
    ax_img = plt.subplot(gs[2, 1])
    ax_img.imshow(rgb_img)
    ax_img.set_title('Original')
    ax_img.axis('off')


    # CAM overlay 
    ax_cam = plt.subplot(gs[2, 0])
    ax_cam.imshow(plot_cam_overlay)
    ax_cam.set_title('Class Activation Map (LESION)')
    ax_cam.axis('off')

    # Save to file
    plt.subplots_adjust(top=0.85)
    plt.tight_layout()
    cam_image_path = os.path.join(plot_dir, os.path.basename(image_path))
    valid_image_path = cam_image_path.replace('.BMP', '.png') # just in case it's a bmp file
    plt.savefig(valid_image_path, bbox_inches='tight')
    plt.close()
    print(f'Saved: {cam_image_path}')


def plot_confusion_matrix(true_labels, predicted_labels, classes, title, ax=None):
    cm = confusion_matrix(true_labels, predicted_labels, labels=classes)
    sns.heatmap(cm, annot=True, fmt='d', cmap='Blues', xticklabels=classes, yticklabels=classes, ax=ax, annot_kws={"size": 38}, cbar=False)

    # Set labels and titles
    ax.set_xlabel('Predicted', fontsize=20)
    ax.set_ylabel('True', fontsize=20)
    ax.set_title(title, fontsize=24)

    # Increase tick label font size
    ax.set_xticklabels(ax.get_xticklabels(), fontsize=30)
    ax.set_yticklabels(ax.get_yticklabels(), fontsize=30)

    # Circle the bottom left-hand corner entry
    circle = Circle((0.5, len(classes) - 0.5), 0.3, color='red', fill=False, linewidth=3, transform=ax.transData)
    ax.add_patch(circle)
    # Add 'false negatives'  above circle
    # ax.text(0.5, len(classes) - 0.1, 'False Negatives', color='red', ha='center', va='bottom', fontsize=18, transform=ax.transData)



def plot_probability_distribution(data, prob_columns, title, ax=None, colors=None):
    if colors is None:
        colors = ['blue', 'orange', 'green', 'red', 'purple', 'brown', 'pink', 'gray', 'olive', 'cyan']  # Default colors
    for col, color in zip(prob_columns, colors):
        sns.kdeplot(data[col], ax=ax, fill=True, common_norm=False, alpha=.5, linewidth=1.5, color=color, label=col, clip=[0, 1])
    ax.set_title(title, fontsize=16)
    ax.set_xlabel('Probabilities', fontsize=24)
    ax.tick_params(axis='x', labelsize=20)
    # ax.set_xticklabels(ax.get_xticklabels(), fontsize=20)
    ax.set_ylabel('Density')
    ax.set_yticks([])
    ax.legend()


# Function to plot Precision-Recall Curve
def plot_precision_recall_curve(true_labels, probabilities, title, ax=None):
    precision, recall, thresholds = precision_recall_curve(true_labels, probabilities)
    ax.plot(recall, precision, marker='.')
    ax.set_xlabel('Recall', fontsize=16)
    ax.set_ylabel('Precision', fontsize=16)
    ax.set_title(title, fontsize=16)
    auc_score = auc(recall, precision)
    ax.legend([f'AUC={auc_score:.2f}'], fontsize=18)


def plot_lesion_probs_with_errors(data, title, ax=None):
    # Plot the KDE for the malignant predictions
    sns.kdeplot(data['lesion_prob_malignant'], ax=ax, fill=True, color='rosybrown', label='Malignant', alpha=.5, clip=[0, 1])

    # Identify the false negatives
    false_negatives = data[(data['lesion_prob_malignant'] < 0.5) & (data['lesion_true'] == 'malignant')]

    # Overlay false negatives as larger, labeled points on the plot
    for i, point in false_negatives.iterrows():
        ax.scatter(point['lesion_prob_malignant'], 0.1, color='blue', s=150, marker='x', zorder=5)  # s controls the size of the marker

    ax.set_xticks([0.0, 0.25, 0.5, 0.75, 1.0])

    ax.set_title(title, fontsize=16)
    ax.set_xlabel('Probabilities', fontsize=24)
    ax.tick_params(axis='x', labelsize=20)
    # ax.set_xticklabels(ax.get_xticklabels(), fontsize=20)
    ax.set_ylabel('Density', fontsize=28)
    # ax.set_ylabel(None)
    ax.set_yticks([])
    ax.legend(fontsize=24)



def plots_probs_v_cases(data, ax):
    # Separate correct and Wrong predictions
    correct_predictions = data[data['lesion_outcome'] == 'Good']
    wrong_predictions   = data[data['lesion_outcome'] == 'Bad']

    ax.yaxis.set_label_position("right")
    ax.yaxis.tick_right()

    # Plot malignant probabilities
    ax.scatter(correct_predictions.index, correct_predictions['lesion_prob_malignant'], color='green', label='Correct', alpha=0.5, s=250)
    ax.scatter(wrong_predictions.index, wrong_predictions['lesion_prob_malignant'],     color='red',   label='Wrong',   alpha=0.5, s=250)
    ax.set_title('Malignant? Test Case Probabilities.\nFailures in RED', fontsize=20)
    ax.set_xlabel('Test Cases', fontsize=14)
    ax.set_ylabel('Malignant?', fontsize=30)
    ax.tick_params(axis='y', labelsize=24)
    ax.set_yticks([0.0, 0.25, 0.5, 0.75, 1.0])
     # Add a light dotted horizontal line at y=0.5
    ax.axhline(y=0.5, color='grey', linestyle=':', linewidth=1.5)
    ax.legend(fontsize=18)
    ax.legend().set_visible(False)


def plot_pointing_game(csv_file_path, plot_title, plot_path):
    # Load CSV file
    df = pd.read_csv(csv_file_path)

    # Calculate number of hits and misses
    hits   = df[df['pointing_game'] == 1].shape[0]
    misses = df[df['pointing_game'] == 0].shape[0]

    # Calculate percentages
    total             = hits + misses
    hits_percentage   = hits / total
    misses_percentage = misses / total

    plt.figure()
    ax = plt.gca()
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)

    # Plotting
    hits_bar = plt.bar('Hits', hits, label=f'Hits ({hits_percentage:.2f})', color='darkseagreen')[0]  # First bar (Hits)
    plt.bar('Misses', misses, label=f'Miss ({misses_percentage:.2f})', color='rosybrown')

    # Adding text in the middle of the 'Hits' bar
    yval = hits_bar.get_height()
    ax.text(hits_bar.get_x() + hits_bar.get_width()/2, yval, f'{hits_percentage:.2f}', va='bottom', ha='center', fontsize=14, color='black')

    # plt.ylabel('Count')
    plt.title('POINTING GAME\n' + plot_title, fontsize=18, pad=20)
    plt.xticks(fontsize=18)
    # Add faint gray dotted horizontal gridlines and set them behind bars
    plt.grid(axis='y', color='gray', linestyle='--', linewidth=0.5, alpha=0.7, zorder=0)
    plt.yticks(fontsize=18)
    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5), fontsize=16)
    plt.savefig(plot_path, bbox_inches='tight')
    plt.close()

    return round(hits_percentage, 2)
