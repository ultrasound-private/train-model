#!/bin/bash

## All devices
    # python 02-TRAIN-MODEL-MULTI-TARGET.py --config='./CONFIGS/config-train-all-devices-masks.ini'
    # done python 02-TRAIN-MODEL-MULTI-TARGET.py --config='./CONFIGS/config-train-all-devices-NO-masks.ini'

## Arietta device
    # done python 02-TRAIN-MODEL-MULTI-TARGET.py --config='./CONFIGS/config-train-arietta-masks.ini'
    # done python 02-TRAIN-MODEL-MULTI-TARGET.py --config='./CONFIGS/config-train-arietta-no-masks.ini'

## Esaote device
    # done python 02-TRAIN-MODEL-MULTI-TARGET.py --config='./CONFIGS/config-train-esaote-masks.ini'
    # python 02-TRAIN-MODEL-MULTI-TARGET.py --config='./CONFIGS/config-train-esaote-no-masks.ini'

## Philips device
    # python 02-TRAIN-MODEL-MULTI-TARGET.py --config='./CONFIGS/config-train-philips-masks.ini'
    python 02-TRAIN-MODEL-MULTI-TARGET.py --config='./CONFIGS/config-train-philips-no-masks.ini'

## Resnet
    # python 02-TRAIN-MODEL-MULTI-TARGET.py --config='./CONFIGS/config-train-resnet-all-devices-masks.ini'
    # python 02-TRAIN-MODEL-MULTI-TARGET.py --config='./CONFIGS/config-train-resnet-all-devices-no-masks.ini'

## Other Architectures
    # python 02-TRAIN-MODEL-MULTI-TARGET.py --config='./CONFIGS/config-train-vit.ini'
    # python 02-TRAIN-MODEL-MULTI-TARGET.py --config='./CONFIGS/config-train-swin.ini'


