from fastai.vision.all import *
import json
import os
import numpy as np
from pathlib import Path
from torchvision.transforms import ToPILImage


# Replace these with your actual directory and size
IMAGE_RESIZE   = 256  # Replace with your value
TRAIN_DATA     = '/home/efarell/Extracted-IDs/'
BATCH_SIZE     = 8  # Since you have 8 images

# This function is needed to load the images
def get_image_files(path):
    path = Path(path)
    return [path/f for f in os.listdir(path) if not f.startswith('.')]

resize_pad       = Resize(IMAGE_RESIZE, method=ResizeMethod.Pad, pad_mode='zeros')  # you can change pad_mode


# Replace 'resize_pad' with 'resize_crop' or another item transform if needed
# item_tfms = Resize(IMAGE_RESIZE, method=ResizeMethod.Crop)  # or Pad
item_tfms = resize_pad

# Load your dataset stats
stats_file =  '/home/efarell/Extracted-IDs/small-stats.json'
with open(stats_file, 'r') as f:
    stats_dict = json.load(f)
my_dataset_stats = (np.array(stats_dict['mean']), np.array(stats_dict['std']))
print("Dataset stats (mean, std):", my_dataset_stats)

# Define your DataBlock
data_block = DataBlock(
    blocks=(ImageBlock, CategoryBlock),
    get_items=get_image_files,
    splitter=RandomSplitter(valid_pct=0.2, seed=42),
    get_y=parent_label,
    item_tfms=item_tfms,
    batch_tfms=[Normalize.from_stats(*my_dataset_stats)]
)

# Create DataLoader from DataBlock
dls = data_block.dataloaders(TRAIN_DATA, bs=BATCH_SIZE)

# You need to call `dls.train.one_batch()` to get a batch from the training set
batch = dls.train.one_batch()

# # Iterate through the batch and save the transformed images
# for i, (x, _) in enumerate(zip(batch[0], batch[1])):
    # # Convert tensor to PIL image
    # img = TensorImage(x) # Use TensorImage to keep the batch tfms
    # # If you're on a GPU, move img to CPU
    # img = img.cpu()
    # # Reverse normalization for viewing
    # # img = img*torch.tensor(my_dataset_stats[1]).view(1, -1, 1, 1) + torch.tensor(my_dataset_stats[0]).view(1, -1, 1, 1)
    # # img = img.clamp(0, 1)
    # # Convert to PIL image
    # img = PILImage.create(img.permute(1, 2, 0))
    # # img = Image(img.permute(1, 2, 0))
    # # Save image
    # img_file = f"transformed_image_{i}.png"
    # img.save(img_file)
    # print('saved:', img_file)


for i, (x, _) in enumerate(zip(batch[0], batch[1])):
    img = TensorImage(x) # Use TensorImage to keep the batch tfms
    img = img.cpu()
    to_pil = ToPILImage()
    img_pil = to_pil(img)
    img_file = f"transformed_image_{i}.png"
    img_pil.save(img_file)
    print('saved:', img_file)


for i, (x, _) in enumerate(zip(batch[0], batch[1])):
    img = TensorImage(x)  # Use TensorImage to keep the batch tfms
    img = img.cpu()

    # Reverse normalization
    mean = torch.tensor(my_dataset_stats[0]).view(1, -1, 1, 1)
    std  = torch.tensor(my_dataset_stats[1]).view(1, -1, 1, 1)
    img  = (img * std) + mean
    img  = img.clamp(0, 1)

    # Convert to PIL image 
    # Note: ToPILImage() function expects a 2D or 3D tensor (HxW or CxHxW), but
    # after reversing the normalization, the tensor img has an additional batch
    # dimension (BxCxHxW), which is not compatible.

    img_pil = ToPILImage()(img.squeeze())

    # Save image
    img_file = f"reversed_image_{i}.png"
    img_pil.save(img_file)
    print('saved:', img_file)
