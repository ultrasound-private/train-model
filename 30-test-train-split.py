# 
# AIM: Take an input folder and Create new output folder with structure:
# 
#   ▶▶▶ "OUTPUT_DIR"
#          ▶▶ training
#                   90% files
#          ▶▶ test
#                   10% files


import os, sys
import shutil
from math import ceil
from random import shuffle
import argparse
import configparser
import utils


AIM = 'Split data into [TEST] and [TRAINING] folders'

# argument parser
parser = argparse.ArgumentParser(description=AIM)
parser.add_argument('--config',  required=True,  help='Which config to use')
parser.add_argument('--filter',  required=False, help='Filter string for filenames', default='all', type=str.lower)
parser.add_argument('--in_dir',  required=True,  help='Name of input folder')
parser.add_argument('--out_dir', required=True,  help='Name of output folder')
args = parser.parse_args()                                                                        

# read ini
config = configparser.ConfigParser(inline_comment_prefixes=";")
config.read(args.config)

EXPORT_DATA          = config['PIPELINE']['EXPORT_DATA']
BASE_DIR             = config[EXPORT_DATA]['BASE_DIR']
PIPELINE_DIR         = os.path.join(BASE_DIR, config['PIPELINE']['NAME'])
PERCENT_FOR_TRAINING = config.getfloat('PIPELINE', 'PERCENT_FOR_TRAINING')
SHUFFLE_FILES        = config.getboolean('PIPELINE', 'SHUFFLE_FILES')
INPUT_DIR            = os.path.join(PIPELINE_DIR, args.in_dir)
OUTPUT_DIR           = os.path.join(PIPELINE_DIR, args.out_dir+args.filter)


# Display settings
utils.print_banner(AIM, os.path.basename(__file__))
print('EXPORT_DATA          ', EXPORT_DATA)
print('INPUT_DIR            ', INPUT_DIR)
print('OUTPUT_DIR           ', OUTPUT_DIR)
print('PERCENT_FOR_TRAINING ', PERCENT_FOR_TRAINING)
print('SHUFFLE_FILES        ', SHUFFLE_FILES)
print('FILTER               ', args.filter)
print('')

# Setup input/output folders
utils.assert_dir_exists(INPUT_DIR)
utils.assert_dir_not_empty(INPUT_DIR)
utils.folder_summary(INPUT_DIR)
utils.recreate_dir(OUTPUT_DIR)

os.makedirs(os.path.join(OUTPUT_DIR, "training"), exist_ok=True)
os.makedirs(os.path.join(OUTPUT_DIR, "test"), exist_ok=True)


# Split and copy files with filter
if args.filter == 'all':
    filter_str = ''
else:
    filter_str = args.filter

files = [f for f in os.listdir(INPUT_DIR) 
         if os.path.isfile(os.path.join(INPUT_DIR, f)) and filter_str in f.lower()]


if SHUFFLE_FILES:
    print("Shuffling files for training/test split...")
    shuffle(files)

# Calculate number for training
num_train_files = ceil(PERCENT_FOR_TRAINING * len(files))

# Copy files to 'TRAINING'
for f in files[:num_train_files]:
    shutil.copy(os.path.join(INPUT_DIR, f), os.path.join(OUTPUT_DIR, "training", f))

# Copy remaining files to 'TEST'
for f in files[num_train_files:]:
    shutil.copy(os.path.join(INPUT_DIR, f), os.path.join(OUTPUT_DIR, "test", f))

print(f"\nFiles copied to {OUTPUT_DIR}!")
utils.folder_summary(OUTPUT_DIR)
