# In[]
import cv2

from fastai.vision.all import *
import torch.nn.functional as F
import timm
from torchvision.transforms import ToPILImage

import importlib.metadata as metadata
import utils_nn as my_nn
import utils    as my_utils
import numpy as np

import os, sys
# import importlib
# from PIL import Image
import matplotlib.pyplot as plt
import pandas as pd

import configparser
import argparse

import matplotlib
from matplotlib.font_manager import FontProperties
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from skimage.transform import resize

# from pytorch_grad_cam                     import (GradCAM, LayerCAM, GradCAMElementWise)
# from pytorch_grad_cam                     import GuidedBackpropReLUModel
# from pytorch_grad_cam.utils.image         import (show_cam_on_image, deprocess_image, preprocess_image)
# from pytorch_grad_cam.utils.model_targets import ClassifierOutputTarget

import pathlib
from pathlib import Path
pathlib.PosixPath = pathlib.WindowsPath


# import torch
# torch.cuda.set_device('cpu')

# fastai.defaults.device = 'cpu'

# In[]

AIM = 'Train CNN model (fastai library)'




# GET CONFIG FILE
# Pass a training config to this script.  e.g.
#       'config-train-all-devices.ini'
#       'config-train-arietta.ini'
#       'config-train-esaote.ini'
parser = argparse.ArgumentParser(description=AIM)
parser.add_argument('--config', required=True, help='Specify training config. e.g. config-arietta.ini')
args            = parser.parse_args()
CONFIG          = args.config
my_utils.CONFIG = args.config

# Read ini
config = configparser.ConfigParser(inline_comment_prefixes=";")
config.read(CONFIG)

# Config
EXPORT_DATA           = config['MODEL']['EXPORT_DATA'] # Nov, June etc
BASE_DIR              = config[EXPORT_DATA]['BASE_DIR']
MODEL_ARCH            = config['MODEL']['MODEL_ARCH']
MODEL_EXPORT_NAME     = config.get('MODEL','MODEL_EXPORT_NAME').replace('.', '_')
RUN_COMMENT           = config['MODEL']['COMMENT']
TRAIN_PIPELINE        = config['MODEL']['TRAIN_PIPELINE']
BATCH_SIZE            = int(config['MODEL']['BATCH_SIZE'])
TRAINING_EPOCHS       = int(config['MODEL']['TRAINING_EPOCHS'])
item_tfms_IMAGE_SIZE  = config['MODEL']['item_tfms_IMAGE_SIZE']
batch_tfms_IMAGE_SIZE = config['MODEL']['batch_tfms_IMAGE_SIZE']
item_tfms_IMAGE_SIZE  = tuple(map(int, item_tfms_IMAGE_SIZE.strip('()').split(',')))
batch_tfms_IMAGE_SIZE = tuple(map(int, batch_tfms_IMAGE_SIZE.strip('()').split(',')))
CREATE_PLOTS          = config.getboolean('MODEL', 'CREATE_PLOTS')
INTERPRET_MODEL       = config.getboolean('MODEL', 'INTERPRET_MODEL')
TRAIN_NEW_MODEL       = config.getboolean('MODEL', 'TRAIN_NEW_MODEL')
TRAIN_DATA            = os.path.join(BASE_DIR, config['MODEL']['TRAIN_DATA'])
TEST_DATA             = os.path.join(BASE_DIR, config['MODEL']['TEST_DATA'])
dynamic_plot_folder   = 'plot-' + os.path.basename(TEST_DATA)
MODEL_EXPORT_DIR      = Path(config['MODEL']['MODEL_EXPORT_DIR'])
PLOT_DIR              = Path(os.path.join(config['MODEL']['PLOT_DIR'], dynamic_plot_folder))


if TRAIN_NEW_MODEL:
    highlight = '■■■ TRAINING NEW MODEL ■■■'
else:
    # using saved model
    highlight = ''


# if not my_nn.jupyter_running():
    # my_utils.print_banner(AIM, os.path.basename(__file__))

print('')
print('PyTorch Version:      ', metadata.version("torch"))
print('FastAI Version:       ', metadata.version("fastai"))
print('OpenCV Version:       ', metadata.version("opencv-python"))
print('PIL Version:          ', metadata.version("pillow") )
print('Timm Version:         ', metadata.version("timm"))
print('')
print('TRAINING_EPOCHS       ', TRAINING_EPOCHS)
print('item_tfms_IMAGE_SIZE  ', item_tfms_IMAGE_SIZE)
print('batch_tfms_IMAGE_SIZE ', batch_tfms_IMAGE_SIZE)
print('BATCH_SIZE            ', BATCH_SIZE)
print('')
print('CREATE_PLOTS          ', CREATE_PLOTS)
print('INTERPRET_MODEL       ', INTERPRET_MODEL)
print('')
print('PLOT_DIR              ', PLOT_DIR)
print('MODEL_EXPORT_DIR      ', MODEL_EXPORT_DIR)
print('')
print('EXPORT_DATA           ', EXPORT_DATA)
print('TRAIN_DATA            ', TRAIN_DATA)
print('TEST_DATA             ', TEST_DATA)
print('')
print('Config:               ', os.path.basename(CONFIG))
print('RUN_COMMENT           ', RUN_COMMENT)
print('MODEL_ARCH            ', MODEL_ARCH)
print('MODEL_EXPORT_NAME     ', MODEL_EXPORT_NAME)
print('------------------')
print('TRAIN_NEW_MODEL?      ', TRAIN_NEW_MODEL, highlight)
print('------------------')
print('')


if not my_nn.jupyter_running():
    # Will we go ahead?
    response = input("Continue? (y/n): ").lower()
    if response == 'n':
        exit()


# In[]

# Verify folders
my_utils.assert_dir_exists(TRAIN_DATA)
my_utils.assert_dir_exists(TEST_DATA)
my_utils.make_dir_if_none(MODEL_EXPORT_DIR)
my_utils.recreate_dir(PLOT_DIR)

# In[]




# METRIC FUNCTIONS
# NOTE: it's a multi-target model
#   learn.dls.vocab:
#     [
#     ['benign', 'malignant'],
#     ['HIGH', 'LOW', 'MODERATE']
#     ]
def combine_loss(inp, lesion, birads):
    return my_nn.lesion_loss(inp, lesion, birads) + my_nn.birads_loss(inp, lesion, birads)

err_metrics = (my_nn.lesion_err, my_nn.birads_err)
all_metrics = err_metrics + (my_nn.lesion_loss, my_nn.birads_loss)

# Datablock functions
my_get_items = get_image_files


# In[]


model_path = os.path.join(MODEL_EXPORT_DIR, MODEL_EXPORT_NAME + '.pkl')

if os.path.isfile(model_path):
    learn = load_learner(model_path)
    print(f"\nLoaded pre-saved model: {model_path}")
else:
    print(f"\nError loading pre-saved model. Not found: {model_path}")
    sys.exit()

# print(learn.model)

print('\n')
print('------------------------------------------------')
print('------------------------------------------------')
print('learn.dls.vocab')
print(learn.dls.vocab)
print('------------------------------------------------')
print('------------------------------------------------')

# In[]


print (TEST_DATA)

image_path = os.path.join(TEST_DATA, 'benign_2--ID_8337--544x531--ARIETTA.png')
print(image_path)

from fastai.vision.core import PILImage

# Load the image
img = PILImage.create(image_path)


# Process the image (the same way your DataLoaders do)
processed_img = learn.dls.test_dl([img]).one_batch()[0]

# Get raw predictions
raw_preds = learn.model(processed_img)

print("Raw predictions:", raw_preds)
print("Type of raw_preds:", type(raw_preds))
print("Shape of raw_preds:", [p.shape for p in raw_preds])
print('\n')

import torch

# Extract the tensor from the list
prob_tensor = raw_preds[0]

# Unpack the probabilities
benign_prob, malignant_prob, high_prob, low_prob, moderate_prob = prob_tensor

# Convert lists to tensors
lesion_type_probs = torch.tensor([benign_prob, malignant_prob])
birads_probs      = torch.tensor([high_prob, low_prob, moderate_prob])

# Get the predicted classes
lesion_type_pred = learn.dls.vocab[0][lesion_type_probs.argmax()]
birads_pred      = learn.dls.vocab[1][birads_probs.argmax()]

print("Lesion Type Prediction:", lesion_type_pred)
print("BIRADS Prediction:     ", birads_pred)


# In[]
