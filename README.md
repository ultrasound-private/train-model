# IPPT BrEaST ultrasound

Create a demo CNN model to classify ultrasound images.

# Output

![training_data](misc/sample-output.jpg)

# Important files
    - Training code:
        - 02-TRAIN-MODEL-MULTI-TARGET.py

    - Bash script:
        - do_train.sh

    - Create data pipeline for training:
        - do_pipeline.sh

    - Summary Report:
        - ./report/Report.docx

# General Notes Folder

- [./ippt-notes/](./ippt-notes/)

# File Locations
    - Input training data:
        - /mnt/dysk_roboczy/efarell/repos/US-Data/2023-11-12

    - Output predictions data:
        - /mnt/dysk_roboczy/efarell/repos/output

# Experiment with 'x' markings: (Beata, Norbert)

- Readme file
    - [./x-experiments/README.md](./x-experiments/README.md)

    - Input data
        - /mnt/dysk_roboczy/efarell/repos/US-Data/x

    - Output data
        - /mnt/dysk_roboczy/efarell/repos/output_xx

# Ultrasound images

[Ultrasound CSV Metadata](misc/csv-export-format.txt)

# Conda Environments

See README:  [./CONDA/README.md](./CONDA/README.md)
